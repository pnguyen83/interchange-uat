@istest
public class skedDataFactory {

    //Create List of Regions
    public static list<sked__Region__c> createRegions(){
        skedAddressOption opt = new skedAddressOption();
        list<sked__Region__c> lstReg = new list<sked__Region__c>();
        sked__Region__c reg = new sked__Region__c();
        reg.Name = 'Western Australia';
        reg.sked__Timezone__c = 'Australia/Sydney';
        reg.sked__Country_Code__c = 'AU';

        lstReg.add(reg);

        sked__Region__c reg1= new sked__Region__c();
        reg1.Name = 'Melbourne';
        reg1.sked__Timezone__c = 'Australia/Sydney';
        reg1.sked__Country_Code__c = 'AU';

        lstReg.add(reg1);

        insert lstReg;
        return lstReg;
    }

    public static Group_Event__c createGroupEvent(Contact emp,enrtcr__Service__c serv,sked__Region__c reg,Account acc, Contact client){
        Group_Event__c GE = new Group_Event__c();
        GE.Name = 'Test Group';
        GE.Group_Status__c = 'Active';
        GE.Coordinator__c = emp.id;
        GE.Address__c = 'Test Address';
        GE.Region__c = reg.id;
        GE.Account__c = acc.id;

        insert GE;

        Group_Client__c GC = new Group_Client__c();
        GC.Group_Event__c = GE.id;
        GC.Client__c = client.id;
        GC.Service_Agreement_Item__c = skedDataFactory.createSupportContract(client).id;

        insert GC;

        return GE;

    }

    public static Account createAccount(){
        
        Account acc = new Account();
        //acc.RecordTypeId = devRecordTypeId;
        acc.Name = 'Test Household';
        acc.ShippingStreet = 'Test';
        acc.ShippingStreet = 'Test';
        acc.ShippingStreet = 'Test';
        acc.ShippingStreet = 'Test';
        acc.ShippingStreet = 'Test';
        insert acc;

        return acc;

    }

    public static Account createOganization(){
        Id devRecordTypeId = skedTestDataFactory.getRecordTypeIDByDeveloperName('Account','Organization');
        Account acc = new Account();
        acc.RecordTypeId = devRecordTypeId;
        acc.Name = 'Test Organization';
        acc.ShippingStreet = 'Test';
        acc.ShippingPostalCode = 'Test';
        acc.ShippingState = 'Test';
        acc.ShippingCity = 'Test';
        acc.ShippingCountry = 'Test';
        insert acc;

        return acc;

    }

    public static Contact createEmployee(Account acc){
        Id devRecordTypeId = skedTestDataFactory.getRecordTypeIDByDeveloperName('Contact','Employee');

        Contact con = new Contact();
        con.FirstName = 'Test';
        con.LastName = 'Employee';
        con.AccountId = acc.id;
        con.RecordTypeId = devRecordTypeId;
        con.enrtcr__Status__c = 'Active';
        con.OtherCity = 'NY';
        con.OtherPostalCode = '4006';
        con.Phone = '0411111111';

        insert con;

        return con;
    }


    public static Contact createClient(Account acc, string Name){
        Id devRecordTypeId = skedTestDataFactory.getRecordTypeIDByDeveloperName('Contact','Client');

        Contact con = new Contact();
        con.FirstName = Name;
        con.LastName = Name;
        con.AccountId = acc.id;
        con.RecordTypeId = devRecordTypeId;
        con.enrtcr__Status__c = 'Current';
        con.enrtcr__Client_Region__c = 'Northern';
        con.Birthdate = System.today().addDays(-10000);
        con.enrtcr__Sex__c = 'Male';
        con.otherStreet = 'Test';
        con.otherCity = 'Test';
        con.otherState = 'Test';
        con.otherPostalCode = 'Test';
        con.otherCountry = 'Test';
        con.Phone = '0411111111';
        con.Email = 'test@test.com';
        con.enrtcr__Preferred_Communication_Method__c = 'Email';
        con.enrtcr__Primary_Disability__c = 'Deaf';
        con.enrtcr__Client_Region__c = 'test';
        insert con;

        return con;
    }

    public static enrtcr__Service__c createServiceItem(string serviceName){
        enrtcr__Service__c serviceItem = new enrtcr__Service__c();
        serviceItem.name = serviceName;
        serviceItem.enrtcr__Support_Item_Code__c = 'Test';
        //serviceItem.SLCS_Group_Event__c = true;
        insert serviceItem;
        return serviceItem;
    }
    
    public static enrtcr__Service__c createServiceItemGroup(string serviceName){
        enrtcr__Service__c serviceItem = new enrtcr__Service__c();
        serviceItem.name = serviceName;
        serviceItem.enrtcr__Support_Item_Code__c = 'Test';
        serviceItem.SLCS_Group_Event__c = true;
        insert serviceItem;
        return serviceItem;
    }

    public static enrtcr__Support_Contract_Item__c createSupportContract(Contact con){
        Id blockGrantRTId = skedTestDataFactory.getRecordTypeIDByDeveloperName('enrtcr__Support_Contract__c','Grant');

        Map<String,enrtcr__Reference_Data__c> referencedata = createEnriteCareReferenceData();

        enrtcr__Support_Contract__c Sup = new enrtcr__Support_Contract__c(enrtcr__Client__c = con.id, Name = 'Test Support Contract');
        Sup.enrtcr__End_Date__c = System.today().addDays(10);
        Sup.enrtcr__Start_Date__c = System.today().addDays(-10);
        Sup.enrtcr__Mac_Funded__c = true;
        Sup.enrtcr__Status__c = skedConstants.STATUS_CURRENT;
        Sup.RecordTypeId = blockGrantRTId;
        Sup.enrtcr__Client__c = null;
        
        insert Sup;

        enrtcr__Service__c sertItem = createServiceItem('Support Item');

        enrtcr__Support_Contract_Item__c SCI = new enrtcr__Support_Contract_Item__c();
        SCI.enrtcr__Service__c = sertItem.id;
        SCI.enrtcr__Support_Contract__c = Sup.Id;
        SCI.enrtcr__Support_Category__c = referencedata.get('Support_Category:Test Support Category').Id;
        SCI.RecordTypeId = skedTestDataFactory.getRecordTypeIDByDeveloperName('enrtcr__Support_Contract_Item__c','Item');

        insert SCI;

        return SCI;

    }

    public static Map<String, RecordType> RecordTypes
    {
        get
        {
            if (RecordTypes == null)
            {
                RecordTypes = new Map<String, RecordType>();

                for (RecordType rt : [SELECT Id,
                        sObjectType,
                        DeveloperName
                FROM RecordType])
                {
                    RecordTypes.put(rt.sObjectType + ':' + rt.DeveloperName, rt);
                }
            }
            return RecordTypes;
        }
        set;
    }

    public static Map<String, enrtcr__Reference_Data__c> createEnriteCareReferenceData()
    {
        Map<String, enrtcr__Reference_Data__c> referenceData;

        // insert rate type reference data
        referenceData = new Map<String, enrtcr__Reference_Data__c>();
        referenceData.put('Rate_Type:NDIS', new enrtcr__Reference_Data__c(RecordTypeId = RecordTypes.get(enrtcr__Reference_Data__c.sObjectType.getDescribe().getName() + ':Rate_Type').Id, enrtcr__Funding_Type__c = 'NDIS'));
        referenceData.put('Rate_Type:Other', new enrtcr__Reference_Data__c(RecordTypeId = RecordTypes.get(enrtcr__Reference_Data__c.sObjectType.getDescribe().getName() + ':Rate_Type').Id, enrtcr__Funding_Type__c = 'Other'));
        referenceData.put('Rate_Type:Grant', new enrtcr__Reference_Data__c(RecordTypeId = RecordTypes.get(enrtcr__Reference_Data__c.sObjectType.getDescribe().getName() + ':Rate_Type').Id, enrtcr__Funding_Type__c = 'Grant'));

        referenceData.put('Support_Category:Test Support Category', new enrtcr__Reference_Data__c(RecordTypeId = RecordTypes.get(enrtcr__Reference_Data__c.sObjectType.getDescribe().getName() + ':Support_Category').Id, Name = 'Test Support Category'));


        insert referenceData.values();

        return referenceData;
    }

    public static enrtcr__Support_Delivered__c createServiceDelivered(Map<String, enrtcr__Reference_Data__c> referenceData)
    {
        List<enrtcr__Site__c> testSites;
        List<enrtcr__Service__c> testServices;
        List<enrtcr__Rate__c> testRates;

        testSites = new List<enrtcr__Site__c>();
        testSites.add(new enrtcr__Site__c(Name = 'Test'));

        insert testSites;

        enrtcr__Site_User__c siteUser = new enrtcr__Site_User__c(enrtcr__Site__c = testSites[0].Id, enrtcr__User__c = UserInfo.getUserId());

        insert siteUser;

        testServices = new List<enrtcr__Service__c>();
        testServices.add(new enrtcr__Service__c(Name = 'Test', enrtcr__NMDS_Service_Code__c = '1234', enrtcr__Include_in_Services_Received_Extract__c = true, enrtcr__Include_in_Service_User_Extract__c = true, enrtcr__Support_Item_Code__c='123'));


        insert testServices;
        update testServices;


        List<enrtcr__Site_Service__c> siteServices = new List<enrtcr__Site_Service__c>();
        siteServices.add(new enrtcr__Site_Service__c(RecordTypeId = RecordTypes.get(enrtcr__Site_Service__c.sObjectType.getDescribe().getName() + ':Service_Assignment').Id, enrtcr__Site__c = testSites[0].Id, enrtcr__Service__c = testServices[0].Id));
        siteServices.add(new enrtcr__Site_Service__c(RecordTypeId = RecordTypes.get(enrtcr__Site_Service__c.sObjectType.getDescribe().getName() + ':Service_Type_Outlet').Id, enrtcr__Site__c = testSites[0].Id, enrtcr__Service__c = testServices[0].Id));


        insert siteServices;


        testRates = new List<enrtcr__Rate__c>();
        testRates.add(new enrtcr__Rate__c(Name = 'NDIS Rate',
                enrtcr__Service__c = testServices[0].Id,
                enrtcr__RateType__c = referenceData.get('Rate_Type:NDIS').Id,
                enrtcr__Amount_ex_GST__c = 1,
                enrtcr__GST__c = 0,
                enrtcr__Quantity_Type__c = 'Days',
                //enrtcr__Rate_Type__c = 'Purchased Service',
                enrtcr__Effective_Date__c = Date.today().addMonths(-1)));

        insert testRates;



        testRates = new List<enrtcr__Rate__c>();
        testRates.add(new enrtcr__Rate__c(Name = 'NDIS Rate',
                enrtcr__Service__c = testServices[0].Id,
                enrtcr__RateType__c = referenceData.get('Rate_Type:NDIS').Id,
                enrtcr__Amount_ex_GST__c = 1,
                enrtcr__GST__c = 0,
                enrtcr__Quantity_Type__c = 'Days',                
                //enrtcr__Rate_Type__c = 'Purchased Service',
                enrtcr__Effective_Date__c = Date.today().addMonths(-1)));

        insert testRates;

        // client
        Contact testClient = new Contact(
                LastName = 'test',
                enrtcr__Sex__c = 'Male',
                Birthdate = Date.parse('01/01/1940'),
                enrtcr__Photo__c = '<img src="test" />',
                enrtcr__Secondary_Disability__c = 'test',
                OtherStreet = '123 test Street',
                OtherCity = 'Adelaide',
                OtherState = 'SA',
                OtherPostalCode = '5112',
                enrtcr__Date_Client_Registered__c = Date.today(),
                Phone = '0411111111',
                enrtcr__Preferred_Communication_Method__c = 'Phone',
                enrtcr__Primary_Disability__c = 'Autism',
                enrtcr__Status__c = 'Waitlist',
                enrtcr__Summary_Disability__c = 'Autism',
                enrtcr__Requires_Support_for_Decision_Making__c = 'No',
                enrtcr__Client_Region__c = 'Northern',
                MobilePhone = '0411111111'
        );

        insert testClient;

        // support contract
        enrtcr__Support_Contract__c testSupportContract = new enrtcr__Support_Contract__c(
                enrtcr__Client__c = testClient.Id,
                enrtcr__Start_Date__c = Date.today(),
                enrtcr__End_Date__c = Date.today().addYears(1),
                enrtcr__Status__c = 'Draft',
                enrtcr__Funding_Type__c = 'Test');

        insert testSupportContract;

        enrtcr__Support_Contract_Item__c testSupportCategoryItem = new enrtcr__Support_Contract_Item__c(
                enrtcr__Support_Contract__c = testSupportContract.Id,
                enrtcr__Support_Category__c = referenceData.get('Support_Category:Test Support Category').Id,
                RecordTypeId = RecordTypes.get('enrtcr__Support_Contract_Item__c:Category').Id,
                enrtcr__Site__c = testSites[0].Id,
                enrtcr__Service__c = testServices[0].Id,
                enrtcr__Rate__c = testRates[0].Id,
                enrtcr__Quantity__c = 1,
                enrtcr__Exclude_Item_In_Totals__c = false);


        enrtcr__Support_Contract_Item__c testSupportContractItem = new enrtcr__Support_Contract_Item__c(
                enrtcr__Support_Contract__c = testSupportContract.Id,
                enrtcr__Site__c = testSites[0].Id,
                enrtcr__Service__c = testServices[0].Id,
                enrtcr__Rate__c = testRates[0].Id,
                enrtcr__Quantity__c = 1,
                enrtcr__Exclude_Item_In_Totals__c = false);

        insert new List<enrtcr__Support_Contract_Item__c>{testSupportCategoryItem, testSupportContractItem};



        enrtcr__Support_Delivered__c serviceDelivered =  new enrtcr__Support_Delivered__c(
                enrtcr__Client__c = testClient.Id,
                enrtcr__Support_Contract__c = testSupportContract.Id,
                enrtcr__Support_Contract_Item__c = testSupportContractItem.Id,
                enrtcr__Rate__c = testRates[0].Id,
                enrtcr__Date__c = Date.today(),
                enrtcr__Quantity__c = 1,
                enrtcr__Worker__c = UserInfo.getUserId());

        insert serviceDelivered;

        return serviceDelivered;
    }


    public static enrtcr__Support_Contract__c createSupportContract2(Contact con){
        enrtcr__Support_Contract__c Sup = new enrtcr__Support_Contract__c(enrtcr__Client__c = con.id, Name = 'Test Support Contract');
        Sup.enrtcr__End_Date__c = System.today().addDays(10);
        Sup.enrtcr__Start_Date__c = System.today().addDays(-10);
        insert Sup;

        enrtcr__Service__c sertItem = createServiceItem('Support Item');

        enrtcr__Support_Contract_Item__c SCI = new enrtcr__Support_Contract_Item__c();
        SCI.enrtcr__Service__c = sertItem.id;
        SCI.enrtcr__Support_Contract__c = Sup.Id;

        insert SCI;

        return Sup;

    }

    public static enrtcr__Risk__c createRisk(Contact con){
        enrtcr__Risk__c risk = new enrtcr__Risk__c();
        risk.enrtcr__Client__c = con.id;
        risk.enrtcr__Domain__c = 'Test Domain';
        risk.enrtcr__Sub_Domain__c = 'Test Sub Domain';
        risk.enrtcr__Details__c = 'Test Details';
        risk.enrtcr__Display_as_Alert__c = true;
        risk.enrtcr__Status__c = 'Active';

        insert risk;

        return risk;
    }

    public static sked__Location__c createLocation(){
        sked__Location__c loc = new sked__Location__c();
        loc.Name = 'Test Location';
        loc.sked__Address__c = 'Test Location';
        insert loc;
        return loc;
    }
}