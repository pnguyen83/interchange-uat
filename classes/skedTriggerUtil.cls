public  class skedTriggerUtil {
    public static boolean isFromJobAllocation = false;
    public static boolean isFromSkeduloActivity = false;
    public static boolean isFromEvent = false;
    public static boolean isFromJob = false;

    public static String DEFAULT_ACTIVITY_TYPE          = 'Outlook/SFDC Calendar';
    public static String DEFAULT_ACTIVITY_LOCATION      = 'Brisbane';
    public static String DEFAULT_STREET                 = 'McLachLan';

    /*
    * Sync Activities to Event when Activities are upserted
    */
    public static void syncActivitiesToEvents(list<sked__Activity__c> activityList){
        if( activityList==null || activityList.isEMpty() ) return;
        //Get source activities (i.e Activities that are not referencing to other events)
        list<sked__Activity__c> sourceList = new list<sked__Activity__c>();
        for(sked__Activity__c a : activityList){
            if( String.isBlank( a.Event_ID__c) ) sourceList.add( a );
        }
        if(sourceList.isEmpty()) return;

        list<Event> eventsToUpsert = new list<Event>();
        list<ID> resourceIDs    = new list<ID> ();
        list<String> activityIDs    = new list<String> ();

        for(sked__Activity__c act: sourceList){
            if(act.sked__Resource__c != null){
                resourceIDs.add(act.sked__Resource__c);
                activityIDs.add( act.Id );
            }
        }


        //Map Resource ID to Resource to get User's Id 
        map<id,sked__Resource__c> map_Id_Res = new map<id,sked__Resource__c>([select id , sked__User__c from sked__Resource__c where id in:resourceIDs and sked__User__c != null]);
        //Search for related Events
        list<Event> relatedEvents = [Select Id, Activity_ID__c from Event where Activity_ID__c IN :activityIDs];
        Map<Id,Id> mapActivityIdToEventId = new Map<Id,Id>();
        for( Event e : relatedEvents ){
            mapActivityIdToEventId.put( Id.valueOf(e.Activity_ID__c), e.Id );
        }

        for(sked__Activity__c act: sourceList){
            if(act.sked__Resource__c != null && map_Id_Res.containskey(act.sked__Resource__c)){
                Id eventId = mapActivityIdToEventId.containsKey( act.Id )? mapActivityIdToEventId.get( act.Id ) : null;
                eventsToUpsert.add( 
                    syncActivityToEvent(act, map_Id_Res.get(act.sked__Resource__c).sked__User__c, eventId)
                );
            }
        }

        if(eventsToUpsert.size() > 0){
            upsert eventsToUpsert;
        }   
    }

    /*
    * Sync an Activity to an Event
    */
    public static Event syncActivityToEvent(sked__Activity__c activity, Id userId, Id eventId){
        Event event = new Event(
            Subject         = activity.Name + (activity.sked__Type__C==null?'':(' - ' + activity.sked__Type__C)),
            OwnerId         = userId,
            Location        = activity.sked__Address__c,
            StartDateTime   = activity.sked__Start__c,
            EndDateTime     = activity.sked__End__c,
            Description     = activity.sked__Notes__c,
            Activity_ID__c  = activity.Id,
            enrtcr__Other_Street__c = activity.sked__Address__c,
            RecordTypeId = getRecordTypeId(Schema.sObjectType.Event,'Individual')
        );
        if(eventId != null){//Update an existing event
            event.Id    = eventId;
        }
        return event;
    }


    public static void syncServiceDelivery(List<sked__Job_Allocation__c> jobAllocations, Map<id, sked__Job_Allocation__c> oldAllocationsByIds)
    {
        List<Id> jobAllocationIds = new List<Id>();
        for(sked__Job_Allocation__c jobAllocation : jobAllocations)
        {
            if(jobAllocation.sked__Time_Completed__c != oldAllocationsByIds.get(jobAllocation.id).sked__Time_Completed__c || jobAllocation.sked__Time_In_Progress__c != oldAllocationsByIds.get(jobAllocation.id).sked__Time_In_Progress__c)
            {
                jobAllocationIds.add(jobAllocation.Id);
            }
        }

        List<enrtcr__Support_Delivered__c> serviceDeliveries = [SELECT enrtcr__Quantity__c, Job_Allocation__r.sked__Time_Completed__c, Job_Allocation__r.sked__Time_In_Progress__c
                                                               FROM enrtcr__Support_Delivered__c
                                                               WHERE Job_Allocation__c in: jobAllocationIds
                                                               AND Job_Allocation__r.sked__Status__c = 'Complete'
                                                               AND Job_Service_Item__r.Delivery_Method__c =: SkeduloConstants.DELIVERY_METHOD_STATUS_WORKER_ACTUAL_TIME];

        for(enrtcr__Support_Delivered__c sd : serviceDeliveries)
        {
            sd.enrtcr__Quantity__c = skedTriggerHandler.getWokerActualTime(sd.Job_Allocation__r);
        }


        update serviceDeliveries;
    }



    /*
    * Sync Events to Activities when Events are upserted
    */
    public static void syncEventsToActivities(list<Event> eventList){   
        if( eventList==null || eventList.isEMpty() ) return;
        //Get source activities (i.e Activities that are not referencing to other events)
        list<Event> sourceList = new list<Event>();
        for(Event a : eventList){
            if( String.isBlank( a.Activity_ID__c) && String.isBlank(a.Job_Allocation_ID__c)) sourceList.add( a );
        }
        if(sourceList.isEmpty()) return;

        list<id> lstWhoIDs = new list<id>();
        list<String> eventIDs   = new list<String> ();

        for(Event e: sourceList){
            lstWhoIDs.add(e.OwnerId);
            eventIDs.add( e.Id );
        }

        list<sked__Resource__c> lstRes = [select id, sked__User__c from sked__Resource__c where sked__User__c in:lstWhoIDS];
        map<id,id> map_User_Res = new map<id,id>();

        for(sked__Resource__c res: lstRes){
            map_User_Res.put(res.sked__User__c,res.id);
        }
        //Search for related activities
        list<sked__Activity__c> relatedActivities = [Select Id, Event_ID__c from sked__Activity__c where Event_ID__c IN :eventIDs];
        Map<Id,Id> mapEventIdToActivityId = new Map<Id,Id>();
        for( sked__Activity__c a : relatedActivities ){
            mapEventIdToActivityId.put( Id.valueOf(a.Event_ID__c), a.Id );
        }

        list<sked__Activity__c> activitiesToUpsert = new list<sked__Activity__c>();

        for(Event e: sourceList){
            if(map_User_Res.containskey(e.OwnerId)){
                Id aId = mapEventIdToActivityId.containsKey( e.Id )? mapEventIdToActivityId.get( e.Id ) : null;
                activitiesToUpsert.add( syncEventToActivity(e, map_User_Res.get(e.OwnerId), aId) );
            }
        }

        if(activitiesToUpsert.size() > 0){
            Schema.SObjectField eventID = sked__Activity__c.Event_ID__c.getDescribe().getSObjectField();
            Database.upsert(activitiesToUpsert, eventID, false)  ;
        }
    }

    /*
    * Sync an Event to an Activity
    */
    public static sked__Activity__c syncEventToActivity(Event e, Id resourceId, Id activityId){
        sked__Activity__c a = new sked__Activity__c(
            sked__Type__C       = DEFAULT_ACTIVITY_TYPE,
            sked__Notes__c      = e.Description,
            sked__Start__c      = e.StartDateTime,
            sked__End__c        = e.EndDateTime,
            sked__Address__c    = fs(e.enrtcr__Other_Street__c, DEFAULT_ACTIVITY_LOCATION),
            Event_ID__c         = e.Id,
            sked__Resource__c   = resourceId
        );
        if(e.IsAllDayEvent){
            DateTime dt = e.StartDateTime;
            a.sked__Start__c      = DateTime.newInstance(dt.year(), dt.month(), dt.day(), 0, 0, 0);
            a.sked__End__c        = DateTime.newInstance(dt.year(), dt.month(), dt.day(), 23, 59, 0);
            System.debug(a.sked__Start__c);
        }
        if(activityId != null){//Update an existing activity
            a.Id    = activityId;
        }
        return a;
    }

    /*
    * Return a list of ID of Dispatched or Confirmed Job Allocations
    */
    public static list<Id> getNewDispatchedJobAllocationIDs(list<sked__Job_Allocation__c> newList){
        list<Id> jaIDs = new list<Id>();

        for (sked__Job_Allocation__c so : newList) {
            if(so.sked__Status__c == 'Dispatched' || so.sked__Status__c=='Confirmed'){ // Dispatch
                jaIDs.add(so.id);
            }
        }

        return jaIDs;
    }

    /*
    * Return a list of ID of Job Allocations which status changed to one of status in statusSet
    */
    public static list<Id> getJobAllocationIDs_StatusChanged(Set<String> statusSet, list<sked__Job_Allocation__c> newList, list<sked__Job_Allocation__c> oldList, map<Id, sked__Job_Allocation__c> oldMap){
        list<Id> jaIDs = new list<Id>();

        for (sked__Job_Allocation__c newJA : newList) {
            sked__Job_Allocation__c oldJA = oldMap.get(newJA.Id);
            if( newJA.sked__Status__c != null && statusSet.contains( newJA.sked__Status__c ) && newJA.sked__Status__c != oldJA.sked__Status__c){ 
                jaIDs.add( newJA.Id );
            }
        }

        return jaIDs;
    }

    /*
    * Sync Job Allocations to Events
    */
    public static void syncJobAllocationsToEvents(list<Id> jaIDs){
        if( jaIDs==null || jaIDs.isEmpty() ) return;

        list<Event> eventsToUpsert = new list<Event>();

        map<Id, sked__Job_Allocation__c> mapJAIdToJA = new map<Id,sked__Job_Allocation__c>(
            [select id, Name,sked__Job__r.sked__Contact__c, sked__Job__r.sked__Type__c, sked__Status__c,sked__Job__r.sked__Contact__r.Phone,sked__Job__r.sked__Account__c,sked__Resource__r.sked__User__c, 
                sked__Job__r.sked__Address__c,sked__Job__r.sked__Account__r.Name, sked__Job__r.sked__Contact__r.Name, sked__Job__r.Name, sked__Job__r.sked__Start__c, 
                sked__Job__r.sked__Finish__c, sked__Job__r.sked__Notes_Comments__c 
            from sked__Job_Allocation__c 
            where Id IN :jaIDs and sked__Job__r.sked__Start__c != null and sked__Job__r.sked__Finish__c != null and sked__Resource__r.sked__User__c!=null]
        );

        //Search for related Events
        list<Event> relatedEvents = [Select Id, Job_Allocation_ID__c from Event where Job_Allocation_ID__c IN :mapJAIdToJA.keySet()];
        Map<Id,Id> mapJAIdToEventId = new Map<Id,Id>();
        
        for( Event e : relatedEvents ){
            mapJAIdToEventId.put( Id.valueOf(e.Job_Allocation_ID__c), e.Id );
        }

        for (sked__Job_Allocation__c ja : mapJAIdToJA.values()) {
            Id eId = mapJAIdToEventId.containsKey( ja.Id )? mapJAIdToEventId.get( ja.Id ) : null;
            eventsToUpsert.add( syncJobAllocationToEvent(ja, eId) );         
        }
        if(eventsToUpsert.size() > 0){
            upsert eventsToUpsert;
        }
    }

    /*
    * Get Record Type ID by Name
    */
    public static Id getRecordTypeId(Schema.DescribeSObjectResult d, String name){
        Map<String,Schema.RecordTypeInfo> rtMapByName = d.getRecordTypeInfosByName();
        if(rtMapByName.containsKey(name)){
            return rtMapByName.get(name).getRecordTypeId();
        }
        return null;
    }

    /*
    * Sync a Job Allocation to an Event record
    */
    public static Event syncJobAllocationToEvent(sked__Job_Allocation__c ja, Id eventId){
        Event e = new Event(
            Subject         = 'Skedulo Job - ' + ja.sked__Job__r.Name,
            OwnerId         = ja.sked__Resource__r.sked__User__c,
            enrtcr__Other_Street__c        = fs(ja.sked__Job__r.sked__Address__c, DEFAULT_STREET),
            StartDateTime   = ja.sked__Job__r.sked__Start__c,
            EndDateTime     = ja.sked__Job__r.sked__Finish__c,
            Description     = fs(ja.sked__Job__r.sked__Account__r.Name, null) + '\n' + fs(ja.sked__Job__r.sked__Contact__r.Name, null) + '\n' + fs(ja.sked__Job__r.sked__Notes_Comments__c, null),
            Job_Allocation_ID__c    = ja.Id,
            Job_Status__c   = ja.sked__Status__c

        );
        if(ja.sked__Job__r.sked__Type__c == 'Group Event'){
            e.RecordTypeId = getRecordTypeId(Schema.sObjectType.Event,'Group');
        }else if(ja.sked__Job__r.sked__Type__c == 'Single Booking'){
            e.RecordTypeId = getRecordTypeId(Schema.sObjectType.Event,'Individual');
        }
        if(ja.sked__job__r.sked__Contact__c != null){
            e.WhoID     = ja.sked__Job__r.sked__Contact__c;
            e.Location  = fs(e.Location, null) + ' ' + fs(ja.sked__Job__r.sked__Contact__r.Phone, null);
        }
        if(eventId != null){//Update an existing event
            e.Id    = eventId;
        }
        return e;
    }

    /*
    * Format a String, return defaultValue if null
    */
    public static String fs(String s, String defaultValue){
        if( !String.isBlank(s) ) return s;
        else if( !String.isBlank( defaultValue ) ) return defaultValue;
        return '';
    }

    /*
    * Delete all Events related to Job Allocations
    */
    public static void deleteRelatedEvents(list<sked__Job_Allocation__c> jaList){
        if( jaList==null || jaList.isEmpty() ) return;

        list<id> allIds = new list<id>();
        for (sked__Job_Allocation__c so : jaList) {
            allIds.add(so.id);
        }

        list<Event> lstEvent = [select id from Event where Job_Allocation_ID__c in:allIds];

        if(lstEvent.size() > 0){
            delete lstEvent;
        }
    }

    /*
    * Return a list of ID of Job Allocations which status changed to one of status in statusSet
    */
    public static list<Id> getJobIDs_StartEndTimeChanged(list<sked__Job__c> newList, list<sked__Job__c> oldList, map<Id, sked__Job__c> oldMap){
        list<Id> jIDs = new list<Id>();

        for (sked__Job__c newRec : newList) {
            sked__Job__c oldRec = oldMap.get(newRec.Id);
            if(newRec.sked__Start__c != null && newRec.sked__Finish__c != null && (newRec.sked__Start__c != oldRec.sked__Start__c || newRec.sked__Finish__c != oldRec.sked__Finish__c)){
                jIDs.add( newRec.Id );
            }
        }

        return jIDs;
    }

    /*
    * Return a list of ID of Jobs which status changed to one of status in statusSet
    */
    public static list<Id> getJobIDs_StatusChanged(Set<String> statusSet, list<sked__Job__c> newList, list<sked__Job__c> oldList, map<Id, sked__Job__c> oldMap){
        list<Id> jaIDs = new list<Id>();

        for (sked__Job__c newRec : newList) {
            sked__Job__c oldRec = oldMap.get(newRec.Id);
            if( newRec.sked__Job_Status__c != null && statusSet.contains( newRec.sked__Job_Status__c ) && newRec.sked__Job_Status__c != oldRec.sked__Job_Status__c){ 
                jaIDs.add( newRec.Id );
            }
        }

        return jaIDs;
    }

    /*
    * Delete Activities related to deleted events
    */
    public static void deleteActivities(list<Event> events){
        Set<Id> eventIDs = new Set<Id>();
        for( Event e : events ){
            eventIDs.add( e.Id );
        }
        list<sked__Activity__c> aList = [Select Id from sked__Activity__c where Event_Id__c IN :eventIDs];
        if( !aList.isEmpty() ) Database.delete( aList );
    }

    /*
    * Get in progress job allocation
    */
    public static List<sked__Job_Allocation__c> getInProgressAllocation(Map<Id, sked__Job_Allocation__c> mapNew, Map<Id, sked__Job_Allocation__c> mapOld) {
        List<sked__Job_Allocation__c> lstResult = new List<sked__Job_Allocation__c>();

        for (sked__Job_Allocation__c allocation : mapNew.values()) {
            sked__Job_Allocation__c oldAllocation = mapOld.get(allocation.Id);
            if (oldAllocation != null) {
                if (allocation.sked__Status__c.equals(skedConstants.JOB_ALLOCATION_STATUS_IN_PROGRESS) && 
                    !oldAllocation.sked__Status__c.equals(skedConstants.JOB_ALLOCATION_STATUS_IN_PROGRESS)) {
                    lstResult.add(allocation);
                }
            }
        }

        return lstResult;
    }

    /*
    * Calculate the distance from home to job or previous job to this job 
    */
    public static void updateDistance(List<sked__Job_Allocation__c> lstNew) {
        Map<String, List<JobAllocation>> mapResJobAllocation = new Map<String, List<JobAllocation>>();
        Map<String, List<sked__Job_Allocation__c>> mapJobNewAllocations = new Map<String, List<sked__Job_Allocation__c>>();
        Set<String> setAllocationIds = new Set<String>();


        for (sked__Job_Allocation__c jobAll : lstNew) {
            List<sked__Job_Allocation__c> lstAllocations = mapJobNewAllocations.get(jobAll.sked__Job__c);
            if (lstAllocations == null) {
                lstAllocations = new List<sked__Job_Allocation__c>();
            }
            lstAllocations.add(jobAll);
            mapJobNewAllocations.put(jobAll.sked__Job__c, lstAllocations);
            if (jobAll.Id != null) {
                setAllocationIds.add(jobAll.id);
            }
        }

        //get Location of current job
        for (sked__Job__c job : [SELECT Id, sked__GeoLocation__c, sked__Start__c, sked__Finish__c 
                                    FROM sked__Job__c 
                                    WHERE sked__Start__c!=null AND Id IN : mapJobNewAllocations.keySet()]) {
            List<sked__Job_Allocation__c> lstAllocations = mapJobNewAllocations.get(job.id);
            if (lstAllocations != null) {
                for (sked__Job_Allocation__c allocation : lstAllocations) {
                    JobAllocation alModel = new JobAllocation();
                    alModel.id = allocation.id;
                    alModel.resourceId = allocation.sked__Resource__c;
                    alModel.jobLocation = job.sked__GeoLocation__c;
                    alModel.startTime = job.sked__Start__c;
                    alModel.endTime = job.sked__Finish__c;
                    alModel.onDate = job.sked__Start__c.Date();
                    alModel.jobId = job.id;
                    List<JobAllocation> lstAllocationModels = mapResJobAllocation.get(allocation.sked__Resource__c);
                    if (lstAllocationModels == null) {
                        lstAllocationModels = new List<JobAllocation>();
                    }
                    lstAllocationModels.add(alModel);
                    mapResJobAllocation.put(allocation.sked__Resource__c, lstAllocationModels);
                }
            }
        }

        //get location of resource
        for (sked__Resource__c res : [SELECT Id, sked__GeoLocation__c FROM sked__Resource__c WHERE Id IN : mapResJobAllocation.keySet()]) {
            List<JobAllocation> lstAllocationModels = mapResJobAllocation.get(res.Id);

            if (lstAllocationModels != null) {
                for (JobAllocation allocation : lstAllocationModels) {
                    allocation.resLocation = res.sked__GeoLocation__c;
                }
                mapResJobAllocation.put(res.Id, lstAllocationModels);
            }
        }

        for (sked__Job_Allocation__c allocation : [SELECT Id, sked__Resource__c, sked__job__r.sked__Start__c,
                                                            sked__job__r.sked__Finish__c, sked__job__r.sked__GeoLocation__c
                                                    FROM sked__Job_Allocation__c 
                                                    WHERE sked__Resource__c IN : mapResJobAllocation.keySet()
                                                    AND sked__Status__c != : skedConstants.JOB_ALLOCATION_DECLINED
                                                    AND sked__Status__c != : skedConstants.JOB_ALLOCATION_DELETED
                                                    And sked__Status__c != : skedConstants.JOB_ALLOCATION_MODIFIED
                                                    And sked__Status__c != : skedConstants.JOB_ALLOCATION_STATUS_COMPLETE
                                                    AND sked__job__r.sked__Start__c!=null 
                                                    AND sked__job__r.sked__Finish__c!=null 
                                                    ORDER BY sked__job__r.sked__Finish__c
                                                    ]) {
            Date allocationDate = allocation.sked__job__r.sked__Finish__c.Date();
            if (mapResJobAllocation.containsKey(allocation.sked__Resource__c)) {
                List<JobAllocation> lstAllocations = mapResJobAllocation.get(allocation.sked__Resource__c);
                if (lstAllocations != null) {
                    for (JobAllocation model : lstAllocations) {
                        if (allocationDate == model.onDate) {
                            if (model.oldAllocation == null || 
                                model.oldAllocation.sked__job__r.sked__Finish__c < allocation.sked__job__r.sked__Finish__c) {
                                if (allocation.sked__job__r.sked__Finish__c < model.startTime) {
                                    model.oldAllocation = allocation;
                                }
                            }
                        }
                    }
                }
                
            }
        }
		
        for (String resId : mapResJobAllocation.keySet()) {
            List<JobAllocation> lstAllocations = mapResJobAllocation.get(resId);
            if (lstAllocations != null) {
            	for (JobAllocation allocation : lstAllocations) {
                    System.debug('allocation = ' + allocation);
                    Location oldLocation;
                    if (allocation.oldAllocation == null || allocation.oldAllocation.sked__Job__r.sked__GeoLocation__c == null) {
                        oldLocation = allocation.resLocation;
                    } else {
                        oldLocation = allocation.oldAllocation.sked__Job__r.sked__GeoLocation__c;
                    }
                    Decimal distance = oldLocation==null?0:oldLocation.getDistance(allocation.jobLocation, 'km');
                    List<sked__Job_Allocation__c> lstJobAllocations = mapJobNewAllocations.get(allocation.jobId);
                    if (lstJobAllocations != null) {
                        for (sked__Job_Allocation__c jobAll : lstJobAllocations) {
                            if (jobAll.sked__Resource__c == resId) {
                                jobAll.Est_Distance_Traveled_KM__c = distance.setScale(0);
                            }
                        }
                    }
                }    
            }
            
        } 
        

    }

    /*
    *
    */
    public static void lockEvents(list<Event> events){
        for(Event e: events){
            if(!String.isBlank(e.Job_Allocation_ID__c) && !skedTriggerUtil.isFromJob && !skedTriggerUtil.isFromJobAllocation){
                e.addError('This event is Locked');
            }
        }
    }

    public static Decimal getQuantity(sked__Job__c job, Group_Client__c groupClient)
    {
        String deliveryMethod = groupClient == null ? job.Delivery_Method__c : groupClient.Delivery_Method__c;
        Decimal deliveryMethodQuantity = groupClient == null ? job.Delivery_Method_Quantity__c : groupClient.Quantity__c;

        if (deliveryMethod == 'Fixed Quantity')
        {
            return deliveryMethodQuantity;
        }

       return job.sked__duration__c / 60;
    }

    

    public class JobAllocation {
        public String id;
        public String jobId;
        public String resourceId;
        public Location jobLocation;
        public Location resLocation;
        public DateTime startTime;
        public DateTime endTime;
        public Date onDate;
        public sked__Job_Allocation__c oldAllocation;
    }
}