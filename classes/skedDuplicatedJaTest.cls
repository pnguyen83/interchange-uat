@isTest
public class skedDuplicatedJaTest {
	Testmethod static void doTest() {
		Map<string, sObject> mapTestData = skedDataSetup.setupJobTestData();
		sked__Job__c job1 = (sked__Job__c)mapTestData.get('job1');
		sked__Job__c job2 = (sked__Job__c)mapTestData.get('job2');
		sked__Resource__c res = (sked__Resource__c)mapTestData.get('resourceCustomerService1');
		DateTime startTime = DateTime.newInstance(Date.today(), Time.newInstance(10, 0, 0, 0));
		DateTime endTime = DateTime.newInstance(Date.today(), Time.newInstance(11, 0, 0, 0));

		job1.sked__Start__c = startTime;
		job1.sked__Finish__c = endTime;
		job1.sked__GeoLocation__Latitude__s = -27.457730;
        job1.sked__GeoLocation__Longitude__s = 153.037080;

		job2.sked__Start__c = startTime.addMinutes(30);
		job2.sked__Finish__c = endTime.addMinutes(30);
		job2.sked__GeoLocation__Latitude__s = -27.457730;
        job2.sked__GeoLocation__Longitude__s = 153.037080;

		update new List<sked__Job__c>{job1, job2};

		sked__Job_Allocation__c ja1 = new sked__Job_Allocation__c(
			sked__Job__c = job1.id,
			sked__Resource__c = res.id,
			sked__Status__c = 'Dispatched',
			sked_isDuplicated__c = true
		);
		sked__Job_Allocation__c ja2 = new sked__Job_Allocation__c(
			sked__Job__c = job2.id,
			sked__Resource__c = res.id,
			sked__Status__c = 'Dispatched',
			sked_isDuplicated__c = true
		);

		insert new List<sked__Job_Allocation__c>{ja1, ja2};

		Test.startTest();
		skedCheckDuplicateJAController controller = new skedCheckDuplicateJAController();
		controller.changeSize();
		controller.previous();
		controller.next();
		controller.filter = 'Future Jobs';
		controller.changeFilter();
		controller.getDuplicated();
		controller.filter = 'Past Jobs';
		controller.getDuplicated();
		controller.filter = 'Time Period';
		controller.changeFilter();
		controller.startDate = System.now().addDays(-1).format('yyyy-MM-dd');
		controller.endDate = System.now().addDays(1).format('yyyy-MM-dd');
		controller.getDuplicated();
		
		Test.stopTest();
	}
}