@isTest(SeeAllData=false)
private class skedCommonModelTest
{
	
	static testmethod void test_skedConfigs() {
		String DISTANCE_TRAVELLED_SPLIT = skedConfigs.DISTANCE_TRAVELLED_SPLIT;
		String RESOURCE_DISTANCE = skedConfigs.RESOURCE_DISTANCE;
		boolean SHOW_ESTIMATED_DISTANCE_TRAVELLED = skedConfigs.SHOW_ESTIMATED_DISTANCE_TRAVELLED;
		String GOOGLE_API_KEY = skedConfigs.GOOGLE_API_KEY;
	}

	static testmethod void test_skedAlertTrigger() {
		

	Test.setMock(HttpCalloutMock.class, new skedHttpCalloutMock(skedHttpCalloutMock.GEOCODE_DATA));
	    Account account = skedTestDataFactory.createAccounts('Test Account', 'Clinic', 1).get(0);
        insert account;

        sked__Region__c region = skedTestDataFactory.createRegion('Sydney','Australia/Sydney');
        insert region;

        sked__Location__c location = skedTestDataFactory.createLocation('Test Location', account.Id, region.Id);
        insert location;

        Contact client = skedTestDataFactory.createContact(account.Id, 'Test Client', 'Client', region.Id);
        insert client;
        
        sked__Job__c job = skedTestDataFactory.createJobs(account.Id, region.Id, null, 2).get(0);
        job.sked__Contact__c = client.Id;
        job.sked__Duration__c = 23*60;
        job.sked__Finish__c = job.sked__Start__c.addMinutes(23*60);
        insert job; 
        
		skedCommonModels.SlotModel slotMOdel = new skedCommonModels.SlotModel(job, job.sked__Start__c.addMinutes(-1), job.sked__Finish__c.addMinutes(1));
		String jsonStr = JSON.serialize(slotModel);
	    test.starttest();
		enrtcr__Alert__c alert = new enrtcr__Alert__c(enrtcr__Client__c=client.Id);
		insert alert;

		delete alert;

		test.stoptest();
	}

	static testmethod void test_skedSingleJobAvailableserviceBatch() {
		
		Test.setMock(HttpCalloutMock.class, new skedHttpCalloutMock(skedHttpCalloutMock.GEOCODE_DATA));
		Account account = skedTestDataFactory.createAccounts('Test Account', 'Clinic', 1).get(0);
        insert account;

        sked__Region__c region = skedTestDataFactory.createRegion('Sydney','Australia/Sydney');
        insert region;

        sked__Location__c location = skedTestDataFactory.createLocation('Test Location', account.Id, region.Id);
        insert location;

        Contact client = skedTestDataFactory.createContact(account.Id, 'Test Client', 'Client', region.Id);
        insert client;
        
        sked__Job__c job = skedTestDataFactory.createJobs(account.Id, region.Id, null, 2).get(0);
        job.sked__Contact__c = client.Id;
        insert job; 
        
	    test.starttest();
	    Test.setMock(HttpCalloutMock.class, new skedHttpCalloutMock(skedHttpCalloutMock.ATTEENDEE_CLIENT));

        skedJobTriggerHandler.IsFromGenerateAttendee = false;
		job.sked__Job_Status__c = 'Dispatched';
		update job;

		test.stoptest();
	}

	static testmethod void test_skedCommonModels() {
		
		insert new sked_Roster_Setting__c (SetupOwnerId=UserInfo.getOrganizationId(), Period__c =14, Start_Date__c =System.now(), No_Of_Days_Before_Next_Roster__c =0, Timesheet_Type__c ='fortnight');
		Test.setMock(HttpCalloutMock.class, new skedHttpCalloutMock(skedHttpCalloutMock.GEOCODE_DATA));
        
                skedTestDataFactory.setupCustomSettings();
                Test.setMock(HttpCalloutMock.class, new skedHttpCalloutMock(skedHttpCalloutMock.GEOCODE_DATA));

                Account account = skedTestDataFactory.createAccounts('Test Account', 'Clinic', 1).get(0);
                insert account;

                sked__Region__c region = skedTestDataFactory.createRegion('Sydney','Australia/Sydney');
                insert region;

                sked__Location__c location = skedTestDataFactory.createLocation('Test Location', account.Id, region.Id);
                insert location;

                Contact client = skedTestDataFactory.createContact(account.Id, 'Test Client', 'Client', region.Id);
                insert client;
                
                sked__Job__c job = skedTestDataFactory.createJobs(account.Id, region.Id, null, 2).get(0);
                job.sked__Contact__c = client.Id;
                job.sked__Duration__c = 23*60;
                job.sked__Finish__c = job.sked__Start__c.addMinutes(23*60);
                insert job; 

		skedCommonModels.SlotModel slotMOdel = new skedCommonModels.SlotModel(job, job.sked__Start__c.addMinutes(-1), job.sked__Finish__c.addMinutes(1));
		String jsonStr = JSON.serialize(slotModel);

		skedCommonModels.getRegions(region.name);
		skedCommonModels.getLocations(location.Name, location.sked__region__c);
		skedCommonModels.searchSites('test');
		skedCommonModels.getAbortReasons();
		skedCommonModels.getResources('test');
		skedCommonModels.toDateTime('2017-08-08 8:00');
		skedCommonModels.Item item = new skedCommonModels.Item(job.Id, 'test label', 20);
		
		skedCommonModels.getNextRoster();
		skedCommonModels.TimeModel tm = new skedCommonModels.TimeModel(1200,'12:00');
		skedCommonModels.ActionResult ar = new skedCommonModels.ActionResult(false, '', '');
		
		job.sked__Notes_Comments__c = 'test';
                test.starttest();
		update job;

		//delete alert;
		test.stoptest();
	}

	@isTest
	static void test_skedTag(){
		Account account = skedTestDataFactory.createAccounts('Test Account', 'Clinic', 1).get(0);
        insert account;

        sked__Region__c region = skedTestDataFactory.createRegion('Sydney','Australia/Sydney');
        insert region;

        sked__Tag__c tag = skedTestDataFactory.createTag('Skill');
        insert tag;

        Contact client = skedTestDataFactory.createContact(account.Id, 'Test Client', 'Client', region.Id);
        insert client;
        
        sked__Job__c job = skedTestDataFactory.createJobs(account.Id, region.Id, null, 2).get(0);
        job.sked__Contact__c = client.Id;
        insert job; 

        sked__Job_Tag__c jobTag = skedTestDataFactory.createJobTag(job.id, tag.Id);
        insert jobTag;

        Client_Tag__c clientTag = skedTestDataFactory.createClientTag(client.Id, tag.Id);
        insert clientTag;

        skedTag jt = new skedTag(jobTag);
        skedTag ct = new skedTag(clientTag);
        skedTag tag2 = new skedTag(tag);
	}
	
	@isTest
	static void test_skedAddressOption(){
		skedAddressOption option = new skedAddressOption();
		System.debug(option.address);
		System.debug(option.locationId);
		System.debug(option.addressID);
		System.debug(option.regionID);
		System.debug(option.pcode);
		System.debug(option.rickassessmnet);
		System.debug(option.lng);
		System.debug(option.lat);
	}
	
	@isTest
	static void test_skedTriggerHandler(){
		insert new sked_Roster_Setting__c (SetupOwnerId=UserInfo.getOrganizationId(), Period__c =14, Start_Date__c =System.now(), No_Of_Days_Before_Next_Roster__c =0, Timesheet_Type__c ='fortnight');
		Test.setMock(HttpCalloutMock.class, new skedHttpCalloutMock(skedHttpCalloutMock.GEOCODE_DATA));
        
                skedTestDataFactory.setupCustomSettings();
                Test.setMock(HttpCalloutMock.class, new skedHttpCalloutMock(skedHttpCalloutMock.GEOCODE_DATA));

                Account account = skedTestDataFactory.createAccounts('Test Account', 'Clinic', 1).get(0);
                insert account;

                sked__Region__c region = skedTestDataFactory.createRegion('Sydney','Australia/Sydney');
                insert region;

                sked__Location__c location = skedTestDataFactory.createLocation('Test Location', account.Id, region.Id);
                insert location;

                enrtcr__Site__c site = skedTestDataFactory.createSite('Test Site');
                insert site;

                Contact client = skedTestDataFactory.createContact(account.Id, 'Test Client', 'Client', region.Id);
                insert client;

                enrtcr__Service__c service = skedTestDataFactory.createService('Test Service');
                insert service;

                enrtcr__Site_Service__c siteService =  skedTestDataFactory.createSiteServiceProgram(site.Id, service.Id, null);
                insert siteService;

                enrtcr__Support_Contract__c sa =  skedTestDataFactory.createServiceAgreement(client.Id, 'Block Grant');
                insert sa;

                enrtcr__Reference_Data__c rd = skedTestDataFactory.createReferenceData('Rate Type','NDIS');
                insert rd;

                enrtcr__Rate__c rate = skedTestDataFactory.createRate(service.Id, rd.Id);
                insert rate;

                enrtcr__Support_Contract_Item__c sai = skedTestDataFactory.createServiceAgreementItem(service.Id, sa.Id, 'Item');
                sai.enrtcr__Rate__c = rate.Id;
                sai.enrtcr__Site_Service_Program__c = siteService.Id;
                sai.enrtcr__Site__c = site.Id;
                insert sai;

                sked__Job__c job = skedTestDataFactory.createJobs(account.Id, region.Id, null, 2).get(0);
                job.sked__Contact__c = client.Id;
                job.Service__c = service.Id;
                job.Service_Agreement__c = sa.Id;
                job.Service_Agreement_Item__c = sai.Id;
                job.Delivery_Method__c = 'Scheduled Time';
                job.Site__c = site.Id;
                job.sked__Job_Status__c = 'Pending Allocation';
                insert job; 

                sked__Resource__c resource = skedTestDataFactory.createResource('Test', UserInfo.getUserId(), region.Id);
                insert resource;

                Job_Service_Item__c jsi = [select id, Delivery_Method__c, Cancellation_reason__c, Group_Attendee__c, Group_Attendee__r.Contact__c,
                                            Group_Attendee__r.Attended__c, Group_Attendee__r.Cancellation_Fee__c, Cancellation__c,
                                            job__r.Service__c, Service_Agreement_Item__r.enrtcr__Rate__c, Service_Agreement_Item__r.enrtcr__Rate__r.enrtcr__Quantity_Type__c, job__r.Rate__c, job__r.Completed_Date__c,
                                            Worker__r.sked__user__c, Service_Agreement_Item__c, Service__c, Rate__c, Comment__c, Travel_Kms__c,
                                            Quantity__c, Worker__c, job__c, job__r.sked__Duration__c, job__r.Service_Agreement__c,
                                            job__r.sked__contact__c, job__r.sked__Start__c, job__r.sked__Job_Status__c, job__r.sked__Actual_End__c, Site_Service_Program__c, Site__c, 
                                            Service_Agreement_Item__r.enrtcr__Support_Contract__c
                							from Job_Service_Item__c where Job__c = :job.Id limit 1];
                System.assertNotEquals(jsi.Service__c, null);
                System.assertNotEquals(jsi.Service_Agreement_Item__c, null);
                jsi.Quantity__c = 2;
                jsi.Worker__c = resource.Id;
                jsi.Site__c = site.Id;
                jsi.Site_Service_Program__c = siteService.Id;
                jsi.Rate__c = rate.Id;
                update jsi;

                Test.startTest();

                sked__Job_Allocation__c ja = skedTestDataFactory.allocateJob(job.Id, resource.Id);
                ja.sked__Status__c = 'Confirmed';
                ja.sked__Time_In_Progress__c = System.now();
                ja.sked__Time_Completed__c = System.now().addHours(1);
                insert ja;

                skedTriggerHandler.getWokerActualTime(ja);

                ja.sked__Status__c = 'Complete';
                update ja;

                skedJobTriggerHandler.IsFromGenerateAttendee = false;
                job.sked__Job_Status__c = 'Complete';
                job.sked__Notes_Comments__c = 'Test Notes';
                //try{
                update job;
                //}catch(Exception ex){}
                
                skedTriggerHandler handler = new skedTriggerHandler();
                handler.supportDeliveryFromGroupJobServiceItem(jsi, new enrtcr__Support_Delivered__c(), ja);
                handler.supportDeliveryFromServiceItemAllocations(jsi, new list<sked__Job_Allocation__c>{ja}, false);

                skedTriggerHandler.populateEventID(new list<enrtcr__Support_Delivered__c>{ new enrtcr__Support_Delivered__c( Job__c = job.Id) });
                
                jsi.Quantity__c = 60;
                update jsi;
        	delete jsi;

                Test.stopTest();
	}

	@isTest
	static void test_skedTimezoneUtil(){
		Datetime d1 = System.now();
		Datetime d2 = System.now().addDays(1);
		skedTimezoneUtil.minutesBetweenDateTimes(d1, d2);
		skedTimezoneUtil.minutesBetweenDateTimes1(d1, d2);
		skedTimezoneUtil.addDays(System.now(), 5, 'Australia/Brisbane');
	}

    @isTest
    static void test_skedHistoricalCaseNoteUpdateBatch(){
        Account account = skedTestDataFactory.createAccounts('Test Account', 'Clinic', 1).get(0);
        insert account;

        sked__Region__c region = skedTestDataFactory.createRegion('Sydney','Australia/Sydney');
        insert region;

        sked__Location__c location = skedTestDataFactory.createLocation('Test Location', account.Id, region.Id);
        insert location;

        Contact client = skedTestDataFactory.createContact(account.Id, 'Test Client', 'Client', region.Id);
        insert client;
        
        sked__Job__c job = skedTestDataFactory.createJobs(account.Id, region.Id, null, 2).get(0);
        job.sked__Contact__c = client.Id;
        job.sked__Duration__c = 23*60;
        job.sked__Finish__c = job.sked__Start__c.addMinutes(23*60);
        insert job;

        skedHistoricalCaseNoteUpdateBatch batchJob = new skedHistoricalCaseNoteUpdateBatch(new Set<Id>{job.Id});
        Database.executeBatch(batchJob, 1);
    }

    @isTest
    static void test_handleSDInsertError(){
        Account account = skedTestDataFactory.createAccounts('Test Account', 'Clinic', 1).get(0);
        insert account;

        sked__Region__c region = skedTestDataFactory.createRegion('Sydney','Australia/Sydney');
        insert region;

        sked__Location__c location = skedTestDataFactory.createLocation('Test Location', account.Id, region.Id);
        insert location;

        Contact client = skedTestDataFactory.createContact(account.Id, 'Test Client', 'Client', region.Id);
        insert client;
        
        sked__Job__c job = skedTestDataFactory.createJobs(account.Id, region.Id, null, 2).get(0);
        job.sked__Contact__c = client.Id;
        job.sked__Duration__c = 23*60;
        job.sked__Finish__c = job.sked__Start__c.addMinutes(23*60);
        insert job;

        try{
            insert new sked__Job__c();
        }catch(Exception ex){
            skedTriggerHandler.handleSDInsertError(ex, new list<String>{job.Id});
        }
    }
}