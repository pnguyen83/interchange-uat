public class skedConfigs {
	
	public static String LOCATION_OR_SITE_FILTER{
        get{
            if(skedConfigs__c.getAll().containsKey('RM_LocationOrSiteFilter')){
                LOCATION_OR_SITE_FILTER     = skedConfigs__c.getAll().get('RM_LocationOrSiteFilter').Value__c;
            }else LOCATION_OR_SITE_FILTER   = 'Location';
            return LOCATION_OR_SITE_FILTER;
        }
        set;
    }

    public static String JOB_URL{
        get{
            if(skedConfigs__c.getAll().containsKey('Global_JobURL')){
                JOB_URL     = skedConfigs__c.getAll().get('Global_JobURL').Value__c;
            }else JOB_URL   = '/';
            return JOB_URL;
        }
        set;
    }

    public static Integer SCHEDULE_START{
        get{
            if(skedConfigs__c.getAll().containsKey('RM_Schedule_Start')){
                SCHEDULE_START     = Integer.valueOf(skedConfigs__c.getAll().get('RM_Schedule_Start').Value__c);
            }else SCHEDULE_START   = 0;
            return SCHEDULE_START;
        }
        set;
    }

    public static Integer SCHEDULE_END{
        get{
            if(skedConfigs__c.getAll().containsKey('RM_Schedule_End')){
                SCHEDULE_END     = Integer.valueOf(skedConfigs__c.getAll().get('RM_Schedule_End').Value__c);
            }else SCHEDULE_END   = 2300;
            return SCHEDULE_END;
        }
        set;
    }

    public static Integer SCHEDULE_INTERVAL{
        get{
            if(skedConfigs__c.getAll().containsKey('RM_Schedule_Interval')){
                SCHEDULE_INTERVAL     = Integer.valueOf(skedConfigs__c.getAll().get('RM_Schedule_Interval').Value__c);
            }else SCHEDULE_INTERVAL   = 60;
            return SCHEDULE_INTERVAL;
        }
        set;
    }

    public static String SKEDULO_API_TOKEN{
        get{
            if(skedConfigs__c.getAll().containsKey('Global_SkeduloAPIToken')){
                SKEDULO_API_TOKEN     = skedConfigs__c.getAll().get('Global_SkeduloAPIToken').Value__c;
            }else SKEDULO_API_TOKEN   = '';
            return SKEDULO_API_TOKEN;
        }
        set;
    }

    public static String GOOGLE_API_KEY{
        get{
            if(GOOGLE_API_KEY == null){
                if(skedConfigs__c.getAll().containsKey('Global_GoogleAPIKEY')){
                    GOOGLE_API_KEY     = skedConfigs__c.getAll().get('Global_GoogleAPIKEY').Value__c;
                }else GOOGLE_API_KEY   = '';
            }
            return GOOGLE_API_KEY;
        }
    }

    public static String GEOCODING_SERVICE{
        get{
            if(GEOCODING_SERVICE == null){
                if(skedConfigs__c.getAll().containsKey('Geocoding_Service')){
                    GEOCODING_SERVICE     = skedConfigs__c.getAll().get('Geocoding_Service').Value__c;
                }else GEOCODING_SERVICE   = 'Skedulo';
            }
            return GEOCODING_SERVICE;
        }
    }

    public static String RESOURCE_DISTANCE{
        get{
            if(skedConfigs__c.getAll().containsKey('Global_ResourceDistance')){
                RESOURCE_DISTANCE     = skedConfigs__c.getAll().get('Global_ResourceDistance').Value__c;
            }else RESOURCE_DISTANCE   = 'First';
            return RESOURCE_DISTANCE;
        }
        set;
    }

    public static String DISTANCE_TRAVELLED_SPLIT{
        get{
            if(skedConfigs__c.getAll().containsKey('Global_DistanceTravelledSplit')){
                DISTANCE_TRAVELLED_SPLIT     = skedConfigs__c.getAll().get('Global_DistanceTravelledSplit').Value__c;
            }else DISTANCE_TRAVELLED_SPLIT   = 'All';
            return DISTANCE_TRAVELLED_SPLIT;
        }
        set;
    }

    public static boolean SHOW_ESTIMATED_DISTANCE_TRAVELLED{
        get{
            if(skedConfigs__c.getAll().containsKey('Global_ShowEstimatedDistanceTravelled')){
                SHOW_ESTIMATED_DISTANCE_TRAVELLED     = Boolean.valueOf(skedConfigs__c.getAll().get('Global_ShowEstimatedDistanceTravelled').Value__c);
            }else SHOW_ESTIMATED_DISTANCE_TRAVELLED   = false;
            return SHOW_ESTIMATED_DISTANCE_TRAVELLED;
        }
        set;
    }

    public static String RESOURCE_DISTANCE_FIRST    = 'First';
    public static String RESOURCE_DISTANCE_COMBINED    = 'Combined';
    public static String DISTANCE_TRAVELLED_SPLIT2    = 'Split';
    public static String DISTANCE_TRAVELLED_ALL    = 'All';

    public static String DEFAULT_DELIVERY_METHOD{
        get{
            if(skedConfigs__c.getAll().containsKey('Global_DefaultDeliveryMethod')){
                DEFAULT_DELIVERY_METHOD     = skedConfigs__c.getAll().get('Global_DefaultDeliveryMethod').Value__c;
            }else DEFAULT_DELIVERY_METHOD   = '';
            return DEFAULT_DELIVERY_METHOD;
        }
        set;
    }

    public static boolean ENABLE_RECURRING_JOB{
        get{
            if(skedConfigs__c.getAll().containsKey('Enable_Recurring_Job')){
                ENABLE_RECURRING_JOB     = Boolean.valueOf(skedConfigs__c.getAll().get('Enable_Recurring_Job').Value__c);
            }else ENABLE_RECURRING_JOB   = false;
            return ENABLE_RECURRING_JOB;
        }
        set;
    }

    public static boolean SHOW_REQUIRED_RESOURCES{
        get{
            if(skedConfigs__c.getAll().containsKey('Show_Required_Resources')){
                SHOW_REQUIRED_RESOURCES     = Boolean.valueOf(skedConfigs__c.getAll().get('Show_Required_Resources').Value__c);
            }else SHOW_REQUIRED_RESOURCES   = false;
            return SHOW_REQUIRED_RESOURCES;
        }
        set;
    }

    public static boolean SHOW_MAX_PARTICIPANT{
        get{
            if(skedConfigs__c.getAll().containsKey('Show_Max_Participants')){
                SHOW_MAX_PARTICIPANT     = Boolean.valueOf(skedConfigs__c.getAll().get('Show_Max_Participants').Value__c);
            }else SHOW_MAX_PARTICIPANT   = false;
            return SHOW_MAX_PARTICIPANT;
        }
        set;
    }

    public static boolean LOCK_EVENT{
        get{
            if(skedConfigs__c.getAll().containsKey('Lock_Event')){
                LOCK_EVENT     = Boolean.valueOf(skedConfigs__c.getAll().get('Lock_Event').Value__c);
            }else LOCK_EVENT   = false;
            return LOCK_EVENT;
        }
        set;
    }

    public static String JSON_GEN_STATUS{
        get{
            if(skedConfigs__c.getAll().containsKey('JSON_Gen_Status')){
                JSON_GEN_STATUS     = skedConfigs__c.getAll().get('JSON_Gen_Status').Value__c;
            }else JSON_GEN_STATUS   = 'Dispatched';
            return JSON_GEN_STATUS;
        }
        set;
    }

    public static boolean ENABLE_EC_SERVICE{
        get{
            if(skedConfigs__c.getAll().containsKey('Enable_EC_Service')){
                ENABLE_EC_SERVICE     = Boolean.valueOf(skedConfigs__c.getAll().get('Enable_EC_Service').Value__c);
            }else ENABLE_EC_SERVICE   = false;
            return ENABLE_EC_SERVICE;
        }
        set;
    }

    public static String SHOW_CASE_NOTES{
        get{
            if(skedConfigs__c.getAll().containsKey('Visible_Case_Notes')){
                SHOW_CASE_NOTES     = skedConfigs__c.getAll().get('Visible_Case_Notes').Value__c;
            }else SHOW_CASE_NOTES   = 'Clinical_Note';
            return SHOW_CASE_NOTES;
        }
        set;
    }

    public static String INVALID_SA_EMAIL_LIST{
        get{
            if(skedConfigs__c.getAll().containsKey('Invalid_SA_Email_List')){
                INVALID_SA_EMAIL_LIST     = skedConfigs__c.getAll().get('Invalid_SA_Email_List').Value__c;
                if(!String.isBlank(INVALID_SA_EMAIL_LIST)) INVALID_SA_EMAIL_LIST = INVALID_SA_EMAIL_LIST.replaceAll(' ','');
            }else INVALID_SA_EMAIL_LIST   = 'Owner';
            return INVALID_SA_EMAIL_LIST;
        }
        set;
    }

    public static Decimal MIN_WORKERS_ACTUAL_TIME{
        get{
            if(skedConfigs__c.getAll().containsKey('Min_Workers_Actual_Time')){
                MIN_WORKERS_ACTUAL_TIME     = Decimal.valueOf(skedConfigs__c.getAll().get('Min_Workers_Actual_Time').Value__c);
            }else MIN_WORKERS_ACTUAL_TIME   = 0.12;
            return MIN_WORKERS_ACTUAL_TIME;
        }
        set;
    }

    public static String SERVICE_DELIVERED_DATE{
        get{
            if(skedConfigs__c.getAll().containsKey('Service_Delivered_Date')){
                SERVICE_DELIVERED_DATE     = skedConfigs__c.getAll().get('Service_Delivered_Date').Value__c;
            }else SERVICE_DELIVERED_DATE   = 'Job';
            return SERVICE_DELIVERED_DATE;
        }
        set;
    }
}