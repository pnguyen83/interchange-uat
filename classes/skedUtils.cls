public class skedUtils {
    
    public static string TIMESLOT_KEY_FORMAT = 'dd/MM/yyyy hh:mm a';
    public static string DATE_FORMAT = 'yyyy-MM-dd';
    public static string DATE_ISO_FORMAT = '"yyyy-MM-dd"';
    
    public static integer GetDifferentDays(string fromDateIso, string toDateIso) {
        Date fromDate = (Date)Json.deserialize(fromDateIso, Date.class);
        Date toDate = (Date)Json.deserialize(toDateIso, Date.class);
        
        return fromDate.daysBetween(toDate);
    }
    
    public static DateTime GetStartOfDate(string dateIsoString, string timezoneSidId) {
        Date tempDate = (Date)Json.deserialize(dateIsoString, Date.class);
        DateTime result = DateTime.newInstance(tempDate, time.newInstance(0, 0, 0, 0));
        result = skedUtils.ConvertBetweenTimezones(result, timezoneSidId, UserInfo.getTimeZone().getID());
        return result;
    }
    
    public static DateTime GetStartOfDate(DateTime input, string timezoneSidId) {
        String dateIsoString = input.format(DATE_ISO_FORMAT, timezoneSidId);
        return GetStartOfDate(dateIsoString, timezoneSidId);
    }
    
    public static DateTime GetEndOfDate(DateTime input, string timezoneSidId) {
        return GetStartOfDate(input, timezoneSidId).addDays(1);
    }
    
    public static string ConvertDateToIsoString(Date input) {
        string result = Json.serialize(input).replace('"', '');
        return result;
    }
    
    public static Datetime convertToTimezone(Datetime currentDT, string TZ) {
        Timezone SetTZ = Timezone.getTimeZone(TZ); // get target timezone  
        Timezone currentTZ = Userinfo.getTimeZone(); // get current user timezone

        integer setOffSet = SetTZ.getoffset(currentDT); //calculate target timezone offset
        integer curOffSet = currentTZ.getoffset(currentDT); //calculate current timezone offset

        integer resOffSet = curOffSet - setOffSet;

        integer offSet = resOffSet / (1000);
        DateTime localDT = currentDT.addSeconds(offSet);
        return localDT;
    }
    
    public static DateTime addDays(DateTime inputDt, integer days, string timezoneSidId) {
        DateTime result = inputDt.addDays(days);
        Timezone tz = Timezone.getTimezone(timezoneSidId);
        integer inputDtOffset = tz.getOffset(inputDt) / 60000;
        integer resultOffset = tz.getOffset(result) / 60000;
        
        result = result.addMinutes(inputDtOffset - resultOffset);
        
        return result;
    }
    
    public static Date ConvertToDateValue(string dateString) {
        String[] temp = dateString.split('-');
        return Date.newInstance(Integer.valueOf(temp[0]), Integer.valueOf(temp[1]), Integer.valueOf(temp[2]));
    }
    
    public static DateTime ConvertBetweenTimezones(DateTime input, string fromTimezoneSidId, string toTimezoneSidId) {
        if (fromTimezoneSidId == toTimezoneSidId) {
            return input;
        }
        TimeZone fromTz = Timezone.getTimeZone(fromTimezoneSidId);
        Timezone toTz = Timezone.getTimeZone(toTimezoneSidId);
        integer offsetMinutes = toTz.getOffset(input) - fromTz.getOffset(input);
        offsetMinutes = offsetMinutes / 60000;
        input = input.addMinutes(offsetMinutes);
        return input;
    }
 
    // @param inputTime: 730
    // @result: 730 will be considered as 7 hour and 30 minutes so the result should be 7 * 60 + 30 = 450
    public static integer ConvertTimeNumberToMinutes(integer inputTime) {
        return integer.valueOf(inputTime / 100) * 60 + Math.mod(inputTime, 100);
    }
    
    public static Set<Date> getHolidaysByRegion(Id regionId) {
        Set<Date> allHolidays = new Set<Date>();
        
        Map<string, Set<Date>> mapHolidays = skedUtils.getHolidays(system.today());
        if (mapHolidays.containsKey(skedConstants.HOLIDAY_GLOBAL)) {
            Set<Date> globalHolidays = mapHolidays.get(skedConstants.HOLIDAY_GLOBAL);
            allHolidays.addAll(globalHolidays);
        }
        if (mapHolidays.containsKey(regionId)) {
            Set<Date> regionHolidays = mapHolidays.get(regionId);
            allHolidays.addAll(regionHolidays);
        }
        
        return allHolidays;
    }

    public static Set<Date> getRegionHolidays(Date startDate, string regionName) {
        Map<string, Set<Date>> mapHoliday = skedUtils.getHolidays(startDate);
        Set<Date> allHolidays = new Set<Date>();
        Set<Date> globalHoidays = mapHoliday.get('global');
        allHolidays.addAll(globalHoidays);
        if (mapHoliday.containsKey(regionName)) {
            Set<Date> regionHolidays = mapHoliday.get(regionName);
            allHolidays.addAll(regionHolidays);
        }
        return allHolidays;
    }
    
    public static Map<string, Set<Date>> getHolidays(Date currentDate) {
        Map<string, Set<Date>> mapHolidays = new Map<string, Set<Date>>();
        List<sked__Holiday__c> skedGlobalHolidays = [SELECT Id, sked__Start_Date__c, sked__End_Date__c
                                                     FROM sked__Holiday__c
                                                     WHERE sked__Global__c = TRUE
                                                     AND sked__End_Date__c >= :currentDate];
        List<sked__Holiday_Region__c> skedRegionHolidays = [SELECT Id, sked__Holiday__r.sked__Start_Date__c, sked__Holiday__r.sked__End_Date__c, 
                                                            sked__Region__r.Name
                                                            FROM sked__Holiday_Region__c
                                                            WHERE sked__Holiday__r.sked__End_Date__c >= :currentDate];
        
        Set<Date> globalHolidays = new Set<Date>();
        for (sked__Holiday__c globalHoliday : skedGlobalHolidays) {
            Date tempDate = globalHoliday.sked__Start_Date__c;
            while (tempDate <= globalHoliday.sked__End_Date__c) {
                globalHolidays.add(tempDate);
                tempDate = tempDate.addDays(1);
            }
        }
        mapHolidays.put('global', globalHolidays);
        
        for (sked__Holiday_Region__c regionHoliday : skedRegionHolidays) {
            Set<Date> regionHolidays;
            if (mapHolidays.containsKey(regionHoliday.sked__Region__r.Name)) {
                regionHolidays = mapHolidays.get(regionHoliday.sked__Region__r.Name);
            } else {
                regionHolidays = new Set<Date>();
            }
            
            Date tempDate = regionHoliday.sked__Holiday__r.sked__Start_Date__c;
            while (tempDate <= regionHoliday.sked__Holiday__r.sked__End_Date__c) {
                regionHolidays.add(tempDate);
                tempDate = tempDate.addDays(1);
            }
            
            if (!mapHolidays.containsKey(regionHoliday.sked__Region__r.Name)) {
                mapHolidays.put(regionHoliday.sked__Region__r.Name, regionHolidays);
            }
        }
        return mapHolidays;
    }
    
    public static integer daySequenceOfWeek(string weekday) {
        string lowerCaseWeekDay = weekday.toLowerCase();
        return lowerCaseWeekDay == 'mon' ? 1 : 
                lowerCaseWeekDay == 'tue' ? 2 : 
                lowerCaseWeekDay == 'wed' ? 3 : 
                lowerCaseWeekDay == 'thu' ? 4 : 
                lowerCaseWeekDay == 'fri' ? 5 : 
            lowerCaseWeekDay == 'sat' ? 6 : 7;
    }

    public static DateTime addMinutes(DateTime inputDt, integer minutes, string timezoneSidId) {
        DateTime result = inputDt.addMinutes(minutes);
        Timezone tz = Timezone.getTimezone(timezoneSidId);  
        //For America/Los_Angeles timezone, offset may be different depend on DateTime input, 
        //especial switch from DatelightSaving to standard time.
        integer inputDtOffset = tz.getOffset(inputDt) / 60000;       
        integer resultOffset = tz.getOffset(result) / 60000;          
        result = result.addMinutes(inputDtOffset - resultOffset);
        
        return result;
    }

    public static string CombineAddress(string street, string city, string state, string postalCode, string country) {
        string address = street;
        if (!string.isBlank(city)) {
            address += ', ' + city;
        }
        if (!string.isBlank(state)) {
            address += ', ' + state;
        }
        if (!string.isBlank(postalCode)) {
            address += ' ' + postalCode;
        }
        if (!string.isBlank(country)) {
            address += ', ' + country;
        }
        return address;
    }

    public static integer increaseDays(integer days, boolean isIncreasing, DateTime inputDt, string timezoneSidId, boolean excludeSats, 
                                                            boolean excludeSuns, Set<Date> skipDates) {
        DateTime tempDt = inputDt.addDays(days);
        string weekday = tempDt.format('EEE', timezoneSidId).toLowerCase();
        if (weekday == 'sat' && excludeSats) {
            days = isIncreasing ? days + 1 : days - 1;
        } 
        else if (weekday == 'sun' && excludeSuns) {
            days = isIncreasing ? days + 1 : days - 1;
        } 
        else if(skipDates.contains(tempDt.date())) {
            days = isIncreasing ? days + 1 : days - 1;
        }
        else {
            return days;
        }
        return increaseDays(days, isIncreasing, inputDt, timezoneSidId, excludeSats, excludeSuns, skipDates);
    }

    public static boolean isLeapYear(integer year) {
        boolean leapYear;
        if (Math.mod(year, 4) == 0) {
            if (Math.mod(year, 4) == 100) {
                if (Math.mod(year, 4) == 400) {
                    leapYear = true;
                }
                else {
                    leapYear = false;
                }
            } 
            else {
                leapYear = true;
            }
        } 
        else {
            leapYear = false;
        }
        return leapYear;
    }

    public static integer getValidDay(integer day, integer month, integer year) {
        integer result = day;
        
        boolean leapYear = isLeapYear(year);
        Set<integer> monthWith30days = new Set<integer> {4, 6, 9, 11}; 
            
            if (month == 2) {
                if (leapYear) {
                    if (result > 29) {
                        result = 29;
                    }
                }
                else {
                    if (result > 28) {
                        result = 28;
                    }
                }
            }
        else if (monthWith30days.contains(month)) {
            if (result > 30) {
                result = 30;
            }
        }
        
        return result;
    }

    public static Time getTime(string timeString)
    {
        list<string> hourTime = timeString.split(':');

        Integer hours = Integer.valueOf(hourTime[0])
                + Integer.valueOf((hourTime[1].right(2) == 'pm') ? '12' : '0');
        Integer minutes = Integer.valueOf(hourTime[1].left(2));

        return time.newInstance(hours, minutes, 0, 0);
    }
    
    public static DateTime addYears(DateTime inputDt, integer years, string timezoneSidId) {
        DateTime result = inputDt.addYears(years);
        Timezone tz = Timezone.getTimezone(timezoneSidId);
        integer inputDtOffset = tz.getOffset(inputDt) / 60000;
        integer resultOffset = tz.getOffset(result) / 60000;
        
        result = result.addMinutes(inputDtOffset - resultOffset);
        
        return result;
    }
}