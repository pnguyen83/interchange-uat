public class skedGroupClient
{
    public string id { get; set; }
    public string groupId { get; set; }
    public string accId { get; set; }
    public string contactNumber { get; set; }
    public string name { get; set; }
    public string label { get; set; }
    public string value { get; set; }
    public string account { get; set; }
    public string birthDate { get; set; }
    public string category { get; set; }
    public string categoryName { get; set; }
    public string serviceAgreementItem { get; set; }
    public enrtcr.ListOption service { get; set; }
    public enrtcr.ListOption site { get; set; }
    public enrtcr.ListOption program { get; set; }
    public enrtcr.ListOption rate { get; set; }
    public string matchingItem { get; set; }
    public string startDate { get; set; }
    public string endDate { get; set; }
    public string status { get;set; }
    public string deliveryMethod {get;set;}
    public decimal quantity {get; set;}
    public List<string> blacklist {get;set;}

    public string serviceAgreementItemName { get; set; }
    public List<skedTag> tags {get;set;}
}