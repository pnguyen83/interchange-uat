public class SkeduloConstants {
    
    public static final string DATE_PARSE_FORMAT = 'M/d/yyyy';
    
    public static final string JOB_STATUS_CANCELLED = 'Cancelled';
    public static final string JOB_STATUS_COMPLETE = 'Complete';
    public static final string JOB_STATUS_DISPATCHED = 'Dispatched';
    public static final string JOB_STATUS_IN_PROGRESS = 'In Progress';
    public static final string JOB_STATUS_PENDING_ALLOCATION = 'Pending Allocation';
    public static final string JOB_STATUS_QUEUED = 'Queued';
    public static final string JOB_STATUS_READY = 'Ready';
    public static final string JOB_STATUS_ON_SITE = 'On Site';
    public static final string JOB_STATUS_EN_ROUTE = 'En Route';
    
    public static final string ALLOCATION_STATUS_NO_ALLOCATION = 'No Allocation';
    public static final string ALLOCATION_STATUS_PARTIALLY_ALLOCATED = 'Partially Allocated';
    public static final string ALLOCATION_STATUS_FULLY_ALLOCATED = 'Fully Allocated';
    
    public static final string JOB_ALLOCATION_STATUS_COMPLETE = 'Complete';
    public static final string JOB_ALLOCATION_STATUS_CONFIRMED = 'Confirmed';
    public static final string JOB_ALLOCATION_STATUS_DECLINED = 'Declined';
    public static final string JOB_ALLOCATION_STATUS_DELETED = 'Deleted';
    public static final string JOB_ALLOCATION_STATUS_DISPATCHED = 'Dispatched';
    public static final string JOB_ALLOCATION_STATUS_IN_PROGRESS = 'In Progress';
    public static final string JOB_ALLOCATION_STATUS_PENDING_DISPATCH = 'Pending Dispatch';
    
    public static final string RESOURCE_ROLE_REMOVALIST = 'Removalist';
    public static final string RESOURCE_ROLE_DIVER = 'Diver';
    public static final string RESOURCE_ROLE_PORTER_TEAM_LEADER = 'Porter team Leader';
    public static final string RESOURCE_ROLE_TECH_PORTER_ITFM = 'Tech Porter - ITFM';
    public static final string RESOURCE_ROLE_SERVER_PORTER_ITFM = 'Server Porter - ITFM';
    public static final string RESOURCE_ROLE_SERVER_TECH_ITFM = 'Server Tech - ITFM';
    public static final string RESOURCE_ROLE_DESK_TEAM_LEAD_ITFM = 'Desk Team Lead - ITFM';
    public static final string RESOURCE_ROLE_SERVER_TEAM_LEAD_ITFM = 'Server Team Lead - ITFM';
    
    public static final string RESOURCE_TYPE_PERSON = 'Person';
    public static final string RESOURCE_TYPE_ASSET = 'Asset';
    
    public static final string GLOBAL_HOLIDAYS = 'global';
    
    public static final string USER_USERTYPE = '';
    
    public static final string JOB_GROUP_ALLOCATION_STATUS_CONSOLE_PINK = 'pink';
    public static final string JOB_GROUP_ALLOCATION_STATUS_CONSOLE_YELLOW = 'yellow';
    public static final string JOB_GROUP_ALLOCATION_STATUS_CONSOLE_GREEN = 'green';


    public static final string DELIVERY_METHOD_STATUS_FIXED_QUANTITY = 'Fixed Quantity';
    public static final string DELIVERY_METHOD_STATUS_SCHEDULED_TIME = 'Scheduled Time';
    public static final string DELIVERY_METHOD_STATUS_WORKER_ACTUAL_TIME = 'Workers Actual Time';

    public static final string NO_ACTIVE_RESOURCE = 'There is not any active resource in system';
    public static final string NO_VIEW_OPTION_SETTING = 'Cannot get Sked View Setting Options value';
    public static final string NO_STARTING_DATE_OPTION_SETTING = 'Cannot get Sked Starting Date Options value';
    public static final string NO_ACTIVITY_TYPE_SETTING = 'Cannot get Sked Activity Types value';
    public static final string NO_TIMESHEET_SETTING = 'Cannot get Sked Timesheet Settings value';
    public static final string NO_RSOTER_INFORMATION = 'Cannot get roster information';
    public static final string TIMESHEET_NO_REGION = 'Cannot find region for resource';
    public static final string TIMESHEET_INVALID_RESOURCE_ID = 'Invalid resource id';

    public static final string TIMESHEET_COMFIRMATION_MESSAGE = 'Do you wish to submit your timesheet for approval?';
    public static final string TIMESHEET_REJECTION_MESSAGE = 'Do you want to reject this timesheet?';
    public static final string MANAGER_NOT_DEFINED = 'Unable to Submit timesheet as the Resource does not have an assigned manager. Please contact your manager.';
    public static final string ROSTER_SETTING_NOT_DEFINED = 'Unable to get data from Roster Setting custom setting';

    public static final string AVAILABILITY_APPROVED_STATUS = 'Approved';

    public static final string JOB = 'Job';

    public static final string ACTIVITY = 'Activity';

    public static final string AVAILABILITY = 'Availability';
    public static final string UNAVAILABILITY = 'Unavailability';

    public static final string APPROVAL_STATUS = 'Approved';
    public static final string REJECT_STATUS = 'Rejected';

    public static final string ESTIMATE_SPEED_NAME = 'Estimate_Speed_Resource';

    public static final string ACTIVITY_OUTSIDE_ROSTER_PERIOD = 'Cannot create Activities that fall outside this timesheet\'s date range';

    public static final string TIMESHEET_WEEK_TYPE = 'week';
    public static final string TIMESHEET_FORTNIGHT_TYPE = 'fortnight';
}