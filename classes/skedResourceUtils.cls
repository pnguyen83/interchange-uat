public class skedResourceUtils {
	public static String DATE_FORMAT = 'yyyy-MM-dd';
	Date scheduleDate;

    public static list<ShiftAvailableSlot> getAvailabilityList(Set<String> resourceIds, DateTime sTime, DateTime eTime, Integer duration){
        list<ResourceModel> result = getResourceList(resourceIds, sTime, eTime);

        list<ShiftAvailableSlot> slots = new list<ShiftAvailableSlot>();
        
        Map<String,list<ShiftModel>> mapDateToShiftModel = new Map<String,list<ShiftModel>>();
        
        for (ResourceModel provider : result) {
            
            for (DateSlotModel dateSlot : provider.dateSlots) {
                list<ShiftModel> tmpList = new list<ShiftModel>();

                for(ShiftModel shift : dateSlot.shifts){
                    tmpList.addAll(dateSlot.breakShift(shift));
                }
                tmpList = dateSlot.removeDuplicateShifts(tmpList);
                if(!mapDateToShiftModel.containsKey(dateSlot.dateString)){
                    mapDateToShiftModel.put(dateSlot.dateString, tmpList);
                }
                
                else{
                    list<ShiftModel> l = mapDateToShiftModel.get(dateSlot.dateString);
                    l = mergeShifts(tmpList, l);
                    mapDateToShiftModel.put(dateSlot.dateString, l);
                }
                
            }
            
        }
        //Merge shift
        DateSlotModel dateSlot = new DateSlotModel();
        for(String d : mapDateToShiftModel.keySet()){
            list<ShiftModel> tmpList = mapDateToShiftModel.get(d);
            tmpList.sort();
            tmpList = dateSlot.mergeShifts(tmpList);
            mapDateToShiftModel.put(d, tmpList);
        }

        Integer i = 0;
        for(String d : mapDateToShiftModel.keySet()){
            list<ShiftModel> tmpList = mapDateToShiftModel.get(d);
            
            for(ShiftModel shift : tmpList){
                if(shift.isAvailable == true){
                    if(skedTimezoneUtil.toMinute(shift.endTime) - skedTimezoneUtil.toMinute(shift.startTime) < duration) continue;
                    slots.add(new ShiftAvailableSlot(String.valueOf(i), d, shift.startTime, shift.endTime, duration));
                    i++;
                }
            }
        }

        return slots;
    }

    public Set<String> getAvailableResourceIDs(Set<String> resourceIDs, DateTime startTime, Integer duration){
        DateTime endTime = startTime.addMinutes(duration);
         list<skedResourceUtils.ResourceModel> resourceList = skedResourceUtils.getResourceList(resourceIDs, startTime, endTime);
        Set<String> result = new Set<String>();
        for(ResourceModel rm : resourceList){
            if(isAvailable(rm, startTime, duration)) result.add(rm.id);
        }
        return result;
    }

    public static boolean isAvailable(ResourceModel rm, DateTime startTime, Integer duration){
        for (DateSlotModel dateSlot : rm.dateSlots) {
            for(ShiftModel shift : dateSlot.shifts){

            }
        }
        return true;
    }

	public static list<ResourceModel> getResourceList(Set<String> resourceIds, DateTime sTime, DateTime eTime) {
        Date startDate 	    = sTime.date();//Date.newInstance(startTime.year(), startTime.month(), startTime.day());
        Date endDate 	    = eTime.date();//Date.newInstance(endTime.year(), endTime.month(), endTime.day());sTime.addDays(1).date();//
        DateTime bufferedStartTime    = sTime.addDays(-1);
        DateTime bufferedEndTime      = eTime.addDays(1);

        list<sked__Resource__c> resources = [select id, Name,sked__Home_Address__c, sked__Primary_Region__c, sked__GeoLocation__c, sked__GeoLocation__latitude__s, sked__GeoLocation__Longitude__s,
        										(SELECT Id, sked__Start__c, sked__Finish__c, sked__Is_Available__c, sked__Type__c, sked__Timezone__c
                                                 FROM sked__Availabilities1__r
                                                 WHERE sked__Status__c = 'Approved' 
                                                 and  sked__Finish__c <= :bufferedEndTime and sked__Start__c >= :bufferedStartTime
                                                 ORDER BY sked__Start__c ASC),
                                                 (Select Id, Name, sked__Start__c, sked__End__c from sked__Activities__r where sked__End__c <= :bufferedEndTime and sked__Start__c >= :bufferedStartTime
                                                 ORDER BY sked__Start__c ASC),
                                                (SELECT Id, sked__Job__r.Name, sked__Job__r.sked__Start__c, sked__Job__r.sked__Finish__c, sked__Job__r.sked__Address__c, sked__Job__c
												FROM sked__Job_Allocations__r where sked__Job__r.sked__Start__c >= :bufferedStartTime and  sked__Job__r.sked__Finish__c <= :bufferedEndTime
												AND sked__Status__c !='Deleted' and sked__Status__c !='Declined' and sked__Status__c !='Complete')
        										from sked__Resource__c where ID IN :resourceIds
        										AND sked__Home_Address__c != null and sked__Home_Address__c!='' AND sked__Is_Active__c = true];   
        Map<Id,sked__Resource__c> resourceMap = new Map<Id,sked__Resource__c>(resources);

        list<ResourceModel> result = new list<ResourceModel>();
        Map<string, Set<Date>> mapHoliday = new Map<string, Set<Date>>();//getHolidays(startOfWeek);
        
        
        for (sked__Resource__c skedResource : resources) {
            system.debug('===resources: ' + JSON.serialize(skedResource));
            resourceIds.add(skedResource.Id);
            
            ResourceModel provider = new ResourceModel(skedResource);
            Map<string, DateSlotModel> mapDateSlot = provider.InitilizeDateSlots(startDate, endDate, mapHoliday);
            
            for (sked__Availability__c skedAvailability : skedResource.sked__Availabilities1__r) {
                string dayOfWeek = skedAvailability.sked__Start__c.format(DATE_FORMAT);
                if(!mapDateSlot.containsKey(dayOfWeek)) continue;
                DateSlotModel dateSlot = mapDateSlot.get(dayOfWeek);
                ShiftModel shift = new ShiftModel(skedAvailability);
                dateSlot.shifts.add(shift);
            }

            for (sked__Activity__c activity : skedResource.sked__Activities__r) {
                string dayOfWeek = activity.sked__Start__c.format(DATE_FORMAT);
                if(!mapDateSlot.containsKey(dayOfWeek)) continue;
                DateSlotModel dateSlot = mapDateSlot.get(dayOfWeek);
                ShiftModel shift = new ShiftModel(activity);
                dateSlot.shifts.add(shift);
            }
            
            result.add(provider);
        }
        //Loading working time for each day of resources
        Map<Id, Map<string, sked__Availability_Template_Entry__c>> mapResourceWorkingTime = getResourceWorkingTime(resourceIds);
        
        for (ResourceModel provider : result) {
            Map<string, sked__Availability_Template_Entry__c> mapWorkingTime = mapResourceWorkingTime.get(provider.id);
            sked__Resource__c resource = resourceMap.get((Id)(provider.id));

            for (DateSlotModel dateSlot : provider.dateSlots) {
                
                //Load job allocation time
                for(sked__Job_Allocation__c ja : resource.sked__Job_Allocations__r){
                    String dString = ja.sked__Job__r.sked__Start__c.format('yyyy-MM-dd');
                    if(dString != dateSlot.dateString) continue;
                    integer startTime 	= skedTimezoneUtil.convertTimeString2Integer(ja.sked__Job__r.sked__Start__c.format('hh:mm a'));
                	integer endTime 	= skedTimezoneUtil.convertTimeString2Integer(ja.sked__Job__r.sked__Finish__c.format('hh:mm a'));
                    ShiftModel sm = new ShiftModel(ja.sked__Job__c, startTime, endTime, false, ja.sked__Job__r.sked__Address__c);
                    sm.info = ja.sked__Job__r.Name;
                	dateSlot.shifts.add( sm );
                }

                DateTime tempDateTime = DateTime.newInstance(dateSlot.d, Time.newInstance(0, 0, 0, 0));
                string weekDay = tempDateTime.format('E').toUpperCase();
                
                if (mapWorkingTime.containsKey(weekDay)) {
                    //Load availability template entry time
	                sked__Availability_Template_Entry__c workingTimeEntry = mapWorkingTime.get(weekDay);
	                
	                dateSlot.startWorkingTime = integer.valueOf(workingTimeEntry.sked__Start_Time__c);
	                dateSlot.endWorkingTime = integer.valueOf(workingTimeEntry.sked__Finish_Time__c);
                    ShiftModel sm = new ShiftModel(null, dateSlot.startWorkingTime, dateSlot.endWorkingTime, true);
                    sm.info     = 'Availability Template';
	                dateSlot.shifts.add( sm );
                }
                
                dateSlot.mergeShifts();
            }
        }

        
        return result;
    }

    public static list<ShiftModel> mergeShifts(list<ShiftModel> list1, list<ShiftModel> list2){
    	list<ShiftModel> result = new list<ShiftModel>();
    	for(ShiftModel sm1 : list1){
    		for(ShiftModel sm2 : list2){
    			if(sm1.startTime == sm2.startTime && sm1.isAvailable==true && sm2.isAvailable == true) {
    				result.add(sm2);
    				break;
    			}
    		}
    	}
    	return result;
    }
    
    

    private static Map<Id, Map<string, sked__Availability_Template_Entry__c>> getResourceWorkingTime(Set<String> inputResourceIds) {
        List<sked__Availability_Template_Resource__c> tempResList = [SELECT sked__Availability_Template__c, sked__Resource__c
                                                                     FROM sked__Availability_Template_Resource__c
                                                                     WHERE sked__Resource__c IN :inputResourceIds];
        Map<Id, Id> map_ResId_TempId = new Map<Id, Id>();
        for (sked__Availability_Template_Resource__c tempRes : tempResList) {
            map_ResId_TempId.put(tempRes.sked__Resource__c, tempRes.sked__Availability_Template__c);
        }
        
        Map<Id, sked__Availability_Template__c> mapTemplates 
            = new Map<Id, sked__Availability_Template__c>([SELECT Id,
                                                           (SELECT sked__Start_Time__c, sked__Finish_Time__c, sked__Is_Available__c, sked__Weekday__c
                                                            FROM sked__Availability_Template_Entries__r)
                                                           FROM sked__Availability_Template__c
                                                           WHERE Id IN :map_ResId_TempId.values()]);
        
        Map<Id, Map<string, sked__Availability_Template_Entry__c>> mapResourceWorkingTime = new Map<Id, Map<string, sked__Availability_Template_Entry__c>>();
        for (Id resourceId : inputResourceIds) {
            Id tempId = map_ResId_TempId.get(resourceId);
            
            Map<string, sked__Availability_Template_Entry__c> map_WeekDay_Entry = new Map<string, sked__Availability_Template_Entry__c>();
            if (mapTemplates.containsKey(tempId)) {
                sked__Availability_Template__c template = mapTemplates.get(tempId);
                
                for (sked__Availability_Template_Entry__c entry : template.sked__Availability_Template_Entries__r) {
                    map_WeekDay_Entry.put(entry.sked__Weekday__c, entry);
                }
            }
            mapResourceWorkingTime.put(resourceId, map_WeekDay_Entry);
        }
        return mapResourceWorkingTime;
    }

    public class ShiftAvailableSlot{
    	public String id;
    	public String availableTime;
    	public String resourceName;
        public String dateString;
        public skedCommonModels.TimeModel rescheduleStartTime;
    	list<skedCommonModels.TimeModel> startTimeList;
    	public ShiftAvailableSlot(String i, String ds, Integer startTime, Integer endTime, Integer duration){
    		this.id 				= i;
            this.dateString         = ds;
    		
    		this.resourceName		= '';
    		startTimeList = new list<skedCommonModels.TimeModel>();
    		
    		Date d = skedCommonModels.toDate(ds);//yyyy-MM-dd
    		DateTime dt = DateTime.newInstance(d, Time.newInstance(0,0,0,0));
            this.availableTime      = dt.addHours(12).format('dd/MM/yyyy') + ', ' + skedTimezoneUtil.toTimeString(startTime) + ' - ' + skedTimezoneUtil.toTimeString(endTime);

    		Integer startMins = skedTimezoneUtil.toMinute(startTime);
    		Integer endMins = skedTimezoneUtil.toMinute(endTime);
    		
    		for( Integer t=startMins; t<= endMins-duration; t=t+15){
    			DateTime tmp = dt.addMinutes(t);
    			startTimeList.add(new skedCommonModels.TimeModel(tmp, tmp));
    		}
    		
    	}
    }

	public class ResourceModel {
        public string id;
        public string name;
        public string addr;
        public String region;
        public List<DateSlotModel> dateSlots;
        
        public ResourceModel(sked__Resource__c skedResource) {
            this.dateSlots = new List<DateSlotModel>();
            this.id = skedResource.Id;
            this.name = skedResource.Name;
            this.addr	= skedResource.sked__Home_Address__c;
        }
        
        public Map<string, DateSlotModel> InitilizeDateSlots(Date fromDate, Date toDate, Map<string, Set<Date>> mapHoliday) {
            Map<string, DateSlotModel> mapDateSlot = new Map<string, DateSlotModel>();
            Set<Date> allHolidays = new Set<Date>();
            
            Date tmpFromDate = fromDate;
            while (tmpFromDate <= toDate) {
                
                DateTime dateTimeTemp = DateTime.newInstance(tmpFromDate, Time.newInstance(0, 0, 0, 0));
                DateSlotModel providerDateSlot = new DateSlotModel();
                providerDateSlot.dateString = dateTimeTemp.format(DATE_FORMAT);
                providerDateSlot.d			= tmpFromDate;
                providerDateSlot.isHoliday = allHolidays.contains(tmpFromDate);
                mapDateSlot.put(providerDateSlot.dateString, providerDateSlot);
                this.dateSlots.add(providerDateSlot);

                tmpFromDate = tmpFromDate.addDays(1);
            }
            return mapDateSlot;
        }
    }
    
    public class DateSlotModel {
        public string dateString {get;set;}
        public Date d;
        public integer startWorkingTime {get;set;}
        public integer endWorkingTime {get;set;}
        public List<ShiftModel> shifts {get;set;}
        public boolean isHoliday {get;set;}
        
        public DateSlotModel() {
            this.shifts = new List<ShiftModel>();
            this.isHoliday = false;
        }

        public void mergeShifts(){
        	list<ShiftModel> tmpList = new list<ShiftModel>();

        	if(shifts == null || shifts.size() <= 1) return;
        	for(ShiftModel shift : shifts){
        		tmpList.addAll(breakShift(shift));
        	}
        	tmpList = removeDuplicateShifts(tmpList);
        	shifts = mergeShifts(tmpList);
        }

	    //Break a shift into multiple 1-minute shifts
        public list<ShiftModel> breakShift(ShiftModel s){
        	list<ShiftModel> result = new list<ShiftModel>();

        	integer tmpStart = s.startTime;
        	while(tmpStart < s.endTime){
        		ShiftModel tmpShift = s.clone();
        		
        		tmpShift.startTime = tmpStart;
        		integer endMin = skedTimezoneUtil.toMinute(tmpStart) + 15;
        		tmpShift.endTime	= skedTimezoneUtil.toTime(endMin);
        		result.add(tmpShift);
        		tmpStart = tmpShift.endTime;
        	}
        	return result;
        }

        public list<ShiftModel> removeDuplicateShifts(list<ShiftModel> shifts){
        	if(shifts == null || shifts.size() <=1) return shifts;
        	shifts.sort();//sort by startTime ascending order
        	integer index = shifts.size()-1;
        	while(index>0){
        		
    			ShiftModel s1 = shifts.get(index-1);
    			ShiftModel s2 = shifts.get(index);
    			if(s1.startTime < s2.startTime) {
    				index--;
    				continue;
    			}
    			//check isAvailable to merge 2 shifts, keep unavailable shift
    			if(s1.isAvailable == s2.isAvailable) shifts.remove(index);
    			else if(s1.isAvailable == true) shifts.remove(index-1);
    			else shifts.remove(index);
        		index--;
        	}
        	return shifts;
        }

        
        //Merge 2 shifts, s1.startTime <= s2.startTime
        public list<ShiftModel> mergeShifts(list<ShiftModel> shifts){
        	if(shifts == null || shifts.size() <=1) return shifts;
        	shifts.sort();//sort by startTime ascending order
        	integer index = shifts.size()-1;
        	while(index>0){
        		
    			ShiftModel s1 = shifts.get(index-1);
    			ShiftModel s2 = shifts.get(index);
    			if(s1.isAvailable != s2.isAvailable || s1.endTime < s2.startTime) {
    				index--;
    				continue;
    			}
    			//merge 2 shifts
    			if(s1.endTime == s2.startTime) s1.endTime = s2.endTime;
    			shifts.set(index-1, s1);
    			shifts.remove(index);
    			index--;
        	}
        	return shifts;
        }
    }
    
    public class ShiftModel implements Comparable{
        public string id {get;set;}
        public integer startTime {get;set;}
        public integer endTime {get;set;}
    	public boolean isAvailable;
    	public string addr;//address of the job allocated to the resource at this time
        public String info;
        
        public ShiftModel(sked__Availability__c skedAvailability) {
            this.startTime 	= integer.valueOf(skedAvailability.sked__Start__c.format('Hmm').toLowerCase());
            this.endTime 	= integer.valueOf(skedAvailability.sked__Finish__c.format('Hmm').toLowerCase());
            if (endTime <= startTime) {//endTime is 0:00 next day, then +2400
                endTime += 2400;
            }
            this.isAvailable 	= skedAvailability.sked__Is_Available__c;
            this.info           = this.isAvailable==true?'Available':(skedAvailability.sked__Type__c==null?'Unavailable':('Unavailable' + skedAvailability.sked__Type__c));
        }

        public ShiftModel(String i, integer st, integer et, boolean ia) {
            this.startTime 		= st;
            this.endTime 		= et;
            this.id 			= i;
            this.isAvailable 	= ia;
        }

        public ShiftModel(sked__Activity__c activity){
            this.startTime  = integer.valueOf(activity.sked__Start__c.format('Hmm').toLowerCase());
            this.endTime    = integer.valueOf(activity.sked__End__c.format('Hmm').toLowerCase());
            if (endTime <= startTime) {//endTime is 0:00 next day, then +2400
                endTime += 2400;
            }
            this.isAvailable    = false;
            this.info           = activity.Name;
        }

        public ShiftModel(String i, integer st, integer et, boolean ia, String a) {
            this.startTime 		= st;
            this.endTime 		= et;
            this.id 			= i;
            this.isAvailable 	= ia;
            this.addr 			= a;
        }

        //Sort slot by startTime increasing order
        public Integer compareTo(Object compareTo) {
		    ShiftModel sm = (ShiftModel)compareTo;
		    if(this.startTime > sm.startTime) return 1;
		    else if(this.startTime == sm.startTime) return 0;
		    return -1;
		}
    }
}