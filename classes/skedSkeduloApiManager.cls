global class skedSkeduloApiManager {
    /*
    @future(callout=true)
    global static void dispatchJobs_Future(set<Id> jobIds) {
        dispatchJobs(jobIds);
    }
    */
    global static void dispatchJobs(set<Id> jobIds) {
        List<sked__Job__c> jobResults = [SELECT Id,
                                         (SELECT Id, sked__Resource__c, sked__Status__c, sked__Resource__r.sked__Resource_Type__c
                                          FROM sked__Job_Allocations__r
                                          WHERE sked__Status__c != :SkeduloConstants.JOB_ALLOCATION_STATUS_DELETED)
                                         FROM sked__Job__c
                                         WHERE Id IN :jobIds];
        if (!jobResults.isEmpty()) {
            List<sked__Job_Allocation__c> jobAllocsToUpdate = new List<sked__Job_Allocation__c>();
            list<sked__Job__c> jobsToUpdate = new list<sked__Job__c>();
            for (sked__Job__c job : jobResults) {
                sendDispatchNotification(job, jobAllocsToUpdate);
                jobsToUpdate.add(job);
            }
            update jobsToUpdate;
        }
    }
    
	private static void sendDispatchNotification(sked__Job__c job, List<sked__Job_Allocation__c> jobAllocsToUpdate) {
		ApiResponse result = dispatchJob(job, jobAllocsToUpdate);
		
        if (result!=null && result.success != FALSE) {
            job.sked__Job_Status__c = SkeduloConstants.JOB_ALLOCATION_STATUS_DISPATCHED;
		} 
	}

	private static ApiResponse dispatchJob(sked__Job__c job, List<sked__Job_Allocation__c> jobAllocsToUpdate) {
		ApiResponse result = new ApiResponse();
        Http http			= new Http();
		HttpRequest req		= new HttpRequest();
		HttpResponse res	= new HttpResponse();

		if (string.isBlank( skedConfigs.SKEDULO_API_TOKEN )) {
			result.success = FALSE;
			result.errorMessage = 'Skedulo API Error: API Token is null.';
			return result;
		}

		//Set end point to Authenciate
		string EndPoint = 'https://app.skedulo.com/dispatch?';
		string jobParams = EncodingUtil.urlEncode(job.Id, 'UTF-8');
		EndPoint = EndPoint + 'job=' + jobParams;

		req.setEndpoint( EndPoint );
		req.setMethod('POST');
		req.setTimeout(20000);
		req.setHeader('Content-Type', 'application/x-www-form-urlencoded');
		req.setHeader('apiToken', skedConfigs.SKEDULO_API_TOKEN);
        
		string jsonResponse;
		//try {
			res = http.send(req);
			jsonResponse = res.getBody();
			system.debug('===response ' + JSON.serialize(jsonResponse));
			Map<string, object> deserializedResponse = (Map<string, object>)Json.deserializeUntyped(jsonResponse);
			Map<string, object> resultsObject = (Map<string, object>)deserializedResponse.get('results');
            
            for (sked__Job_Allocation__c jobAlloc : job.sked__Job_Allocations__r) {
                if (jobAlloc.sked__Resource__r.sked__Resource_Type__c == SkeduloConstants.RESOURCE_TYPE_ASSET) {
                    if (jobAlloc.sked__Status__c == SkeduloConstants.JOB_ALLOCATION_STATUS_PENDING_DISPATCH) {
                        jobAlloc.sked__Status__c = SkeduloConstants.JOB_ALLOCATION_STATUS_DISPATCHED;
                        jobAllocsToUpdate.add(jobAlloc);
                    }
                    continue;
                }
                
                string resourceResponseString = Json.serialize(resultsObject.get(jobAlloc.sked__Resource__c));
                if(Test.isRunningTest()) resourceResponseString = JSON.serialize(resultsObject.values().get(0));
                system.debug(resourceResponseString);
                ApiResponse allocResponse = (ApiResponse)Json.deserialize(resourceResponseString, ApiResponse.class);
                
                if (allocResponse.success == FALSE) {
                    jobAlloc.Skedulo_API_Error_Message__c = '';
                    if (!string.isBlank(allocResponse.errorCode)) {
                        jobAlloc.Skedulo_API_Error_Message__c += 'Error Code: ';
                        jobAlloc.Skedulo_API_Error_Message__c += allocResponse.errorCode;
                        jobAlloc.Skedulo_API_Error_Message__c += '\n';
                    }
                    if (!string.isBlank(allocResponse.errorMessage)) {
                        jobAlloc.Skedulo_API_Error_Message__c += 'Error Message: ';
                        jobAlloc.Skedulo_API_Error_Message__c += allocResponse.errorMessage;
                    }
                    jobAllocsToUpdate.add(jobAlloc);
                } else {
                    if (jobAlloc.sked__Status__c == SkeduloConstants.JOB_ALLOCATION_STATUS_PENDING_DISPATCH) {
                        jobAlloc.sked__Status__c = SkeduloConstants.JOB_ALLOCATION_STATUS_DISPATCHED;
                        jobAllocsToUpdate.add(jobAlloc);
                    }
                }
            }
            if(!jobAllocsToUpdate.isEmpty()) update jobAllocsToUpdate;
        system.debug(result);
		return result;
	}

	global class ApiResponse {
		public boolean success {get;set;}
		public string errorMessage {get;set;}
		public string errorCode {get;set;}
	}
}