@isTest
private class skedResourceUtilsTest
{
	static testmethod void doTest() {
		Map<String, sObject> mapData = skedTestUtilities.createData();
		sked__Resource__c resource = (sked__Resource__c)mapData.get('resource 1');
		Set<String> setResourceIDs = new Set<String>{resource.Id};
		sked__Job__c job = (sked__Job__c)mapData.get('job 1');
		sked__Availability__c avai1 = (sked__Availability__c)mapData.get('availability 1');
		sked__Activity__c activity = (sked__Activity__c)mapData.get('activity 1');

		test.starttest();
		skedResourceUtils.ShiftModel shift1 = new skedResourceUtils.ShiftModel(avai1);
		skedResourceUtils.ShiftModel shift2 = new skedResourceUtils.ShiftModel('aaa', 800, 900, true);
		skedResourceUtils.ShiftModel shift3 = new skedResourceUtils.ShiftModel(activity);
		skedResourceUtils util = new skedResourceUtils();
		util.getAvailableResourceIDs(setResourceIDs, job.sked__Start__c.addMinutes(90), 30);
		skedResourceUtils.getAvailabilityList(setResourceIDs, job.sked__Start__c.addMinutes(90), job.sked__Finish__c.addMinutes(90), 30);
		test.stoptest();
	}


}