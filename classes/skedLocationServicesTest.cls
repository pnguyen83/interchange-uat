@isTest
public class skedLocationServicesTest {
    @testSetup
    static void setupData(){
        skedTestDataFactory.setupCustomSettings();

        Account account = skedTestDataFactory.createAccounts('Test Account', 'Clinic', 1).get(0);
        insert account;

        sked__Region__c region = skedTestDataFactory.createRegion('Sydney','Australia/Sydney');
        insert region;

        Contact client = skedTestDataFactory.createContact(account.Id, 'Test Client', 'Client', region.Id);
        insert client;
    }
    @isTest
    static void tetGeocodingSkeduloService() {
        Account account = [Select Id from Account limit 1];
        sked__Region__c region = [Select Id from sked__Region__c limit 1];
        Contact client = [Select Id from Contact limit 1];

        Test.startTest();
        
        Test.setMock(HttpCalloutMock.class, new skedHttpCalloutMock(skedHttpCalloutMock.GEOCODE_DATA));
        //Test insert
        sked__Resource__c resource = skedTestDataFactory.createResource('Test', null, region.Id);
        insert resource;
        //Test update
        resource.sked__Home_Address__c = 'another address';
        update resource;
        //Test Insert Job
        sked__Job__c job = skedTestDataFactory.createJobs(account.Id, region.Id, null, 1).get(0);
        job.sked__Contact__c = client.Id;
        insert job;
        
        Test.stopTest();
    }

    @isTest
    static void tetGeocodingGoogleService() {
        Account account = [Select Id from Account limit 1];
        sked__Region__c region = [Select Id from sked__Region__c limit 1];
        Contact client = [Select Id from Contact limit 1];

        skedConfigs__c config = skedConfigs__c.getAll().get('Geocoding_Service');
        config.Value__c = 'Google';
        update config;

        Test.startTest();
        
        Test.setMock(HttpCalloutMock.class, new skedHttpCalloutMock(skedHttpCalloutMock.GEOCODE_DATA));
        //Test insert
        sked__Resource__c resource = skedTestDataFactory.createResource('Test', null, region.Id);
        insert resource;
        //Test update
        resource.sked__Home_Address__c = 'another address';
        update resource;
        //Test Insert Job
        sked__Job__c job = skedTestDataFactory.createJobs(account.Id, region.Id, null, 1).get(0);
        job.sked__Contact__c = client.Id;
        insert job;
        
        Test.stopTest();
    }

    @isTest
    static void testBatchProcessor() {
        sked__Region__c region = [Select Id from sked__Region__c limit 1];

        Test.startTest();
        
        Test.setMock(HttpCalloutMock.class, new skedHttpCalloutMock(skedHttpCalloutMock.GEOCODE_DATA));
        //Test insert
        sked__Resource__c resource = skedTestDataFactory.createResource('Test', null, region.Id);
        insert resource;

        //Test batch
        skedBatchProcessor batch = new skedBatchProcessor(resource.Id, skedLocationServices.OBJ_RESOURCE);
        Database.executeBatch(batch,1);
        
        Test.stopTest();
    }
    /*
    static testmethod void testDistanceMatrix() {

        Test.startTest();
        
        Test.setMock(HttpCalloutMock.class, new skedHttpCalloutMock(skedHttpCalloutMock.DISTANCE_MATRIX_DATA));

        skedLocationServices.calculateTravelTime('Australia','Vietnam');
        
        Test.stopTest();
    }
    */
}