@isTest
private class skedJobCreateControllerTests {
    
    @isTest static void CreatejobWithServiceItem() {
        List<sked__Region__c> lstRegion = skedDataFactory.createRegions();
        Account acc = skedDataFactory.createAccount();
        Contact con = skedDataFactory.createEmployee(acc);
        Contact client = skedDataFactory.createClient(acc,'Test');
        enrtcr__Support_Contract_Item__c SupContract = skedDataFactory.createSupportContract(client);
        sked__Location__c location = skedTestDataFactory.createLocation('Test', acc.Id, lstRegion.get(0).Id);
        insert location;

        enrtcr__Site__c site = skedTestDataFactory.createSite('Test Site');
        insert site;

        sked__Region__c region = skedTestDataFactory.createRegion('Sydney','Australia/Sydney');
        insert region;

        Test.setCurrentPage( new PageReference('/skedJobCreation?id=' + client.id + '&duration=60&locationId=' + location.id + '&regionId=' + region.id + '&startDate=' + System.today().format()) );
        ApexPages.StandardController std = new ApexPages.StandardController(client);
        skedJobCreationController cont = new skedJobCreationController(std);
        String jobURL                   = cont.configData.jobURL;
        String altAddressLookup         = cont.altAddressLookup;
        cont.getAltLookupOptions();
        cont.changeAltAddressLookup();
        skedJobCreationController.getVisualforceUrl();

        cont.DummyJob.sked__Account__c  = acc.id;
        cont.NewJob.sked__Account__c    = acc.id;
        cont.NewJob.Service_Agreement_Item_Temp__c = SupContract.id;


        Test.startTest();
        cont.SelectedServiceAgreementItem = SupContract.id;
        cont.Cmd_SelectServiceAgreementItemCategory();

        cont.Cmd_PopulateAddress();
        cont.jobStartDateString = system.today().format();

        cont.altAddressLookup = 'Location';
        cont.DummyJob.sked__Location__c = location.Id;
        cont.Cmd_PopulateAddress();

        cont.altAddressLookup = 'Site';
        cont.DummyJob.sked__Location__c = site.Id;
        cont.Cmd_PopulateAddress();

        cont.Cmd_Confirm();
        Test.stopTest();
    }
    
    @isTest static void CreatejobWithService() {
        List<sked__Region__c> lstRegion = skedDataFactory.createRegions();
        Account acc = skedDataFactory.createAccount();
        Contact con = skedDataFactory.createEmployee(acc);
        
        enrtcr__Support_Delivered__c supportDelivered = skedDataFactory.createServiceDelivered(skedDataFactory.createEnriteCareReferenceData());
        Contact client = [select LastName, enrtcr__Sex__c, Birthdate, enrtcr__Photo__c, enrtcr__Secondary_Disability__c, OtherStreet, OtherCity, OtherState, OtherPostalCode,
                            enrtcr__Date_Client_Registered__c, Phone, enrtcr__Preferred_Communication_Method__c, enrtcr__Primary_Disability__c, enrtcr__Status__c, enrtcr__Summary_Disability__c,
                            enrtcr__Requires_Support_for_Decision_Making__c, enrtcr__Client_Region__c, MobilePhone
                             from contact where id =:supportDelivered.enrtcr__Client__c];
                                    
        ApexPages.StandardController std = new ApexPages.StandardController(client);
        Test.startTest();
        skedJobCreationController cont = new skedJobCreationController(std);
        cont.Cmd_Confirm();
        cont.jobStartDateString = system.today().format();
        cont.NewJob.Region_tmp__c = null;
        cont.Cmd_Confirm();
        cont.NewJob.sked__Region__c = lstRegion[0].id;
        
        cont.DummyJob.sked__Account__c = acc.id;
        cont.NewJob.sked__Account__c = acc.id;
        cont.NewJob.Service_Agreement__c = supportDelivered.enrtcr__Support_Contract__c;
        cont.NewJob.Service_Agreement_Item__c = supportDelivered.enrtcr__Support_Contract_Item__c;
        cont.Cmd_Confirm();
        cont.Cmd_PopulateAddress();
        
        cont.Cmd_Confirm();

        cont.Cmd_PopulateCategories();
        
        cont.NewJob.sked__Job_Status__c = 'Dispatched';
        upsert cont.newJob;
        
        Test.stopTest();
    }

    @isTest static void CreatejobWithServiceAgreementItem() {
        List<sked__Region__c> lstRegion = skedDataFactory.createRegions();
        Account acc = skedDataFactory.createAccount();
        Contact con = skedDataFactory.createEmployee(acc);
        Contact client = skedDataFactory.createClient(acc,'Test');
        enrtcr__Support_Contract_Item__c SupContract = skedDataFactory.createSupportContract(client);

        ApexPages.StandardController std = new ApexPages.StandardController(client);
        skedJobCreationController cont = new skedJobCreationController(std);
         Test.startTest();
        cont.DummyJob.sked__Account__c = acc.id;
        cont.NewJob.sked__Account__c = acc.id;
        cont.NewJob.Service_Agreement_Item_Temp__c = SupContract.id;
        cont.jobStartDateString = datetime.now().format('dd/MM/YYYY');

       
        cont.Cmd_SelectedCategory();
        cont.Cmd_PopulateCategories();
        cont.NewJob.Service__c = SupContract.enrtcr__Service__c;
        cont.Cmd_SelectedService();
        cont.Cmd_SelectProgram();
        cont.Cmd_SelectServiceAgreementItemCategory();

        Test.stopTest();



    }

    @isTest static void UpdateCompleteJobActualFinishTime() {
        List<sked__Region__c> lstRegion = skedDataFactory.createRegions();
        Account acc = skedDataFactory.createAccount();
        Contact con = skedDataFactory.createEmployee(acc);
        
        enrtcr__Support_Delivered__c supportDelivered = skedDataFactory.createServiceDelivered(skedDataFactory.createEnriteCareReferenceData());
        Contact client = [select LastName, enrtcr__Sex__c, Birthdate, enrtcr__Photo__c, enrtcr__Secondary_Disability__c, OtherStreet, OtherCity, OtherState, OtherPostalCode,
                            enrtcr__Date_Client_Registered__c, Phone, enrtcr__Preferred_Communication_Method__c, enrtcr__Primary_Disability__c, enrtcr__Status__c, enrtcr__Summary_Disability__c,
                            enrtcr__Requires_Support_for_Decision_Making__c, enrtcr__Client_Region__c, MobilePhone
                             from contact where id =:supportDelivered.enrtcr__Client__c];
                                    
        ApexPages.StandardController std = new ApexPages.StandardController(client);
        Test.startTest();
        skedJobCreationController cont = new skedJobCreationController(std);
        cont.Cmd_Confirm();
        cont.jobStartDateString = system.today().format();
        cont.NewJob.Region_tmp__c = null;
        cont.Cmd_Confirm();
        cont.NewJob.sked__Region__c = lstRegion[0].id;
        
        cont.DummyJob.sked__Account__c = acc.id;
        cont.NewJob.sked__Account__c = acc.id;
        cont.NewJob.Service_Agreement__c = supportDelivered.enrtcr__Support_Contract__c;
        cont.NewJob.Service_Agreement_Item__c = supportDelivered.enrtcr__Support_Contract_Item__c;
        cont.Cmd_Confirm();
        cont.Cmd_PopulateAddress();
        
        cont.Cmd_Confirm();

        cont.Cmd_PopulateCategories();
        
        cont.NewJob.sked__Job_Status__c = 'Dispatched';
        cont.NewJob.sked__Start__c = System.now();
        cont.NewJob.sked__Finish__c = System.now().addHours(1);
        upsert cont.newJob;
        
        Test.stopTest();
    }

    @isTest static void confirmRegion() {
        List<sked__Region__c> lstRegion = skedDataFactory.createRegions();
        Account acc = skedDataFactory.createAccount();
        Contact con = skedDataFactory.createEmployee(acc);

        enrtcr__Support_Delivered__c supportDelivered = skedDataFactory.createServiceDelivered(skedDataFactory.createEnriteCareReferenceData());
        Contact client = [select LastName, enrtcr__Sex__c, Birthdate, enrtcr__Photo__c, enrtcr__Secondary_Disability__c, OtherStreet, OtherCity, OtherState, OtherPostalCode,
                enrtcr__Date_Client_Registered__c, Phone, enrtcr__Preferred_Communication_Method__c, enrtcr__Primary_Disability__c, enrtcr__Status__c, enrtcr__Summary_Disability__c,
                enrtcr__Requires_Support_for_Decision_Making__c, enrtcr__Client_Region__c, MobilePhone
        from contact where id =:supportDelivered.enrtcr__Client__c];

        ApexPages.StandardController std = new ApexPages.StandardController(client);
        Test.startTest();
        skedJobCreationController cont = new skedJobCreationController(std);
        cont.jobStartDateString = system.today().format();
        cont.jobStartTimeString = '09:00 AM';
        cont.NewJob.Region_tmp__c = null;
        cont.Cmd_Confirm();
        cont.NewJob.Region_tmp__c = lstRegion[0].id;
        cont.AlternativeAddress = 'Alt address';
        cont.Cmd_Confirm();
        cont.NewJob.Service_Agreement_Item__c = null;
        cont.SelectedServiceAgreementItem = supportDelivered.enrtcr__Support_Contract_Item__c;
        cont.Cmd_Confirm();

        Test.stopTest();
    }

    @isTest static void testDailyRecurringAfter(){
        List<sked__Region__c> lstRegion = skedDataFactory.createRegions();
        Account acc = skedDataFactory.createAccount();
        Contact con = skedDataFactory.createEmployee(acc);
        
        enrtcr__Support_Delivered__c supportDelivered = skedDataFactory.createServiceDelivered(skedDataFactory.createEnriteCareReferenceData());
        Contact client = [select LastName, enrtcr__Sex__c, Birthdate, enrtcr__Photo__c, enrtcr__Secondary_Disability__c, OtherStreet, OtherCity, OtherState, OtherPostalCode,
                            enrtcr__Date_Client_Registered__c, Phone, enrtcr__Preferred_Communication_Method__c, enrtcr__Primary_Disability__c, enrtcr__Status__c, enrtcr__Summary_Disability__c,
                            enrtcr__Requires_Support_for_Decision_Making__c, enrtcr__Client_Region__c, MobilePhone
                             from contact where id =:supportDelivered.enrtcr__Client__c];
                                    
        ApexPages.StandardController std = new ApexPages.StandardController(client);
        Test.startTest();
        skedJobCreationController cont = new skedJobCreationController(std);
        cont.NewJob.sked__Region__c = lstRegion[0].id;
        cont.DummyJob.sked__Account__c = acc.id;
        cont.NewJob.sked__Account__c = acc.id;
        cont.NewJob.sked__Duration__c = 60;
        cont.NewJob.Service_Agreement__c = supportDelivered.enrtcr__Support_Contract__c;
        cont.NewJob.Service_Agreement_Item__c = supportDelivered.enrtcr__Support_Contract_Item__c;
        
        cont.Wrapper.IsRecurring = true;
        cont.Wrapper.Every = 5;

        cont.jobStartDateString = system.today().format();
        cont.jobStartTimeString = '09:00 AM';
        cont.Wrapper.Ending = 'after';
        cont.Wrapper.NumberOfSessions = 10;
        cont.Wrapper.Repeat = 'daily';
        cont.Wrapper.ExcludeWeekends = false;
        cont.Wrapper.ExcludeHolidays = false;
        
        cont.Cmd_Confirm();

        Test.stopTest();
    }

    @isTest static void testDailyRecurringOn(){
        List<sked__Region__c> lstRegion = skedDataFactory.createRegions();
        Account acc = skedDataFactory.createAccount();
        Contact con = skedDataFactory.createEmployee(acc);
        
        enrtcr__Support_Delivered__c supportDelivered = skedDataFactory.createServiceDelivered(skedDataFactory.createEnriteCareReferenceData());
        Contact client = [select LastName, enrtcr__Sex__c, Birthdate, enrtcr__Photo__c, enrtcr__Secondary_Disability__c, OtherStreet, OtherCity, OtherState, OtherPostalCode,
                            enrtcr__Date_Client_Registered__c, Phone, enrtcr__Preferred_Communication_Method__c, enrtcr__Primary_Disability__c, enrtcr__Status__c, enrtcr__Summary_Disability__c,
                            enrtcr__Requires_Support_for_Decision_Making__c, enrtcr__Client_Region__c, MobilePhone
                             from contact where id =:supportDelivered.enrtcr__Client__c];
                                    
        ApexPages.StandardController std = new ApexPages.StandardController(client);
        Test.startTest();
        skedJobCreationController cont = new skedJobCreationController(std);
        cont.NewJob.sked__Region__c = lstRegion[0].id;
        cont.DummyJob.sked__Account__c = acc.id;
        cont.NewJob.sked__Account__c = acc.id;
        cont.NewJob.sked__Duration__c = 60;
        cont.NewJob.Service_Agreement__c = supportDelivered.enrtcr__Support_Contract__c;
        cont.NewJob.Service_Agreement_Item__c = supportDelivered.enrtcr__Support_Contract_Item__c;
        
        cont.Wrapper.IsRecurring = true;
        cont.Wrapper.Every = 5;

        cont.jobStartDateString = system.today().format();
        cont.jobStartTimeString = '09:00 AM';
        cont.Wrapper.Ending = 'on';
        cont.Wrapper.EndingOn = '15/11/2017';
        cont.Wrapper.ExcludeWeekends = false;
        cont.Wrapper.ExcludeHolidays = false;
        
        cont.Cmd_Confirm();

        Test.stopTest();
    }

    @isTest static void testWeeklyRecurringAfter(){
        List<sked__Region__c> lstRegion = skedDataFactory.createRegions();
        Account acc = skedDataFactory.createAccount();
        Contact con = skedDataFactory.createEmployee(acc);
        
        enrtcr__Support_Delivered__c supportDelivered = skedDataFactory.createServiceDelivered(skedDataFactory.createEnriteCareReferenceData());
        Contact client = [select LastName, enrtcr__Sex__c, Birthdate, enrtcr__Photo__c, enrtcr__Secondary_Disability__c, OtherStreet, OtherCity, OtherState, OtherPostalCode,
                            enrtcr__Date_Client_Registered__c, Phone, enrtcr__Preferred_Communication_Method__c, enrtcr__Primary_Disability__c, enrtcr__Status__c, enrtcr__Summary_Disability__c,
                            enrtcr__Requires_Support_for_Decision_Making__c, enrtcr__Client_Region__c, MobilePhone
                             from contact where id =:supportDelivered.enrtcr__Client__c];
                                    
        ApexPages.StandardController std = new ApexPages.StandardController(client);
        Test.startTest();
        skedJobCreationController cont = new skedJobCreationController(std);
        cont.NewJob.sked__Region__c = lstRegion[0].id;
        cont.DummyJob.sked__Account__c = acc.id;
        cont.NewJob.sked__Account__c = acc.id;
        cont.NewJob.sked__Duration__c = 60;
        cont.NewJob.Service_Agreement__c = supportDelivered.enrtcr__Support_Contract__c;
        cont.NewJob.Service_Agreement_Item__c = supportDelivered.enrtcr__Support_Contract_Item__c;
        
        cont.Wrapper.IsRecurring = true;
        cont.Wrapper.Every = 5;

        cont.jobStartDateString = system.today().format();
        cont.jobStartTimeString = '09:00 AM';
        cont.Wrapper.Ending = 'after';
        cont.Wrapper.NumberOfSessions = 10;
        cont.Wrapper.Repeat = 'weekly';
        cont.Wrapper.DaysOfWeek = 'Friday, Monday';
        cont.Wrapper.ExcludeWeekends = false;
        cont.Wrapper.ExcludeHolidays = false;
        
        cont.Cmd_Confirm();

        Test.stopTest();
    }

    @isTest static void testWeeklyRecurringOn(){
        List<sked__Region__c> lstRegion = skedDataFactory.createRegions();
        Account acc = skedDataFactory.createAccount();
        Contact con = skedDataFactory.createEmployee(acc);
        
        enrtcr__Support_Delivered__c supportDelivered = skedDataFactory.createServiceDelivered(skedDataFactory.createEnriteCareReferenceData());
        Contact client = [select LastName, enrtcr__Sex__c, Birthdate, enrtcr__Photo__c, enrtcr__Secondary_Disability__c, OtherStreet, OtherCity, OtherState, OtherPostalCode,
                            enrtcr__Date_Client_Registered__c, Phone, enrtcr__Preferred_Communication_Method__c, enrtcr__Primary_Disability__c, enrtcr__Status__c, enrtcr__Summary_Disability__c,
                            enrtcr__Requires_Support_for_Decision_Making__c, enrtcr__Client_Region__c, MobilePhone
                             from contact where id =:supportDelivered.enrtcr__Client__c];
                                    
        ApexPages.StandardController std = new ApexPages.StandardController(client);
        Test.startTest();
        skedJobCreationController cont = new skedJobCreationController(std);
        cont.NewJob.sked__Region__c = lstRegion[0].id;
        cont.DummyJob.sked__Account__c = acc.id;
        cont.NewJob.sked__Account__c = acc.id;
        cont.NewJob.sked__Duration__c = 60;
        cont.NewJob.Service_Agreement__c = supportDelivered.enrtcr__Support_Contract__c;
        cont.NewJob.Service_Agreement_Item__c = supportDelivered.enrtcr__Support_Contract_Item__c;
        
        cont.Wrapper.IsRecurring = true;
        cont.Wrapper.Every = 5;

        cont.jobStartDateString = system.today().format();
        cont.jobStartTimeString = '09:00 AM';
        cont.Wrapper.Ending = 'on';
        cont.Wrapper.EndingOn = system.now().addDays(30).format('dd/MM/yyyy');
        cont.Wrapper.Repeat = 'weekly';
        cont.Wrapper.DaysOfWeek = 'Friday, Monday';
        cont.Wrapper.ExcludeWeekends = false;
        cont.Wrapper.ExcludeHolidays = false;
        
        cont.Cmd_Confirm();

        Test.stopTest();
    }

    @isTest static void testMonthlyRecurringAfter(){
        List<sked__Region__c> lstRegion = skedDataFactory.createRegions();
        Account acc = skedDataFactory.createAccount();
        Contact con = skedDataFactory.createEmployee(acc);
        
        enrtcr__Support_Delivered__c supportDelivered = skedDataFactory.createServiceDelivered(skedDataFactory.createEnriteCareReferenceData());
        Contact client = [select LastName, enrtcr__Sex__c, Birthdate, enrtcr__Photo__c, enrtcr__Secondary_Disability__c, OtherStreet, OtherCity, OtherState, OtherPostalCode,
                            enrtcr__Date_Client_Registered__c, Phone, enrtcr__Preferred_Communication_Method__c, enrtcr__Primary_Disability__c, enrtcr__Status__c, enrtcr__Summary_Disability__c,
                            enrtcr__Requires_Support_for_Decision_Making__c, enrtcr__Client_Region__c, MobilePhone
                             from contact where id =:supportDelivered.enrtcr__Client__c];
                                    
        ApexPages.StandardController std = new ApexPages.StandardController(client);
        Test.startTest();
        skedJobCreationController cont = new skedJobCreationController(std);
        cont.NewJob.sked__Region__c = lstRegion[0].id;
        cont.DummyJob.sked__Account__c = acc.id;
        cont.NewJob.sked__Account__c = acc.id;
        cont.NewJob.sked__Duration__c = 60;
        cont.NewJob.Service_Agreement__c = supportDelivered.enrtcr__Support_Contract__c;
        cont.NewJob.Service_Agreement_Item__c = supportDelivered.enrtcr__Support_Contract_Item__c;
        
        cont.Wrapper.IsRecurring = true;
        cont.Wrapper.Every = 5;

        cont.jobStartDateString = system.today().format();
        cont.jobStartTimeString = '09:00 AM';
        cont.Wrapper.Ending = 'after';
        cont.Wrapper.NumberOfSessions = 10;
        cont.Wrapper.Repeat = 'monthly';
        cont.Wrapper.ExcludeWeekends = true;
        cont.Wrapper.ExcludeHolidays = false;
        
        cont.Cmd_Confirm();

        Test.stopTest();
    }

    @isTest static void testMonthlyRecurringOn(){
        List<sked__Region__c> lstRegion = skedDataFactory.createRegions();
        Account acc = skedDataFactory.createAccount();
        Contact con = skedDataFactory.createEmployee(acc);
        
        enrtcr__Support_Delivered__c supportDelivered = skedDataFactory.createServiceDelivered(skedDataFactory.createEnriteCareReferenceData());
        Contact client = [select LastName, enrtcr__Sex__c, Birthdate, enrtcr__Photo__c, enrtcr__Secondary_Disability__c, OtherStreet, OtherCity, OtherState, OtherPostalCode,
                            enrtcr__Date_Client_Registered__c, Phone, enrtcr__Preferred_Communication_Method__c, enrtcr__Primary_Disability__c, enrtcr__Status__c, enrtcr__Summary_Disability__c,
                            enrtcr__Requires_Support_for_Decision_Making__c, enrtcr__Client_Region__c, MobilePhone
                             from contact where id =:supportDelivered.enrtcr__Client__c];
                                    
        ApexPages.StandardController std = new ApexPages.StandardController(client);
        Test.startTest();
        skedJobCreationController cont = new skedJobCreationController(std);
        cont.NewJob.sked__Region__c = lstRegion[0].id;
        cont.DummyJob.sked__Account__c = acc.id;
        cont.NewJob.sked__Account__c = acc.id;
        cont.NewJob.sked__Duration__c = 60;
        cont.NewJob.Service_Agreement__c = supportDelivered.enrtcr__Support_Contract__c;
        cont.NewJob.Service_Agreement_Item__c = supportDelivered.enrtcr__Support_Contract_Item__c;
        
        cont.Wrapper.IsRecurring = true;
        cont.Wrapper.Every = 5;

        cont.jobStartDateString = system.today().format();
        cont.jobStartTimeString = '09:00 AM';
        cont.Wrapper.Ending = 'on';
        cont.Wrapper.EndingOn = system.now().addMonths(12).format('dd/MM/yyyy');
        cont.Wrapper.Repeat = 'monthly';
        cont.Wrapper.ExcludeWeekends = false;
        cont.Wrapper.ExcludeHolidays = false;
        
        cont.Cmd_Confirm();

        Test.stopTest();
    }

    @isTest static void testYearlyRecurringAfter(){
        List<sked__Region__c> lstRegion = skedDataFactory.createRegions();
        Account acc = skedDataFactory.createAccount();
        Contact con = skedDataFactory.createEmployee(acc);
        
        enrtcr__Support_Delivered__c supportDelivered = skedDataFactory.createServiceDelivered(skedDataFactory.createEnriteCareReferenceData());
        Contact client = [select LastName, enrtcr__Sex__c, Birthdate, enrtcr__Photo__c, enrtcr__Secondary_Disability__c, OtherStreet, OtherCity, OtherState, OtherPostalCode,
                            enrtcr__Date_Client_Registered__c, Phone, enrtcr__Preferred_Communication_Method__c, enrtcr__Primary_Disability__c, enrtcr__Status__c, enrtcr__Summary_Disability__c,
                            enrtcr__Requires_Support_for_Decision_Making__c, enrtcr__Client_Region__c, MobilePhone
                             from contact where id =:supportDelivered.enrtcr__Client__c];
                                    
        ApexPages.StandardController std = new ApexPages.StandardController(client);
        Test.startTest();
        skedJobCreationController cont = new skedJobCreationController(std);
        cont.NewJob.sked__Region__c = lstRegion[0].id;
        cont.DummyJob.sked__Account__c = acc.id;
        cont.NewJob.sked__Account__c = acc.id;
        cont.NewJob.sked__Duration__c = 60;
        cont.NewJob.Service_Agreement__c = supportDelivered.enrtcr__Support_Contract__c;
        cont.NewJob.Service_Agreement_Item__c = supportDelivered.enrtcr__Support_Contract_Item__c;
        
        cont.Wrapper.IsRecurring = true;
        cont.Wrapper.Every = 5;

        cont.jobStartDateString = system.today().format();
        cont.jobStartTimeString = '09:00 AM';
        cont.Wrapper.Ending = 'after';
        cont.Wrapper.NumberOfSessions = 10;
        cont.Wrapper.Repeat = 'yearly';
        cont.Wrapper.ExcludeWeekends = true;
        cont.Wrapper.ExcludeHolidays = false;
        
        cont.Cmd_Confirm();
        Test.stopTest();
    }

    @isTest static void testYearlyRecurringOn(){
        List<sked__Region__c> lstRegion = skedDataFactory.createRegions();
        Account acc = skedDataFactory.createAccount();
        Contact con = skedDataFactory.createEmployee(acc);
        
        enrtcr__Support_Delivered__c supportDelivered = skedDataFactory.createServiceDelivered(skedDataFactory.createEnriteCareReferenceData());
        Contact client = [select LastName, enrtcr__Sex__c, Birthdate, enrtcr__Photo__c, enrtcr__Secondary_Disability__c, OtherStreet, OtherCity, OtherState, OtherPostalCode,
                            enrtcr__Date_Client_Registered__c, Phone, enrtcr__Preferred_Communication_Method__c, enrtcr__Primary_Disability__c, enrtcr__Status__c, enrtcr__Summary_Disability__c,
                            enrtcr__Requires_Support_for_Decision_Making__c, enrtcr__Client_Region__c, MobilePhone
                             from contact where id =:supportDelivered.enrtcr__Client__c];
                                    
        ApexPages.StandardController std = new ApexPages.StandardController(client);
        Test.startTest();
        skedJobCreationController cont = new skedJobCreationController(std);
        cont.NewJob.sked__Region__c = lstRegion[0].id;
        cont.DummyJob.sked__Account__c = acc.id;
        cont.NewJob.sked__Account__c = acc.id;
        cont.NewJob.sked__Duration__c = 60;
        cont.NewJob.Service_Agreement__c = supportDelivered.enrtcr__Support_Contract__c;
        cont.NewJob.Service_Agreement_Item__c = supportDelivered.enrtcr__Support_Contract_Item__c;
        
        cont.Wrapper.IsRecurring = true;
        cont.Wrapper.Every = 5;

        cont.jobStartDateString = system.today().format();
        cont.jobStartTimeString = '09:00 AM';
        cont.Wrapper.Ending = 'on';
        cont.Wrapper.EndingOn = system.now().addYears(12).format('dd/MM/yyyy');
        cont.Wrapper.Repeat = 'yearly';
        cont.Wrapper.ExcludeWeekends = false;
        cont.Wrapper.ExcludeHolidays = false;
        
        cont.Cmd_Confirm();
        Test.stopTest();
    }
}