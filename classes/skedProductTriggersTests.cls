@isTest
private class skedProductTriggersTests {
    @isTest static void TestSyncProduct() {
        createProduct();
        list<Product2> p2s = [select id, Name from Product2 where isActive=true limit 1];
        
        P2s[0].Name = 'Test Sync';
        
        update p2s;
    }
    
    private static Product2 createProduct(){
        Product2 createNewProduct = new Product2();
         createNewProduct.Name = '24" Monitor' ;
         createNewProduct.ProductCode = '24MN' ;
         createNewProduct.IsActive = true ;
         createNewProduct.Family = 'Accessory' ;
        insert createNewProduct;
        return createNewProduct;
}


}