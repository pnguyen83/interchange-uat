@isTest
public class skedTestUtilities
{
	public static testmethod Map<String, sObject> createData() {
		skedDataSetup.setupServiceTypeColorSettings();

		Map<String, sObject> mapsObject = new Map<String, sObject>();
		List<sObject> lstsObject1 = new List<sObject>();
		List<sObject> lstsObject2 = new List<sObject>();
		List<sObject> lstsObject3 = new List<sObject>();
		List<sObject> lstsObject4 = new List<sObject>();

		//=================================================================Group 1============================================================//
		//Create Tag
		sked__Tag__c tag1 = new sked__Tag__c(
				name = 'Test tag',
				sked__Type__c = 'Skill',
				sked__Classification__c = 'Global'
			);
		mapsObject.put('tag 1', tag1);
		lstsObject1.add(tag1);
		
		//Create region
		sked__Region__c region1 = new sked__Region__c(
				name = 'Test Region',
				sked__Timezone__c = 'Australia/Brisbane'
			);
		mapsObject.put('region 1', region1);
		lstsObject1.add(region1);

		//Create Availability Template
		sked__Availability_Template__c avaiTemplate1 = new sked__Availability_Template__c(
				sked__Global__c = true
			);
		mapsObject.put('available template 1', avaiTemplate1);
		lstsObject1.add(avaiTemplate1);

		//Create Account
		List<RecordType> lstRecordType = [SELECT Id FROM RecordType WHERE Name = 'Education Setting' AND sObjectType = 'Account'];
		String recTypeId = '';
		if (lstRecordType != null && !lstRecordType.isEmpty()) {
			recTypeId = lstRecordType.get(0).Id;
		}

		Account acc1 = new Account(
				RecordTypeId = recTypeId,
				Name = 'Test Education Setting',
				BillingCity = 'Salisbury',
				BillingStreet = '28 Tuckett Rd',
				BillingState = 'Queensland',
				BillingPostalCode = '4000',
				BillingCountry = 'AU'
			);
		mapsObject.put('account 1', acc1);
		lstsObject1.add(acc1);

		//Create Site
		List<RecordType> lstSiteRecordType = [SELECT Id FROM RecordType WHERE Name = 'Other' AND sObjectType = 'enrtcr__Site__c'];
		String siteRecTypeId = '';
		if (lstSiteRecordType != null && !lstSiteRecordType.isEmpty()) {
			siteRecTypeId = lstSiteRecordType.get(0).Id;
		}

		enrtcr__Site__c site1 = new enrtcr__Site__c(
				name = 'test site',
				enrtcr__Site_Name__c = 'test site',
				RecordTypeId = siteRecTypeId,
				enrtcr__Business_Address_1__c = '118 North Terrace',
				enrtcr__Business_Suburb__c = 'Adelaide',
				enrtcr__Business_State__c = 'SA',
				enrtcr__Business_Postcode__c = '5000'
			);
		mapsObject.put('site 1', site1);
		lstsObject1.add(site1);

		//Create service
		List<RecordType> lstServiceRecordType = [SELECT Id FROM RecordType WHERE Name = 'NDIS' AND sObjectType = 'enrtcr__Service__c'];
		String sRecTypeId = '';
		if (lstServiceRecordType != null && !lstServiceRecordType.isEmpty()) {
			sRecTypeId = lstServiceRecordType.get(0).Id;
		}
		enrtcr__Service__c service1 = new enrtcr__Service__c(
				RecordTypeId = sRecTypeId,
				name = 'assistance with self-care - active overnight 1',
				enrtcr__Registration_Group__c = 'Assist Personal Activities',
				enrtcr__Support_Item_Name__c = 'assistance with self-care - active overnight',
				enrtcr__Support_Item_Code__c = '01_002_0107_1_1',
				enrtcr__Support_Item_Description__c = 'Assistance with, or supervision of, personal tasks of daily living',
				enrtcr__Price_Control__c = 'Y',
				enrtcr__Service_Type__c = 'OT'
			);
		mapsObject.put('service 1', service1);
		lstsObject1.add(service1);

		insert lstsObject1;

		//=================================================================Group 2=========================================================//
		//Create resource
		sked__Resource__c resource1 = new sked__Resource__c(
				name = 'Test Resource',
				sked__Resource_Type__c = 'Person',
				sked__Primary_Region__c = region1.Id,
				sked__Category__c = 'Customer Service',
				sked__Country_Code__c = 'AU',
				sked__Home_Address__c = '24 Tuckett Rd, Salisbury, Queensland, AUS',
				sked__Is_Active__c = true,
				sked__Weekly_Hours__c = 40
			);
		mapsObject.put('resource 1', resource1);
		lstsObject2.add(resource1);

		//create location
		sked__Location__c location1 = new sked__Location__c(
				name = 'Pitt St test',
				sked__Address__c = '30 Tuckett Rd, Salisbury, Queensland, AUS',
				sked__Type__c = 'Home',
				sked__Account__c = acc1.Id,
				sked__Region__c = region1.Id
			);
		mapsObject.put('location 1', location1);
		lstsObject2.add(location1);

		//Create contact
		List<RecordType> lstContactRecType = [SELECT Id FROM RecordType WHERE sObjectType = 'Contact' AND DeveloperName = 'Client'];
		String contRecType = '';

		if (lstContactRecType != null && !lstContactRecType.isEmpty()) {
			contRecType = lstContactRecType.get(0).Id;
		}
		Contact cont1 = new Contact(
				RecordTypeId = contRecType,
				LastName = 'Test',
				FirstName = 'Contact',
				enrtcr__Sex__c = 'Male',
				Birthdate = Date.newInstance(1983, 12, 10),
				OtherStreet = '30 Tuckett Rd',
				OtherCity = 'Salisbury',
				OtherState = 'Queensland',
				OtherPostalCode = '4000',
				OtherCountry = 'AU',
				enrtcr__Preferred_Communication_Method__c = 'Phone',
				Phone = '0411111111',
				enrtcr__Primary_Disability__c = 'Anxiety',
				AccountId = acc1.Id, 
				enrtcr__Client_Region__c = 'Western'
			);
		mapsObject.put('contact 1', cont1);
		lstsObject2.add(cont1);

		insert lstsObject2;

		//==================================================================Group 3========================================================//
		//Create Job
		sked__Job__c job1 = new sked__Job__c(
				sked__Description__c = 'Test Job',
				sked__Type__c = 'Single Booking',
				sked__Address__c = '26 Tuckett Rd, Salisbury, Queensland, AUS',
				sked__Region__c = region1.Id,
				sked__Start__c = System.now(),
				sked__Duration__c = 60,
				sked__Finish__c = System.now().addMinutes(60),
				sked__Account__c = acc1.Id,
				sked__Contact__c = cont1.Id,
				Service__c = service1.Id
			);
		mapsObject.put('job 1', job1);
		lstsObject3.add(job1);

		sked__Job__c job2 = new sked__Job__c(
				sked__Description__c = 'Test Job',
				sked__Type__c = 'Single Booking',
				sked__Address__c = '26 Tuckett Rd, Salisbury, Queensland, AUS',
				sked__Region__c = region1.Id,
				sked__Start__c = System.now(),
				sked__Duration__c = 1440,
				sked__Finish__c = System.now().addMinutes(1440),
				sked__Account__c = acc1.Id,
				sked__Contact__c = cont1.Id,
				Service__c = service1.Id,
				sked__Job_Status__c = 'Pending Allocation'
			);
		mapsObject.put('job 2', job2);
		lstsObject3.add(job2);

		//Create Resource tag
		sked__Resource_Tag__c resourceTag1 = new sked__Resource_Tag__c(
				sked__Resource__c = resource1.Id,
				sked__Tag__c = tag1.Id
			);
		mapsObject.put('resourceTag 1', resourceTag1);
		lstsObject3.add(resourceTag1);

		//Create sked__Availability_Template_Resource__c
		sked__Availability_Template_Resource__c avaiTemplateResource1 = new sked__Availability_Template_Resource__c(
				sked__Availability_Template__c = avaiTemplate1.Id,
				sked__Resource__c = resource1.Id
			);
		mapsObject.put('available template resource 1', avaiTemplateResource1);
		lstsObject3.add(avaiTemplateResource1);

		//Create availability
		sked__Availability__c avai1 = new sked__Availability__c(
				sked__Resource__c = resource1.Id,
				sked__Timezone__c = 'Australia/Brisbane',
				sked__Is_Available__c = false,
				sked__Type__c = 'Leave',
				sked__Status__c = 'Approved',
				sked__Start__c = job1.sked__Start__c.addDays(1),
				sked__Finish__c = job1.sked__Finish__c.addDays(1)
			);
		mapsObject.put('availability 1', avai1);
		lstsObject3.add(avai1);

		//Create activity
		sked__Activity__c activity1 = new sked__Activity__c(
				sked__Address__c = '28 Tuckett Rd, Salisbury, Queensland, AUS',
				sked__Type__c = 'Meal Break',
				sked__Start__c = job1.sked__Start__c.addMinutes(120),
				sked__End__c = job1.sked__Finish__c.addMinutes(120),
				sked__Timezone__c = 'Australia/Brisbane',
				sked__Resource__c = resource1.Id
			);
		mapsObject.put('activity 1', activity1);
		lstsObject3.add(activity1);

		insert lstsObject3;

		//=================================================================Group 4=========================================================//
		//Create Job Allocation
		String uniqueKey = job1.Id + ':' + resource1.Id;
		sked__Job_Allocation__c jobAlloc1 = new sked__Job_Allocation__c(
				sked__Job__c = job1.Id,
				sked__Resource__c = resource1.Id,
				sked__Assigned_To__c = resource1.Id,
				sked__UniqueKey__c = uniqueKey
			);
		mapsObject.put('job allocation 1', jobAlloc1);
		lstsObject4.add(jobAlloc1);

		//create address option
		skedAddressOption addOpt = new skedAddressOption();
		addOpt.id = 'test id';
		addOpt.label = 'test label';
		addOpt.value = 'test value';
		addOpt.type = 'test type';
		addOpt.address = 'test address';
		addOpt.locationID = location1.Id;
		addOpt.addressID = 'test address Id';
		addOpt.regionID = region1.Id;
		addOpt.pcode = 'test pcode';
		addOpt.rickassessmnet = 'test rickassessmnet';
		addOpt.lng = 27.555014906999588;
		addOpt.lat = 153.02763973200058;

		insert lstsObject4;

		return mapsObject;
	}
}