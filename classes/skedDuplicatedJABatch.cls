global class skedDuplicatedJABatch implements Database.Batchable<sked__Resource__c>, Database.AllowsCallouts, Database.Stateful {
  List<sked__Resource__c> lstResources;
  public static final String dateFormat = 'yyyy-MM-dd';
  
  public skedDuplicatedJABatch() {
    this.lstResources = new List<sked__Resource__c>();
    for (sked__Resource__c skedRes : [SELECT Id, Name, sked__Primary_Region__c, sked__Primary_Region__r.Name, sked__Primary_Region__r.sked__Timezone__c,
                        (SELECT id, name, sked__Job__c, sked__Status__c, sked__Job__r.name, sked__Job__r.sked__Start__c, sked__Job__r.sked__Finish__c, sked_isDuplicated__c FROM sked__Job_Allocations__r              
                          WHERE sked__Status__c != :skedConstants.JOB_ALLOCATION_STATUS_COMPLETE 
                            AND sked__Status__c != :skedConstants.JOB_ALLOCATION_STATUS_DECLINED 
                            AND sked__Status__c != :skedConstants.JOB_ALLOCATION_STATUS_DELETED 
                          ORDER BY sked__Job__r.sked__Start__c)          
                      FROM sked__Resource__c 
                      WHERE sked__Is_Active__c = true
                      ]) {
      this.lstResources.add(skedRes);
    }
  }

  public skedDuplicatedJABatch(List<String> lstJAIds) {
    this.lstResources = new List<sked__Resource__c>();
    Set<String> setResIds = new Set<String>();
    for (sked__Job_Allocation__c ja : [SELECT Id, sked__Resource__c FROM sked__Job_Allocation__c WHERE Id IN :lstJAIds]) {
      setResIds.add(ja.sked__Resource__c);
    }

    for (sked__Resource__c skedRes : [SELECT Id, Name, sked__Primary_Region__c, sked__Primary_Region__r.Name, sked__Primary_Region__r.sked__Timezone__c,
                        (SELECT id, name, sked__Job__c, sked__Status__c, sked__Job__r.name, sked__Job__r.sked__Start__c, sked__Job__r.sked__Finish__c, sked_isDuplicated__c FROM sked__Job_Allocations__r              
                          WHERE sked__Status__c != :skedConstants.JOB_ALLOCATION_STATUS_COMPLETE 
                            AND sked__Status__c != :skedConstants.JOB_ALLOCATION_STATUS_DECLINED 
                            AND sked__Status__c != :skedConstants.JOB_ALLOCATION_STATUS_DELETED 
                          ORDER BY sked__Job__r.sked__Start__c)          
                      FROM sked__Resource__c 
                      WHERE sked__Is_Active__c = true
                      AND id IN :setResIds
                      ]) {
      this.lstResources.add(skedRes);
    }
  }

  global Iterable<sked__Resource__c> start(Database.BatchableContext BC) {
    return lstResources;
  }

  global void execute(Database.BatchableContext BC, List<sked__Resource__c> scope) {
    Map<id, sked__Job_Allocation__c> mapResult = new Map<id, sked__Job_Allocation__c>();

    for (sked__Resource__c res : scope) {
      System.debug('res = ' + res.name);
      List<sked__Job_Allocation__c> lstJAs = new List<sked__Job_Allocation__c>();
      Set<String> setCheckedIds = new Set<String>();
      Map<String, sked__Job_Allocation__c> mapDuplicate = new Map<String, sked__Job_Allocation__c>();
      

      //this is for case the number of job allocation is more than 200
      for (sked__Job_Allocation__c ja : res.sked__Job_Allocations__r) {
        lstJAs.add(ja);
      }

      for (sked__Job_Allocation__c ja : lstJAs) {
        String jobDate = ja.sked__Job__r.sked__Start__c.format(dateFormat);
        System.debug('1 ja = ' + ja.name + ', sked_isDuplicated__c = ' + ja.sked_isDuplicated__c);
        for (sked__Job_Allocation__c checkJA : lstJAs) {
          if (checkJA.id != ja.id && !setCheckedIds.contains(checkJA.id)) {
            String checkJobDate = checkJA.sked__Job__r.sked__Start__c.format(dateFormat);
            if (checkJobDate == jobDate) {
              DateTime jaStart = ja.sked__Job__r.sked__Start__c;
              DateTime jaFinish = ja.sked__Job__r.sked__Finish__c;
              DateTime checkStart = checkJA.sked__Job__r.sked__Start__c;
              DateTime checkFinish = checkJA.sked__Job__r.sked__Finish__c;
              if ((jaStart >= checkStart && jaFinish <= checkFinish) ||
                (jaStart <= checkStart && jaFinish >= checkFinish) ||
                (jaStart >= checkStart && jaStart <= checkFinish && jaFinish >= checkFinish) ||
                (jaStart <= checkStart && jaFinish >= checkStart && jaFinish <= checkFinish)) {
                  ja.sked_isDuplicated__c = true;
                  checkJa.sked_isDuplicated__c = true;
                  mapDuplicate.put(ja.id, ja);
                  mapDuplicate.put(checkJa.id, checkJa);
              }    
            }
          }
        }
        setCheckedIds.add(ja.id);
        
        if (!mapDuplicate.containsKey(ja.id)) {
            sked__Job_Allocation__c newJa = new sked__Job_Allocation__c(
               id = ja.id,
               sked_isDuplicated__c = false  
            );
            mapResult.put(ja.id, newJa);
        }
        

         for (sked__Job_Allocation__c ja2 : mapDuplicate.values()) {
            sked__Job_Allocation__c newJa = new sked__Job_Allocation__c(
               id = ja2.id,
               sked_isDuplicated__c = ja2.sked_isDuplicated__c
            );
            mapResult.put(ja2.id, newJa);
         }
        
      }
    }

    SavePoint sp = Database.setSavePoint();

    try {
      update mapResult.values();
    }
    catch(Exception ex) {
      System.debug('Error: ' + ex.getMessage());
      Database.rollback(sp);
    }
  }

  global void finish(Database.BatchableContext BC) {
      
  }

  public static void checkDuplicatedJA(List<sked__Job_Allocation__c> lstNews) {
    if (!System.isBatch()) {
      List<String> lstJAs = new List<String>();
          for (sked__Job_Allocation__c ja : lstNews) {
             // if (ja.sked__Status__c != skedConstants.JOB_ALLOCATION_STATUS_COMPLETE &&
              //        ja.sked__Status__c != skedConstants.JOB_ALLOCATION_STATUS_DECLINED &&
               //           ja.sked__Status__c != skedConstants.JOB_ALLOCATION_STATUS_DELETED) {
                  lstJAs.add(ja.id);   
             // }
          }

          if (!lstJAs.isEmpty()) {
              skedDuplicatedJABatch dupBatch = new skedDuplicatedJABatch(lstJAs);
              Database.executeBatch(dupBatch, 1);
          }  
    }
    }
}