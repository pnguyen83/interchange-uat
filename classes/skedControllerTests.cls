/*
* Unit tests for following controllers:
* 1. skedCustomLookupController
* 2. SkedClientServiceItemPopupController
* 3. SkedJobDetailController
*/
@isTest
private class skedControllerTests {
	private static String CLIENT_NAME = 'skedCustomLookupControllerTest';
    
    @testSetup static void setuptestData(){
		List<sked__Region__c> lstRegion = skedDataFactory.createRegions();
        Account acc = skedDataFactory.createAccount();
		Contact con = skedDataFactory.createEmployee(acc);
		Contact client = skedDataFactory.createClient(acc, CLIENT_NAME);
        enrtcr__Support_Contract_Item__c SupContract = skedDataFactory.createSupportContract(client);
        
        
		sked__Resource__c resource = new sked__Resource__c(
        	sked__User__c = UserInfo.getUserId(),
            Name			= 'Test Resource',
            sked__Primary_Region__c = lstRegion.get(0).Id
        );
        insert resource;
        //Create a job for SkedJobDetailController
        sked__Job__c Job = new sked__Job__c();
		Job.sked__Account__c = acc.id;
		Job.sked__Contact__c = con.Id;
        job.sked__Address__c = '79 mclachlan Fortitude Valley QLD';
		//Job.sked__Location__c = Loc.Id;
		Job.sked__Start__c = system.now();
		Job.sked__Finish__c = system.now().addHours(1);
		Job.sked__Duration__c = 60;
		Job.sked__Region__c = lstRegion.get(0).id;
		Job.sked__Job_Status__c = 'Dispatched';
        Job.Service_Agreement__c = SupContract.enrtcr__Support_Contract__c;
        Job.Service_Options__c = '[{"UnitOfMeasure":"Hours","ServiceId":"' + SupContract.Id + '","Rate":48.00,"Name":"AS"}]';
        Job.sked__Notes_Comments__c = 'Test Note';
       
		insert Job;
	}
    
    //Test to create Activity
	@isTest static void testskedCustomLookupControllerTest() {
        Contact client = [Select id from Contact where FirstName = :CLIENT_NAME limit 1];
        
		Test.startTest();
        
        PageReference page = new PageReference('/apex/skedCustomLookup?txt=j_id&contactId=' + client.Id + '&objectType=SAI&frm=j_id0&lksearch=&lksrch=SA');
        Test.setCurrentPage(page);        
        skedCustomLookupController controller = new skedCustomLookupController();
        controller.getFormTag();
        controller.getTextBox();
        controller.search();
        
        controller.searchString = null;//test error
        controller.search();
        
        Test.stopTest();

	}
    
    //Test to create Activity
	@isTest static void testskedCustomLookupControllerTest2() {
        Contact client = [Select id from Contact where FirstName = :CLIENT_NAME limit 1];
        
		Test.startTest();
        
        PageReference page = new PageReference('/apex/skedCustomLookup?txt=j_id&contactId=' + client.Id + '&objectType=SA&frm=j_id0&lksearch=&lksrch=');
        Test.setCurrentPage(page);        
        skedCustomLookupController controller = new skedCustomLookupController();
        
        controller.search();
        
        Test.stopTest();

	}

    //Test to create Activity
    @isTest static void testskedCustomLookupControllerTest3() {
        Contact client = [Select id from Contact where FirstName = :CLIENT_NAME limit 1];

        Test.startTest();

        PageReference page = new PageReference('/apex/skedCustomLookup?txt=j_id&contactId=' + client.Id + '&objectType=SVC&frm=j_id0&lksearch=&lksrch=SA');
        Test.setCurrentPage(page);
        skedCustomLookupController controller = new skedCustomLookupController();

        controller.search();

        Test.stopTest();

    }

    
    //Test to create Activity
	@isTest static void testSkedClientServiceItemPopupController() {
		Test.startTest();      
        
        SkedClientServiceItemPopupController controller = new SkedClientServiceItemPopupController();
        controller.query = 'SA';
        controller.runQuery();
        controller.getServiceAgreementItems();
        
        Test.stopTest();

	}
    
    //Test to create Activity
	@isTest static void testSkedJobDetailController() {
		Test.startTest();      
        sked__Job__c job = [Select Id from sked__Job__c limit 1];
        
        PageReference page = new PageReference('/apex/SkedJobDetail?jobId=' + job.Id);
        Test.setCurrentPage(page);     
        
        SkedJobDetailController controller = new SkedJobDetailController();
        controller.getServiceItems();
        controller.viewServiceItems();
        controller.addServiceItems();
        controller.cancel();
        controller.back();
        controller.saveServiceItem();
        
        Test.stopTest();

	}
}