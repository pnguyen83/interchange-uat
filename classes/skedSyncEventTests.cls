@isTest
public class skedSyncEventTests {
	@testSetup static void setuptestData(){
		setupData();
	}
    
    public static void setupData(){
        Account Acc = new Account
        (
            Name = 'Test Account'
        );
        insert Acc;
        Id devRecordTypeId = [Select Id from RecordType where SObjectType='Contact' and DeveloperName='Client'].Id;
        Contact Con = new Contact
        (
            FirstName = 'Test',
            LastName = 'Test',
            accountId = Acc.id,
            email = 'test@skedulo.com',
			enrtcr__Client_Region__c = 'Northern',
			Birthdate = System.today().addYears(-20),
			enrtcr__Status__c = 'Waiting',
			enrtcr__Sex__c = 'Male',
			enrtcr__Date_Client_Registered__c = System.today(),
			enrtcr__Primary_Disability__c = 'abc',
			Phone = '0411111111',
			enrtcr__Requires_Support_for_Decision_Making__c = 'No',
			enrtcr__Summary_Disability__c = 'AUtism',
            enrtcr__Primary_Referral_Source__c = 'Internet - EFA Website',
            OtherCity = 'NY',
            OtherPostalCode = '4006'
        );

       insert Con;
       sked__Region__c Reg = new sked__Region__c(Name='ACT');
       Reg.sked__Timezone__c='Australia/Sydney';
       insert Reg;

       sked__Location__c Loc = new sked__Location__c
        (
            Name = 'Test Location',
            sked__Account__c = Acc.Id,
            sked__Region__c = Reg.Id
        );
        insert Loc;

       sked__Resource__c Res = new sked__Resource__c(Name = 'Test Resource', sked__User__c=UserInfo.getUserId(), sked__Primary_Region__c = Reg.Id);
       insert Res;
    }
    
	//Test to create Event
	@isTest static void CreateEvent() {
        
   		Event e = new Event();
		e.Subject = 'Test';
		e.OwnerId = UserInfo.getUserId();
		e.Location = 'Test';
		e.StartDateTime = System.Now();
		e.EndDateTime = System.Now().addHours(1);
		e.Description = 'Test';
        e.enrtcr__Other_Country__c = 'AU';

		insert e;

		list<sked__Activity__c> lstActivity = [select id from sked__Activity__c];

		System.assertEquals(lstActivity.size(),1);

		delete e;
	}

	//Test to create Activity
	@isTest static void CreateActivity() {

       sked__Resource__c Res = [select Id, Name, sked__User__c from sked__Resource__c where sked__User__c=:UserInfo.getUserId()];

   		sked__Activity__c act = new sked__Activity__c();
		act.sked__Type__C = 'Dinner';
		act.sked__Notes__c = 'Have some dinner';
		act.sked__Start__c = system.now();
		act.sked__End__c = system.now().addHours(1);
		act.sked__Address__c = 'At Home';
		act.sked__Resource__c = res.id;

		insert act;

		list<Event> lstEvent = [select id from Event];

		System.assertEquals(lstEvent.size(),1);
	}

	//Test to create Activity
	@isTest static void CreateJobAndDispatch() {
        Account Acc =[select Id from Account limit 1];
        Contact Con = [Select Id from Contact limit 1];
        sked__Region__c Reg = [Select Id from sked__Region__c limit 1];
        sked__Location__c Loc = [Select Id from sked__Location__c limit 1];
        sked__Resource__c Res = [select Id, Name, sked__User__c from sked__Resource__c where sked__User__c=:UserInfo.getUserId()];

		sked__Job__c Job = new sked__Job__c();
		Job.sked__Account__c = Acc.id;
		Job.sked__Contact__c = Con.Id;
		Job.sked__Location__c = Loc.Id;
		Job.sked__Start__c = system.now();
		Job.sked__Finish__c = system.now().addHours(1);
		Job.sked__Duration__c = 60;
		Job.sked__Region__c = Reg.id;
		Job.sked__Job_Status__c = 'Dispatched';
		Job.sked__Address__c = '79 mclachlan';
       
		insert Job;

		sked__Job_Allocation__c Jall = new sked__Job_Allocation__c();
		Jall.sked__Job__c = Job.id;
		Jall.sked__Resource__c = Res.id;
		Jall.sked__Status__c = 'Pending Dispatch';

		insert Jall;

		Jall.sked__Status__c = 'Dispatched';
       
		update Jall;

		list<Event> lstEvent = [select id from Event];

		System.assertEquals(lstEvent.size(),1);

	}

	//Test to create Activity
	@isTest static void ChangeJobAllocationToDeleted() {
        Account Acc =[select Id from Account limit 1];
        Contact Con = [Select Id from Contact limit 1];
        sked__Region__c Reg = [Select Id from sked__Region__c limit 1];
        sked__Location__c Loc = [Select Id from sked__Location__c limit 1];
        sked__Resource__c Res = [select Id, Name, sked__User__c from sked__Resource__c where sked__User__c=:UserInfo.getUserId()];

		sked__Job__c Job = new sked__Job__c();
		Job.sked__Account__c = Acc.id;
		Job.sked__Contact__c = Con.Id;
		Job.sked__Location__c = Loc.Id;
		Job.sked__Start__c = system.now();
		Job.sked__Finish__c = system.now().addHours(1);
		Job.sked__Duration__c = 60;
		Job.sked__Region__c = Reg.id;
		Job.sked__Job_Status__c = 'Dispatched';
		Job.sked__Address__c = '79 mclachlan';
       
		insert Job;

		sked__Job_Allocation__c Jall = new sked__Job_Allocation__c();
		Jall.sked__Job__c = Job.id;
		Jall.sked__Resource__c = Res.id;
		Jall.sked__Status__c = 'Pending Dispatch';

		insert Jall;

		Test.startTest();

		Jall.sked__Status__c = 'Dispatched';
       
		update Jall;

		list<Event> lstEvent = [select id from Event];

		System.assertEquals(lstEvent.size(),1);

		Jall.sked__Status__c = 'Deleted';

		update Jall;

		lstEvent = [select id from Event];

		System.assertEquals(lstEvent.size(),0);

		Test.stopTest();
	}


	//Test to create Activity
	@isTest static void DeleteJobAllocation() {
        Account Acc =[select Id from Account limit 1];
        Contact Con = [Select Id from Contact limit 1];
        sked__Region__c Reg = [Select Id from sked__Region__c limit 1];
        sked__Location__c Loc = [Select Id from sked__Location__c limit 1];
        sked__Resource__c Res = [select Id, Name, sked__User__c from sked__Resource__c where sked__User__c=:UserInfo.getUserId()];

		sked__Job__c Job = new sked__Job__c();
		Job.sked__Account__c = Acc.id;
		Job.sked__Contact__c = Con.Id;
		Job.sked__Location__c = Loc.Id;
		Job.sked__Start__c = system.now();
		Job.sked__Finish__c = system.now().addHours(1);
		Job.sked__Duration__c = 60;
		Job.sked__Region__c = Reg.id;
		Job.sked__Job_Status__c = 'Dispatched';
		Job.sked__Address__c = '79 mclachlan';
       
		insert Job;

		sked__Job_Allocation__c Jall = new sked__Job_Allocation__c();
		Jall.sked__Job__c = Job.id;
		Jall.sked__Resource__c = Res.id;
		Jall.sked__Status__c = 'Pending Dispatch';

		insert Jall;

		Test.startTest();

		Jall.sked__Status__c = 'Dispatched';
       
		update Jall;

		list<Event> lstEvent = [select id from Event];

		System.assertEquals(lstEvent.size(),1);

		delete [select id, Name from sked__Job_Allocation__c where Id=:Jall.Id];
		
		lstEvent = [select id from Event];

		System.assertEquals(lstEvent.size(),0);

		Test.stopTest();
	}

	//Test to create Activity
	@isTest static void ChangeJobStartFinishTime() {
        Account Acc =[select Id from Account limit 1];
        Contact Con = [Select Id from Contact limit 1];
        sked__Region__c Reg = [Select Id from sked__Region__c limit 1];
        sked__Location__c Loc = [Select Id from sked__Location__c limit 1];
        sked__Resource__c Res = [select Id, Name, sked__User__c from sked__Resource__c where sked__User__c=:UserInfo.getUserId()];

		sked__Job__c Job = new sked__Job__c();
		Job.sked__Account__c = Acc.id;
		Job.sked__Contact__c = Con.Id;
		Job.sked__Location__c = Loc.Id;
		Job.sked__Start__c = system.now();
		Job.sked__Finish__c = system.now().addHours(1);
		Job.sked__Duration__c = 60;
		Job.sked__Region__c = Reg.id;
		Job.sked__Job_Status__c = 'Dispatched';
		Job.sked__Address__c = '79 mclachlan';
       
		insert Job;

		sked__Job_Allocation__c Jall = new sked__Job_Allocation__c();
		Jall.sked__Job__c = Job.id;
		Jall.sked__Resource__c = Res.id;
		Jall.sked__Status__c = 'Pending Dispatch';

		insert Jall;

		Test.startTest();

		Jall.sked__Status__c = 'Dispatched';
       
		update Jall;

		list<Event> lstEvent = [select id from Event];

		System.assertEquals(lstEvent.size(),1);

		Job.sked__Start__c = system.now();
		Job.sked__Finish__c = system.now().addHours(2);
		Job.sked__Duration__c = 120;

		update Job;

		Test.stopTest();
	}
	
	
}