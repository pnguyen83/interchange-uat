public class skedRosterManagementController  extends skedCommonModels{
	public static String DATE_FORMAT = 'yyyy-MM-dd';

    public String locationOrSiteFilter{
        get{
            return skedConfigs.LOCATION_OR_SITE_FILTER;
        }
    }

    /*
    * Get Config data
    */
    @RemoteAction
    public static ConfigData getConfigData(){
        return skedCommonModels.getConfigData(skedCommonModels.ROSTER_MANAGEMENT);
    }

    /*
    *
    */
    @RemoteAction
    public static void cancelJob(String jobId, String abortReason, boolean allJobs){
        list<sked__Job__c> jobs = skedCommonModels.getRecurringJobs(jobId, allJobs);
        for(sked__Job__c job : jobs){
            job.sked__Job_Status__c = 'Cancelled';
            job.sked__Abort_Reason__c = abortReason;
        }
        update jobs;
    }

    /*
    *
    */
    @RemoteAction
    public static ActionResult dispatchJob(String jobId){
        
        try{
            sked__Job__c job = [Select Id, sked__Job_Status__c from sked__Job__c where Id=:jobId];
            job.sked__Job_Status__c = SkeduloConstants.JOB_STATUS_DISPATCHED;
            //update job;
            skedSkeduloAPIManager.dispatchJobs(new Set<Id>{ job.Id });
        }catch(Exception ex){
            return new ActionResult(true, ex.getMessage() + ' ' + ex.getStackTraceString());
        }
        return new ActionResult(false,'');
    }

    /*
    *
    */
    @RemoteAction
    public static ActionResult rescheduleJob(String recId, String dateString, Integer startTime){
        sked__Job__c job    = [select Id, sked__Start__c, sked__Finish__c, sked__Duration__c from sked__Job__c where Id=:recId];
        Date startDate      = skedCommonModels.toDate(dateString);
        job.sked__Start__c  = DateTime.newInstance(startDate, Time.newInstance(0, skedTimezoneUtil.toMinute(startTime),0,0));
        job.sked__Finish__c = job.sked__Start__c.addMinutes( Integer.valueOf(job.sked__Duration__c) );
        update job;
        return new ActionResult(false, '');
    }

    /*
    * Unschedule a job
    */
    @RemoteAction
    public static ActionResult unscheduleJob(String jobId){
        //Savepoint sp = Database.setSavepoint();
        
        try{
            sked__Job__c job    = [select Id, sked__Job_Status__c from sked__Job__c where Id=:jobId];
            job.sked__Job_Status__c = 'Pending Allocation';
            //Cancell job allocation
            update job;
            list<sked__Job_Allocation__c> jaList = [Select Id from sked__Job_Allocation__c where sked__Job__c=:job.Id];
            if(!jaList.isEmpty()){
                for(sked__Job_Allocation__c ja : jaList){
                    ja.sked__Status__c = 'Deleted';
                }
                update jaList;
            } 
            
        }catch(Exception ex){
           // Database.rollback(sp);
            return new ActionResult(true, ex.getMessage() + ' ' + ex.getStackTraceString());
        }
        return new ActionResult(false,'');
    }

    /**
    * getShiftsByRetion
    * @param  string regionId 
    * @param  string dateString   'YYYY-MM-DD'
    * @param  string period       'Day|Week|2 Weeks|Month|Current Fortnightly Roster'
    * @param  string locationId   null : load all
    * @return list                dateSlots starting from Thu and include selectedDate
   */
    @RemoteAction
    public static ActionResult getShiftsByRegion(String regionId, String dateString, String period, String locationId, String clientId, String resourceId){
        ActionResult ar = new ActionResult(false,'');
        
        try{
            DateTime startTime = skedCommonModels.getStartTime(dateString, period);
            DateTime endTime = skedCommonModels.getEndTime(startTime, period);
            //Query Group Event Jobs
            Set<Id> geJobIDs = new Set<Id>();
            for(Group_Attendee__c ga : [Select Id, Job__c from Group_Attendee__c where Contact__c=:clientId]){
                geJobIDs.add( ga.Job__c );
            }
            //Query jobs
            list<sked__Job__c> jobs = [Select Id, Name, sked__Job_Status__c, sked__Description__c, sked__Address__c, sked__Duration__c, sked__Region__c,sked__Location__c, sked__Notes_Comments__c, sked__Start__c, sked__Finish__c, 
                                        //Job_Type__c, Travel_Time__c,
                                        Job_Type__c, Site__c,
                                       sked__Contact__r.Name, sked__Contact__r.OtherStreet, sked__Contact__r.OtherCity, sked__Contact__r.OtherState, sked__Contact__r.OtherPostalCode, sked__Contact__r.OtherCountry , sked__Contact__r.Phone, sked__Contact__r.MobilePhone,
                                       sked__Contact__r.enrtcr__Sex__c,
                                       sked__Actual_Start__c, sked__Actual_End__c, Group_Event__c, Group_Event__r.Name, Group_Event__r.Description__c, 
                                       //Group_Event__r.Site__r.Name, 
                                       Group_Event__r.Alternative_Address__c, Group_Event__r.Coordinator__c, Group_Event__r.Coordinator__r.Name,
                                       Service__r.enrtcr__Service_Type__c,
                                       //Group_Event__r.Service_Type__c, 
                                       //Visible_in_Mobile__c, Scheduled_on_RDO__c,
                                            (Select Id, sked__Estimated_Travel_Time__c , sked__Resource__r.Id, sked__Resource__r.Name, sked__Resource__r.sked__Home_Address__c, sked__Resource__r.sked__Primary_Phone__c , 
                                                sked__Resource__r.sked__Mobile_Phone__c from sked__Job_Allocations__r
                                             where (NOT sked__Status__c IN ('Deleted','Declined', 'Pending Dispatch'))) ,
                                            (Select Id, sked__Tag__c from sked__JobTags__r where sked__Required__c =true)
                                       from sked__Job__c where Id!=null and sked__Job_Status__c NOT IN ('Cancelled','Complete')
                                       //and ((sked__Start__c >= :startTime and sked__Start__c <=:endTime) OR (sked__Finish__c >= :startTime and sked__Finish__c <=:endTime)) order by sked__Start__c];
                                       and ((sked__Start__c < :endTime) AND (sked__Finish__c > :startTime)) order by sked__Start__c];
            
            list<DateSlotModel> result = new list<DateSlotModel>();
            Map<String,DateSlotModel> mapDateToDateSlot = new Map<String,DateSlotModel>();

            DateTime tmpStart = startTime;
            while(tmpStart < endTime){
                mapDateToDateSlot.put(tmpStart.format(DATE_FORMAT), new DateSlotModel(tmpStart));
                tmpStart = tmpStart.addDays(1);
            }

            Id rId = String.isBlank(regionId)?null: (Id)(regionId);
            Id lId = String.isBlank(locationId)?null: (Id)(locationId);
            Id cId = String.isBlank(clientId)?null: (Id)(clientId);
            Id rsId = String.isBlank(resourceId)?null: (Id)(resourceId);

            for(sked__Job__c job : jobs){

                //Filter by region & location & service type
                if(rId != null && job.sked__Region__c != rId ) continue;
                if(lId != null && !(skedConfigs.LOCATION_OR_SITE_FILTER=='Location' && job.sked__Location__c == lId)  && !(skedConfigs.LOCATION_OR_SITE_FILTER!='Location' && job.Site__c == lId)) continue;
                if(cId != null && ( !(job.Group_Event__c==null && job.sked__Contact__c == cId) && !(job.Group_Event__c!=null && geJobIDs.contains(job.Id)))) continue;
                //Resource Filter
                if(rsId != null){
                    boolean isAllocated = false;
                    for(sked__Job_Allocation__c ja : job.sked__Job_Allocations__r){
                        if(ja.sked__Resource__c == rsId) isAllocated = true;
                    }
                    if(!isAllocated) continue;
                }
                String key = job.sked__Start__c.format(DATE_FORMAT);
                //if(job.sked__Finish__c >= startTime && job.sked__Finish__c <= endTime) key = job.sked__Finish__c.format(DATE_FORMAT);//Overtime job which start time is out of selected period
                if( !mapDateToDateSlot.containsKey(key) ){
                    if(job.sked__Start__c >= startTime && job.sked__Start__c <= endTime)
                    mapDateToDateSlot.put(key, new DateSlotModel(job.sked__Start__c));
                }
                
                DateTime start = job.sked__Start__c;
                DateTime finish = job.sked__Finish__c;
                DateTime nextDate = DateTime.newInstance(Date.newInstance(start.year(), start.month(), start.day()).addDays(1), Time.newInstance(0,0,0,0 ));
                if(finish < nextDate){
                    if(mapDateToDateSlot.containsKey(key)) mapDateToDateSlot.get(key).slots.add( new SlotModel(job, null, null) );
                    
                }else{//Overnight job
                    DateTime originalStart = job.sked__Start__c;
                    DateTime originalFinish = job.sked__Finish__c;
                    sked__Job__c job1 = job.clone(true, true, true, true);
                    job1.sked__Finish__c = nextDate;
                    job1.sked__Duration__c = (job1.sked__Finish__c.getTime() - job1.sked__Start__c.getTime())/(1000*60);
                    if(job1.sked__Start__c >= startTime && job1.sked__Finish__c <= endTime){
                        SlotModel sm =  new SlotModel(job1, originalStart, originalFinish) ;
                        sm.finish.id = 2400;
                        if(mapDateToDateSlot.containsKey(key)) mapDateToDateSlot.get(key).slots.add(sm);
                    }
                    sked__Job__c job2 = job.clone(true, true, true, true);
                    job2.sked__Start__c = nextDate;
                    job2.sked__Duration__c = (job2.sked__Finish__c.getTime() - job2.sked__Start__c.getTime())/(1000*60);
                    
                    if(job2.sked__Start__c >= startTime && job2.sked__Finish__c <= endTime){
                        String key2 = job2.sked__Finish__c.format(DATE_FORMAT);
                        if(mapDateToDateSlot.containsKey(key2)) mapDateToDateSlot.get(key2).slots.add( new SlotModel(job2, originalStart, originalFinish) );
                    }
                    
                }
               
            }
            list<DateSlotModel> dateSlots = mapDateToDateSlot.values();
            populatejobInfo(dateSlots);
            ar.result = dateSlots;
        }catch(Exception ex){
            return new ActionResult(true, ex.getMessage() + ' ' + ex.getStackTraceString());
        }
        return ar;
    }

    /*
    *
    */
    public static void populatejobInfo(list<DateSlotModel> dateSlots){
        Set<String> jobIds = new Set<String>();
        for(DateSlotModel dsm : dateSlots){
            for(SlotModel slot : dsm.slots){
                jobIds.add( slot.id );
            }
        }
        
        String query = 'Select Id ';
        for(String field : skedCommonModels.getFields(skedCommonModels.JOB_HOVER_FIELDS)){
            if(!query.containsIgnoreCase(field)) query += ',' + field;
        }
        query += ' from sked__Job__c where Id=:jobIds';

        for(sked__Job__c job : Database.query(query)){
            for(DateSlotModel dsm : dateSlots){
                for(SlotModel slot : dsm.slots){
                    if(job.Id == (Id)(slot.id)){
                        slot.populateJobInfo(job);
                        break;
                    }
                }
            }
        }
    }
}