/**
* @author admin@skedulo.com.crdev
* @description 
*/
public class skedJobTriggerHandler
{

    //Revent loop when create group attendee
    public static boolean IsFromGenerateAttendee = false;
	/**
	* @description 
	* @param lstInsJobs 
	*/
    public static void generateGroupAttendee(list<sked__Job__c> lstInsJobs)
    {
        set<id> groupID = new set<id>();

        list<Group_Attendee__c> insAtt = new list<Group_Attendee__c>();

        for (sked__job__c job: lstInsJobs)
        {
            if (job.Group_Event__c != null)
            {
                groupId.add(job.Group_Event__c);
            }
        }
        system.debug('#groupId# ' + groupId);

        map<id, Group_Event__c> map_id_Group = new map<id, Group_Event__c>([
                select id,
                (
                        select id, client__c,
                                client__r.AccountId,
                                client__r.Name,
                                Service_Agreement_Item__r.enrtcr__Rate__r.enrtcr__Quantity_Type__c,
                                Rate__r.enrtcr__Quantity_Type__c,
                                Service_Agreement_Item__c,
                                Service__c,
                                Site__c,
                                Rate__c,
                                Program__c,
                                client__r.MobilePhone
                        from Group_Clients__r
                )
                from Group_Event__c
                where id in:groupId
        ]);

        for (sked__job__c job: lstInsJobs)
        {
            if (job.Group_Event__c != null && map_id_Group.containskey(job.Group_Event__c))
            {
                Group_Event__c GE = map_id_Group.get(job.Group_Event__c);

                for (Group_Client__c GEC : GE.Group_Clients__r)
                {
                    Group_Attendee__c Att = new Group_Attendee__c();

                    att.Job__c = job.id;
                    att.Contact__c = GEC.Client__c;
                    att.Account__c = GEC.client__r.AccountId;
                    insAtt.add(Att);

                }

            }
        }

        if (insAtt.size() > 0)
        {
            insert insAtt;
        }
    }

    public static void removeGroupAttendeeServiceItem(Map<Id, Group_Attendee__c> oldMap)
    {
        Set<String> deleteableStatuses = new Set<String>
        {
                'Queued', 'Pending Allocation', 'Pending Dispatch'
        };

        Set<Id> deleteIds = new Set<Id>();
        List<Group_Attendee__c> toDelete =
        [
                select Id, Name, Job__r.sked__Job_Status__c
                from Group_Attendee__c
                where Id in :oldMap.keySet()
        ];

        for (Group_Attendee__c groupAttendee : toDelete)
        {
            if (!deleteableStatuses.contains(groupAttendee.Job__r.sked__Job_Status__c))
            {
                oldMap.get(groupAttendee.Id).addError(new CustomException('You can not remove an attendee after the Job has been dispatched.'));
                continue;
            }
            deleteIds.add(groupAttendee.Id);
        }


        delete [select Id from Job_Service_Item__c where Group_Attendee__c in :deleteIds];
    }

    public static void generateGroupAttendeeServiceItem(Map<Id, Group_Attendee__c> newMap)
    {
        Set<Id> jobIds = new Set<Id>();

        // Le fix reschedule Jobs schedule
        Map<Id,map<id, Group_Attendee__c>> groupattendeesByContactIds = new Map<id,map<Id, Group_Attendee__c>>();
        for (Group_Attendee__c attendee : newMap.values())
        {
            map<id, Group_Attendee__c> tempMapIdAttendee = new map<id, Group_Attendee__c>();
            if(groupattendeesByContactIds.containsKey(attendee.Job__c)) tempMapIdAttendee = groupattendeesByContactIds.get(attendee.Job__c);
            tempMapIdAttendee.put(attendee.Contact__c, attendee);
            groupattendeesByContactIds.put(attendee.Job__c, tempMapIdAttendee);
            jobIds.add(attendee.Job__c);
        }
        List<sked__Job__c> skedJobs =
        [
                select id, sked__duration__c, Group_Event__c,
                (select id, Contact__c, Account__c from Group_Attendees__r),
                (
                        select
                                Billable__c,
                                Cancellation__c,
                                Cancellation_reason__c,
                                Comment__c,
                                Group_Attendee__c,
                                Id,
                                Job__c,
                                Quantity__c,
                                Related_Id__c,
                                Resource__c,
                                Service_Agreement_Item__c,
                                sked_Service_Delivered__c,
                                SystemModstamp,
                                Travel_Kms__c,
                                UOM__c,
                                Worker__c,
                                Group_Attendee__r.Contact__c,
                                Group_Attendee__r.Attended__c
                        from Job_Service_Item__r
                )
                from sked__job__c
                where id in :jobIds
        ];

        Map<Id, set<enrtcr__Support_Delivered__c>> sdMap = new Map<Id, set<enrtcr__Support_Delivered__c>>();

        for (enrtcr__Support_Delivered__c sd : [SELECT Id, enrtcr__Worker__c, job_service_Item__c FROM enrtcr__Support_Delivered__c where job_service_Item__r.Job__c in:jobIds])
        {
            if (!sdMap.containsKey(sd.Job_Service_Item__c))
                sdMap.put(sd.Job_Service_Item__c, new Set<enrtcr__Support_Delivered__c>());

            sdMap.get(sd.Job_Service_Item__c).add(sd);
        }

        Set<Id> groupEventIds = new Set<Id>();
        for (sked__Job__c job : skedJobs)
        {
            groupEventIds.add(job.Group_Event__c);
        }

        map<id, Group_Event__c> map_id_Group = new map<id, Group_Event__c>([
                select id,
                (
                        select id, client__c,
                                client__r.AccountId,
                                client__r.Name,
                                Service_Agreement_Item__r.enrtcr__Rate__r.enrtcr__Quantity_Type__c,
                                Rate__r.enrtcr__Quantity_Type__c,
                                Service_Agreement_Item__c,
                                Service_Agreement_Item__r.enrtcr__Site_Service_Program__c,
                                Service_Agreement_Item__r.enrtcr__Site__c ,
                                Service_Agreement_Item__r.enrtcr__Rate__c,
                                Service_Agreement_Item__r.enrtcr__Service__c,
                                Service__c,
                                Site__c,
                                Rate__c,
                                Program__c,
                                client__r.MobilePhone,
                                Delivery_Method__c,
                                Quantity__c
                        from Group_Clients__r
                )
                from Group_Event__c
                where id in:groupEventIds
        ]);

        Set<Id> siteIDs = new Set<Id>();
        Set<Id> serviceIDs = new Set<Id>();
        for(Group_Event__c ge : map_id_Group.values()){
            for(Group_Client__c ga : ge.Group_Clients__r){
                if(ga.Service_Agreement_Item__r.enrtcr__Site_Service_Program__c!=null) continue;
                if(ga.Site__c!=null) siteIDs.add(ga.Site__c);
                if(ga.Service__c!=null) serviceIDs.add(ga.Service__c);
            }
        }
        //Search Site Service Programme
        list<enrtcr__Site_Service__c> siteServiceList = [Select ID, enrtcr__Site__c,enrtcr__Service__c  from enrtcr__Site_Service__c where enrtcr__Site__c IN :siteIDs and enrtcr__Service__c IN :serviceIDs order by enrtcr__Program__c asc nulls first];
        
        List<Job_Service_Item__c> jobServiceItemsToUpsert = new List<Job_Service_Item__c>();
        List<enrtcr__Support_Delivered__c> servicesDeliveredToUpdate = new List<enrtcr__Support_Delivered__c>();
        for (sked__job__c job: skedJobs)
        {
            if (job.Group_Event__c != null && map_id_Group.containskey(job.Group_Event__c))
            {
                Group_Event__c groupEvent = map_id_Group.get(job.Group_Event__c);

                Map<Id, Job_Service_Item__c> jobServiceItemsByContactIds = new Map<Id, Job_Service_Item__c>();
                for (Job_Service_Item__c jobServiceItem : job.Job_Service_Item__r)
                {
                    jobServiceItemsByContactIds.put(jobServiceItem.Group_Attendee__r.Contact__c, jobServiceItem);
                }

                for (Group_Client__c groupClient : groupEvent.Group_Clients__r)
                {
                    //Le fixed generate group attendee issue
                    if(!groupattendeesByContactIds.containsKey(job.id)){
                        continue;
                    }
                    map<id, Group_Attendee__c> tempMapIdAttendee = groupattendeesByContactIds.get(job.id);

                    Group_Attendee__c att = tempMapIdAttendee.get(groupClient.Client__c);
                    if (att == null) continue;
                    Job_Service_Item__c si = jobServiceItemsByContactIds.containsKey(groupClient.Client__c) ?
                            jobServiceItemsByContactIds.get(groupClient.Client__c) : new Job_Service_Item__c();

                    si.Group_Attendee__c = att.id;
                    si.Job__c = job.id;
                    si.Billable__c = true;
                    si.Quantity__c = skedTriggerUtil.getQuantity(job, groupClient);
                    si.UOM__c = groupClient.Rate__c != null ? groupClient.Rate__r.enrtcr__Quantity_Type__c : groupClient.Service_Agreement_Item__r.enrtcr__Rate__r.enrtcr__Quantity_Type__c;
                    si.Service_Agreement_Item__c = groupClient.Service_Agreement_Item__c;
                    si.Site__c = groupClient.Site__c!=null? groupClient.Site__c : groupClient.Service_Agreement_Item__r.enrtcr__Site__c;
                    si.Rate__c = groupClient.Rate__c!=null? groupClient.Rate__c : groupClient.Service_Agreement_Item__r.enrtcr__Rate__c;
                    si.Service__c = groupClient.Service__c!=null? groupClient.Service__c : groupClient.Service_Agreement_Item__r.enrtcr__Service__c;
                    si.Program__c = groupClient.Program__c;
                    si.Delivery_Method__c = groupClient.Delivery_Method__c;
                    //Populate Site Service/Program
                    if(groupClient.Service_Agreement_Item__r.enrtcr__Site_Service_Program__c!=null) si.Site_Service_Program__c = groupClient.Service_Agreement_Item__r.enrtcr__Site_Service_Program__c;
                    else{
                        for(enrtcr__Site_Service__c ss : siteServiceList){
                            if(si.Site__c == ss.enrtcr__Site__c && si.Service__c == ss.enrtcr__Service__c){
                                si.Site_Service_Program__c = ss.Id;
                                break;
                            }
                        }
                    }

                    if (sdMap.containsKey(si.id))
                    {
                        for (enrtcr__Support_Delivered__c sd : sdMap.get(si.id))
                        {
                            sd.enrtcr__Cancellation__c = !si.Group_Attendee__r.Attended__c;
                            sd.enrtcr__Cancellation_reason__c = (!si.Group_Attendee__r.Attended__c) ? 'Did Not Attend' : si.Cancellation_reason__c;

                            servicesDeliveredToUpdate.add(sd);
                        }
                    }
                    jobServiceItemsToUpsert.add(si);
                }

            }
        }
        upsert jobServiceItemsToUpsert;

        update servicesDeliveredToUpdate;
    }


    //generate Attendee Attachment after insert an attendee to Job Dispatched or Ready

    public static void createAttendeeAttachment(list<Group_Attendee__c> gAttendees){
        list<id> attendeeids = new list<id>();
        for(Group_Attendee__c gatt: [select id from Group_Attendee__c where id in: gAttendees and (Job__r.sked__Job_Status__c IN (:SkeduloConstants.JOB_STATUS_DISPATCHED, :SkeduloConstants.JOB_STATUS_READY, :SkeduloConstants.JOB_STATUS_IN_PROGRESS, :SkeduloConstants.JOB_STATUS_EN_ROUTE, :SkeduloConstants.JOB_STATUS_ON_SITE, :SkeduloConstants.JOB_STATUS_COMPLETE) )]){
            attendeeids.add(gatt.id);
        }

        if(attendeeids.size() > 0){
            skedAttendeeAvailableServiceBatch b = new skedAttendeeAvailableServiceBatch(attendeeids,UserInfo.getSessionId());
            Database.executeBatch(b,5);
        }
    }

	/**
	*@description all up update Note from Jobs and update/insert note in contact
	*
	*/
    public static void generateContactNote(list<sked__Job__c> lstUpJobs, map<id, sked__Job__c> oldMap)
    {
        Id devRecordTypeId = skedTestDataFactory.getRecordTypeIDByDeveloperName('Contact','Employee');

        List<enrtcr__Note__c> insNotes = new list<enrtcr__Note__c>();
        set<id> contactIds = new set<id>();
        set<string> noteName = new set<string>();
        map<string, enrtcr__Note__c> map_conID_Note = new map<string, enrtcr__Note__c>();
        list<sked__Job__c> jobs =
        [
                select id, Name, sked__Type__c, sked__Contact__c, sked__Notes_Comments__c, sked__Contact__r.Name, sked__Contact__r.RecordType.DeveloperName, (SELECT Contact__c, Contact__r.Name, Note__c FROM Group_Attendees__r)
                from sked__Job__c
                where id in:lstUpJobs
        ]; // WHEN WOULD WE NOT WANT TO CREATE A NOTE? HOW ARE THESE NOT LIMITED TO CLIENT? and sked__Contact__r.RecordTypeId <> :devRecordTypeId];
        for (sked__Job__c job: jobs)
        {
            if (job.sked__Contact__c != null && job.sked__Contact__r.RecordType.DeveloperName == 'Client' && job.sked__Notes_Comments__c != oldMap.get(job.id).sked__Notes_Comments__c && oldMap.get(job.id).sked__Job_Status__c != 'Complete')
            {
                contactIds.add(job.sked__Contact__c);
                noteName.add(job.Name + ' Note');
            }
            else if (job.Group_Attendees__r != null && job.sked__Notes_Comments__c != oldMap.get(job.id).sked__Notes_Comments__c && oldMap.get(job.id).sked__Job_Status__c != 'Complete')
            {
                for (Group_Attendee__c groupAttendee : job.Group_Attendees__r)
                {
                    contactIds.add(groupAttendee.Contact__c);
                }
                noteName.add(job.Name + +' Note');
            }
        }


        enrtcr__Settings__c settings = enrtcr__Settings__c.getOrgDefaults()!= null?enrtcr__Settings__c.getOrgDefaults():new enrtcr__Settings__c();

        String[] confidentialNoteTypes = settings.enrtcr__Confidential_Note_Types__c!=null?enrtcr__Settings__c.getOrgDefaults().enrtcr__Confidential_Note_Types__c.split(';'):
            new List<String>();

        for (enrtcr__Note__c careNote :
        [
                select id, Name, enrtcr__Client__c, enrtcr__Parent_Id__c, enrtcr__Description__c
                from enrtcr__Note__c
                where enrtcr__Client__c in:contactIds and Name in:noteName
                and enrtcr__Type__c in : confidentialNoteTypes
        ])
        {
            map_conID_Note.put(careNote.enrtcr__Client__c + careNote.Name, careNote);
        }

        for (sked__Job__c job: jobs)
        {
            if (job.sked__Contact__c != null && job.sked__Contact__r.RecordType.DeveloperName == 'Client' && job.sked__Notes_Comments__c != oldMap.get(job.id).sked__Notes_Comments__c)
            {
                enrtcr__Note__c note = new enrtcr__Note__c();
                if (map_conID_Note.containskey(job.sked__Contact__c + job.Name + ' Note'))
                {
                    note = map_conID_Note.get(job.sked__Contact__c + job.Name + ' Note');
                }

                note.enrtcr__Client__c = job.sked__Contact__c;
                note.enrtcr__Parent_Id__c = job.sked__Contact__c;
                note.Name = job.Name + ' Note';
                note.enrtcr__Type__c = 'Progress Note';
                note.enrtcr__Description__c = job.sked__Notes_Comments__c;
                note.enrtcr__Parent_Name__c = job.sked__Contact__r.Name;
                note.Job__c = job.id;

                insNotes.add(note);
            }
            else if (job.Group_Attendees__r != null && job.sked__Notes_Comments__c != oldMap.get(job.id).sked__Notes_Comments__c)
            {
                for (Group_Attendee__c groupAttendee : job.Group_Attendees__r)
                {
                    enrtcr__Note__c note = new enrtcr__Note__c();
                    if (map_conID_Note.containskey(groupAttendee.Contact__c + job.Name + ' Note'))
                    {
                        note = map_conID_Note.get(groupAttendee.Contact__c + job.Name + ' Note');
                    }

                    note.enrtcr__Client__c = groupAttendee.Contact__c;
                    note.enrtcr__Parent_Id__c = groupAttendee.Contact__c;
                    note.Name = job.Name + ' Note';
                    note.enrtcr__Type__c = 'Progress Note';
                    note.enrtcr__Description__c = job.sked__Notes_Comments__c;
                    note.enrtcr__Parent_Name__c = groupAttendee.Contact__r.Name;
                    note.Job__c = job.id;

                    insNotes.add(note);
                }
            }
        }
        upsert insNotes;
    }
    
    /**
	*@description all up update Note from Group Attendee and update/insert note in contact
	*
	*/
    public static void generateContactNote(list<Group_Attendee__c > newList, map<id, Group_Attendee__c > oldMap)
    {

        set<id> contactIds = new set<id>();
        set<id> gaIDs = new set<id>();

        Group_Attendee__c oldRec;
        for (Group_Attendee__c newRec: newList)
        {
            oldRec = oldMap == null ? null : oldMap.get(newRec.Id);
            if ((oldRec == null && newRec.Note__c != null) || oldRec.Note__c != newRec.Note__c)
            {
                contactIds.add(newRec.Contact__c);
                gaIDs.add(newRec.Id);
            }
        }

        Map<Id, Contact> contactMap = new Map<Id, Contact>([Select Id, Name from Contact where Id IN :contactIds]);
        List<enrtcr__Note__c> notesToUpsert = new list<enrtcr__Note__c>();
        map<string, enrtcr__Note__c> mapGAIDToNote = new map<string, enrtcr__Note__c>();

        enrtcr__Settings__c settings = enrtcr__Settings__c.getOrgDefaults()!= null?enrtcr__Settings__c.getOrgDefaults():new enrtcr__Settings__c();

        String[] confidentialNoteTypes = settings.enrtcr__Confidential_Note_Types__c!=null?enrtcr__Settings__c.getOrgDefaults().enrtcr__Confidential_Note_Types__c.split(';'):
                new List<String>();

        for (enrtcr__Note__c careNote :
        [
                select id, Name, enrtcr__Client__c, enrtcr__Parent_Id__c, enrtcr__Description__c, Group_Attendee__c
                from enrtcr__Note__c
                where Group_Attendee__c in:gaIDs
                and enrtcr__Type__c in : confidentialNoteTypes
        ])
        {
            mapGAIDToNote.put(careNote.Group_Attendee__c, careNote);
        }

        for (Group_Attendee__c ga: newList)
        {
            if (!gaIDs.contains(ga.Id)) continue;//GA is updated but Note is not changed or GA is created but Note is empty

            enrtcr__Note__c note = new enrtcr__Note__c(
                    Name = ga.Name + ' Note',
                    enrtcr__Type__c = 'Progress Note',
                    Group_Attendee__c = ga.id,
                    enrtcr__Description__c = ga.Note__c,
                    enrtcr__Parent_Name__c = contactMap.containsKey(ga.Contact__c) ? contactMap.get(ga.Contact__c).Name : '',
                    enrtcr__Client__c = ga.Contact__c,
                    enrtcr__Parent_Id__c = ga.Contact__c
            );

            if (mapGAIDToNote.containskey(ga.Id))
            {
                note.Id = mapGAIDToNote.get(ga.Id).Id;
            }

            notesToUpsert.add(note);
        }
        upsert notesToUpsert;
    }

    public class CustomException extends Exception
    {
    }
}