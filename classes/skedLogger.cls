public class skedLogger {
	public static String INFO 			= 'Info';
	public static String DEBUG 			= 'Debug';
	public static String WARNING 		= 'Warning';
	public static String ERROR 			= 'Error';

	 public static Boolean IS_ENABLED{
        get{
            if(skedConfigs__c.getAll().containsKey('Enable_Logger')){
                IS_ENABLED     = Boolean.valueOf(skedConfigs__c.getAll().get('Enable_Logger').Value__c);
            }else IS_ENABLED   = false;
            return IS_ENABLED;
        }
        set;
    }

     public static Integer MAX_MESSAGE_LENGTH{
        get{
            if(skedConfigs__c.getAll().containsKey('Max_Message_Length')){
                MAX_MESSAGE_LENGTH     = Integer.valueOf(skedConfigs__c.getAll().get('Max_Message_Length').Value__c);
            }else MAX_MESSAGE_LENGTH   = 1000;
            return MAX_MESSAGE_LENGTH;
        }
        set;
    }

	public String message;
	public String level;
	public String tags;

	public skedLogger(){
		this.message    = '';
		this.level      = INFO;
	}

	public skedLogger(String msg, String level, String tags){
		this.message    = msg;
		this.level 		= level;
		this.tags 		= tags;
	}

	public static Logger__c newInstance(String msg, String level, String tags){
		skedLogger logger = new skedLogger(msg, level, tags);
		return logger.save();
	}

	// insert a generic log entry 
	public Logger__c save() { 
		if(message.length() > MAX_MESSAGE_LENGTH) message = message.substring(0, MAX_MESSAGE_LENGTH - 1);
		Logger__c log = new Logger__c(Level__c = level, Message__c = message, Tags__c = tags);
		insert log;
		return log;
	}

	// 
	public void appendNewLine(String msg) { 
		this.message += ' \n\n ' + msg;
	}

	public void appendFirst(String msg) { 
		this.message = msg + ' \n ' + this.message;
	}
}