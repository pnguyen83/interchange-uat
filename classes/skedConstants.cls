public class skedConstants {
	public static final String REGION_WESTERN_AUSTRALIA = 'Western Australia';
    public static final String STATUS_CURRENT = 'Current';
    public static final String TIMESHEET_STATUS_DRAFT = 'Draft';
    public static final String TIMESHEET_STATUS_PENDING_APPROVAL = 'Pending Approval';
    public static final String TIMESHEET_STATUS_APPROVED = 'Approved';
    public static final String TIMESHEET_STATUS_REJECTED = 'Rejected';
    public static final String JOB_ALLOCATION_DECLINED = 'Declined';
    public static final String JOB_ALLOCATION_DELETED = 'Deleted';
    public static final String JOB_ALLOCATION_MODIFIED = 'Modified';

    public static final string AVAILABILITY_STATUS_APPROVED = 'Approved';
    public static final string AVAILABILITY_STATUS_PENDING = 'Pending';
    public static final string AVAILABILITY_STATUS_DECLINED = 'Declined';

    public static final string JOB_STATUS_CANCELLED = 'Cancelled';
    public static final string JOB_STATUS_COMPLETE = 'Complete';
    public static final string JOB_STATUS_DISPATCHED = 'Dispatched';
    public static final string JOB_STATUS_IN_PROGRESS = 'In Progress';
    public static final string JOB_STATUS_PENDING_ALLOCATION = 'Pending Allocation';
    public static final string JOB_STATUS_PENDING_DISPATCH = 'Pending Dispatch';
    public static final string JOB_STATUS_QUEUED = 'Queued';
    public static final string JOB_STATUS_READY = 'Ready';
    
    public static final string JOB_ALLOCATION_STATUS_COMPLETE = 'Complete';
    public static final string JOB_ALLOCATION_STATUS_CONFIRMED = 'Confirmed';
    public static final string JOB_ALLOCATION_STATUS_DECLINED = 'Declined';
    public static final string JOB_ALLOCATION_STATUS_DELETED = 'Deleted';
    public static final string JOB_ALLOCATION_STATUS_DISPATCHED = 'Dispatched';
    public static final string JOB_ALLOCATION_STATUS_PENDING_DISPATCH = 'Pending Dispatch';
    public static final string JOB_ALLOCATION_STATUS_IN_PROGRESS = 'In Progress';
    
    public static final string HOLIDAY_GLOBAL = 'global';

    public static final String CONTACT_CLIENT_RT = 'Client';
}