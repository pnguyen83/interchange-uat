public with sharing class skedCustomLookupController {
    
    public List<enrtcr__Support_Contract__c> serviceAgreements{get;set;} // search results
    public List<enrtcr__Support_Contract_Item__c> serviceAgreementItems{get;set;} // search results
    public List<enrtcr__Service__c> services{get;set;} // search results

    public string searchString{get;set;} // search keyword
    public string contactId{get;set;} 
    public string saId{get;set;} 
    public string objectType{get;set;}
    public string category{get;set;}
  
    public skedCustomLookupController() {
        // get the current search string
        searchString    = System.currentPageReference().getParameters().get('lksrch');
        contactId       = System.currentPageReference().getParameters().get('contactId');
        saId            = System.currentPageReference().getParameters().get('saId');
        objectType      = System.currentPageReference().getParameters().get('objectType');
        category        = System.currentPageReference().getParameters().get('category');

        runSearch(); 
    }
   
    // performs the keyword search
    public PageReference search() {
        runSearch();
        return null;
    }
  
    // prepare the query and issue the search command
    public void runSearch() {
        // TODO prepare query string for complex serarches & prevent injections
        try{
                if(objectType == 'SA')
                {
                    serviceAgreements = searchSA(searchString);
                }
                else if(objectType == 'SVC')
                {
                    services = searchSVC(searchString);
                }
                else serviceAgreementItems   = searchSAI(searchString);
            
        }catch(Exception ex){
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR,ex.getMessage());
            ApexPages.addMessage(msg);
        }
    }

    public List<enrtcr__Service__c> searchSVC(string search)
    {
        List<enrtcr__Service__c> results = new List<enrtcr__Service__c>();
        String newSearchString = searchString.removeEnd('*').removeStart('*').replace('*','%');
        newSearchString = ( String.isBlank(newSearchString)?'%%':('%' + newSearchString + '%'));

        system.debug(category);
        enrtcr.LookupParams params = new enrtcr.LookupParams();
        params.searchTerm = newSearchString;
        params.additionalCriteria = new Map<string, string>{'category' => category};
        Set<String> addedServiceIDs = new Set<String>();

        for(enrtcr.ListOption option : enrtcr.CustomRemoteActionController.lookupServices(params))
        {
            if(!addedServiceIDs.contains(option.Id)){
                results.add(new enrtcr__Service__c(Id=option.Id,Name=option.label));
                addedServiceIDs.add(option.Id);
            }
            
        }
        return results;
    }
    
    // run the search and return the records found. 
    public List<enrtcr__Support_Contract__c> searchSA(string searchString) {
        List<enrtcr__Support_Contract__c> result = new List<enrtcr__Support_Contract__c>();
        String newSearchString = searchString.removeEnd('*').removeStart('*').replace('*','%');
        newSearchString = ( String.isBlank(newSearchString)?'%%':('%' + newSearchString + '%'));
        
        for (enrtcr__Support_Contract__c  serviceAgreement : [SELECT Id, Name,RecordType.Name,enrtcr__Start_Date__c, enrtcr__End_Date__c,enrtcr__Status__c,enrtcr__Funding_Type_Display__c,
                                                                (SELECT Id, Name, enrtcr__Service__r.Name, enrtcr__Rate__r.Name FROM enrtcr__Support_Contract_Items__r),
                                                                (SELECT Id, enrtcr__Client__c FROM enrtcr__Funded_Clients__r)
                                                                FROM enrtcr__Support_Contract__c
                                                                WHERE RecordType.Name = 'Block Grant'
                                                                AND enrtcr__Start_Date__c <= :Date.today()
                                                                AND enrtcr__End_Date__c >= :Date.today()
                                                                AND Name like :newSearchString
                                                                AND enrtcr__Client__c = null
                                                                AND enrtcr__Status__c = :skedConstants.STATUS_CURRENT
                                                                AND (enrtcr__Remaining_Balance__c > 0 OR enrtcr__Mac_Funded__c = TRUE)])
        {
            Pattern regexPattern = Pattern.compile(searchString);
            Matcher regexMatcher = regexPattern.matcher(serviceAgreement.Name);
            system.debug(regexMatcher.find());
            
            //if(!String.isBlank(searchString)) continue;
            if (serviceAgreement.enrtcr__Funded_Clients__r.size()==0){
                result.add(serviceAgreement);
            }  else if (serviceAgreement.enrtcr__Funded_Clients__r.size() > 0){
                for (enrtcr__Funded_Support_Contract_Client__c fc : serviceAgreement.enrtcr__Funded_Clients__r){
                    if (fc.enrtcr__Client__c == (Id)contactId){
                        result.add(serviceAgreement); break; 
                    }
                }
            }          
        }
        return result;
    }
    
    // run the search and return the records found. 
    public List<enrtcr__Support_Contract_Item__c> searchSAI(string searchString) {
        String newSearchString = searchString.removeEnd('*').removeStart('*').replace('*','%');
        newSearchString = ( String.isBlank(newSearchString)?'%%':('%' + newSearchString + '%'));
        
        List<enrtcr__Support_Contract_Item__c> result = new List<enrtcr__Support_Contract_Item__c>();
        
        if( !String.isBlank(saId) ){
            return [Select Id, Name, enrtcr__Service__r.Name, enrtcr__Rate_Type__c, enrtcr__Service_Type__c, enrtcr__Site__r.Name,
                    enrtcr__Rate__r.Name, enrtcr__Quantity__c, enrtcr__Remaining__c, enrtcr__Total__c, enrtcr__Delivered__c
                    from enrtcr__Support_Contract_Item__c where enrtcr__Support_Contract__c  =:saId
                    and Name like :newSearchString
                    AND (enrtcr__Support_Contract__r.enrtcr__Mac_Funded__c = TRUE OR (enrtcr__Support_Contract__r.enrtcr__Remaining_Balance__c > 0 AND enrtcr__Remaining__c > 0))
                    AND enrtcr__Support_Contract__r.enrtcr__Status__c = :skedConstants.STATUS_CURRENT ];
                        //AND (enrtcr__Support_Contract__r.enrtcr__Remaining_Balance__c > 0 OR enrtcr__Support_Contract__r.enrtcr__Mac_Funded__c = TRUE)];
        }else if( !String.isBlank(contactId) ){
            return [Select Id, Name, enrtcr__Service__r.Name, enrtcr__Rate_Type__c, enrtcr__Service_Type__c, enrtcr__Site__r.Name,
                    enrtcr__Rate__r.Name, enrtcr__Quantity__c, enrtcr__Remaining__c, enrtcr__Total__c, enrtcr__Delivered__c
                    from enrtcr__Support_Contract_Item__c where enrtcr__Support_Contract__r.enrtcr__Client__c =:contactId
                    and Name like :newSearchString
                    AND enrtcr__Remaining__c > 0
                    AND enrtcr__Support_Contract__r.enrtcr__Status__c = :skedConstants.STATUS_CURRENT
                        AND (enrtcr__Support_Contract__r.enrtcr__Remaining_Balance__c > 0 OR enrtcr__Support_Contract__r.enrtcr__Mac_Funded__c = TRUE)];
        }
        
        return result;
    }
    
    // used by the visualforce page to send the link to the right dom element
    public string getFormTag() {
        return System.currentPageReference().getParameters().get('frm');
    }
    
    // used by the visualforce page to send the link to the right dom element for the text box
    public string getTextBox() {
        return System.currentPageReference().getParameters().get('txt');
    }
 
}