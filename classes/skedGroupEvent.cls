public class skedGroupEvent {
	public string groupName {get;set;}
	public skedOption groupStatus {get;set;} // Group_Status__c 
	public skedOption facilitator {get;set;} // Facilitator__c
    public string facilitatorName {get;set;}
	public string description {get;set;}
	public skedOption serviceItem {get;set;} // Service_Item__c
	public string serviceItemName {get;set;}
	public string regionName {get;set;}
	public skedOption region {get;set;} // Service_Item__c
	public skedAddressOption location {get;set;} // 
	public string address {get;set;}
	public string alternativeAddress {get;set;}
	public string groupId {get;set;}
	public skedAddressOption alternativeLocation {get;set;} // Attendace_Fee__c
	public skedOption account {get;set;} // Facilitator__c 
	public skedOption site {get;set;} // Location or Site, key is location/site address 
	public string accountName {get;set;}
    public string startDate {get;set;}
    public string startTime {get;set;}
    public Decimal duration {get;set;}
    public string urgency {get;set;}
    public string jobType {get;set;}

    public boolean is_recurring{get;set;}
	public List<string> patternWeeks{get;set;}
	public boolean skipWeekends{get;set;}
	public boolean skipHolidays{get;set;}
	public integer recurWeeks{get;set;}
    public boolean confirmJob {get;set;}
	public string pattern{get;set;}

	public string selectedRepeat{get;set;}
	public integer every{get;set;}
	public string selectedEnding{get;set;}
	public string endingOn{get;set;}
	public integer numberOfSessions{get;set;}
	public integer selectedNthDay{get;set;}
	public string selectedWeekdays{get;set;}

    public boolean isSavingGroup {get;set;}
    public boolean saveTagToFutureJob {get;set;}
	public list<skedGroupClient> listGroupClients {get;set;}
	public list<skedAppointmentDetail> appointments {get;set;}
	public list<skedTag> tags {get;set;}
	public String altAddressLookup;

	public decimal numberOfResourcesRequired {get;set;}
	public decimal maximumNumberOfParticipants {get;set;}

	public boolean showNumberOfResourcesRequired {get;set;}
	public boolean showMaximumNumberOfParticipants {get;set;}

	public skedGroupEvent(){
		this.groupName = '';
		is_recurring = false;
		this.groupStatus = new skedOption('Active','Active');
		this.facilitator = new skedOption();
        this.facilitatorName = '';
        this.startDate = Datetime.now().format('dd/MM/YYYY');
        this.account = new skedOption();
        this.site = new skedOption();
        this.accountName = '';
        this.alternativeAddress = '';
		this.description = '';
		this.serviceItem = new skedOption();
		this.serviceItemName = '';
		this.regionName = '';
		this.address = '';
		this.alternativeAddress = '';
		this.altAddressLookup = 'Account';
		this.region = new skedOption('none','---Select---');
		this.confirmJob = true;
		this.numberOfResourcesRequired = 1;
		this.maximumNumberOfParticipants = 1;
		//this.attendanceFee = 0.0;
		listGroupClients = new list<skedGroupClient>();
		appointments = new list<skedAppointmentDetail> ();
		tags = new list<skedTag>();
		
		this.showNumberOfResourcesRequired = skedConfigs.SHOW_REQUIRED_RESOURCES;
		this.showMaximumNumberOfParticipants = skedConfigs.SHOW_MAX_PARTICIPANT;
	}

	public skedGroupEvent(string groupID){
		Group_Event__c GE = [select id,Coordinator__c,Account__c, Account__r.Name,Coordinator__r.Name,Coordinator__r.AccountID,Alternative_Address__c,GeoLocation__latitude__s,
								GeoLocation__longitude__s, Name, Address__c,Description__c,Group_Status__c,Region__c,Region__r.Name,Service__c,Service__r.Name,
								Service__r.enrtcr__Service_Type__c ,Account__r.ShippingStreet,Account__r.ShippingCity,Account__r.ShippingState,Account__r.ShippingPostalCode, 
								Account__r.ShippingCountry, Location__c, Location__r.Name, Location__r.sked__Address__c, Site__c, Site__r.Name, Site__r.Address__c,
								Maximum_Number_of_Participants__c, Number_of_Resources_Required__c,
						         (select id, service__c, Delivery_Method__c, Quantity__c, service__r.name, service_agreement_item__c, service_agreement_item__r.name, service_agreement_item__r.enrtcr__Support_Contract__r.enrtcr__Status__c, rate__c, rate__r.name, rate__r.enrtcr__ratetype__r.name, site__c, site__r.name, program__c, program__r.name, client__c, client__r.Name, client__r.Birthdate, client__r.MobilePhone from Group_Clients__r),
								 (select id, name,sked__Start__c, sked__Duration__c, sked__Region__c, sked__Region__r.sked__Timezone__c, sked__Job_Status__c, No_of_Attendees__c from Jobs__r order by sked__Start__c),
								 (select id, Group_Event__c, Tag__c, Tag__r.Name, Required__c, Weighting__c from Group_Tags__r)
							 from Group_Event__c where id=: groupID];

		this.groupName = Ge.Name;
		this.groupId = GE.Id;
		this.groupStatus = new skedOption(Ge.Group_Status__c,Ge.Group_Status__c);
		this.facilitator = new skedOption(Ge.Coordinator__c,Ge.Coordinator__r.Name,Ge.Coordinator__r.AccountID);
        this.facilitatorName = Ge.Coordinator__r.Name;
        this.site 			= new skedOption();
        this.account 		= new skedOption();
        this.altAddressLookup = 'Account';
        this.confirmJob = true;
        this.is_recurring = false;
        
        if(Ge.Account__c != null) {
        	this.account = new skedOption(Ge.Account__c,Ge.Account__r.Name, skedGroupEventExt.createAddressStringAcc(GE.Account__r));
        	this.accountName = Ge.Account__r.Name;
        	this.alternativeAddress = this.account.key;
        }else{
        	this.altAddressLookup = skedConfigs.LOCATION_OR_SITE_FILTER;
        	if(this.altAddressLookup == 'Location' && Ge.Location__c != null){
        		this.site  = new skedOption(Ge.Location__c, Ge.Location__r.Name, Ge.Location__r.sked__Address__c);
        	}else if(this.altAddressLookup == 'Site' && Ge.Site__c != null){
        		this.site  = new skedOption(Ge.Site__c, Ge.Site__r.Name, Ge.Site__r.Address__c);
        	}
        	this.alternativeAddress = this.site.key;
        }

		this.description = Ge.Description__c;
		this.serviceItem = new skedOption(Ge.Service__c,Ge.Service__r.Name + (String.isBlank(Ge.Service__r.enrtcr__Service_Type__c)?'':(' - ' +Ge.Service__r.enrtcr__Service_Type__c)));
		this.serviceItemName = Ge.Service__r.Name + (String.isBlank(Ge.Service__r.enrtcr__Service_Type__c)?'':(' - ' +Ge.Service__r.enrtcr__Service_Type__c));
		this.regionName = Ge.Region__r.Name;
		this.region = new skedOption(Ge.Region__c,Ge.Region__r.Name);
		//this.attendanceFee = GE.Attendance_Fee__c;	
		this.address= '';

		this.location = new skedAddressOption();
		this.location.address = GE.Address__c;
		this.address =  GE.Address__c;
		this.location.value = GE.Address__c;
		this.location.label = GE.Address__c;
		this.location.lng   = GE.GeoLocation__longitude__s;
		this.location.lat   = GE.GeoLocation__latitude__s;

		this.numberOfResourcesRequired = GE.Number_of_Resources_Required__c;
		this.maximumNumberOfParticipants = GE.Maximum_Number_of_Participants__c;

		this.showNumberOfResourcesRequired = skedConfigs.SHOW_REQUIRED_RESOURCES;
		this.showMaximumNumberOfParticipants = skedConfigs.SHOW_MAX_PARTICIPANT;

		listGroupClients = new list<skedGroupClient>();
		tags = new list<skedTag>();

        Set<Id> serviceAgreementItemIds = new Set<Id>();
        set<id> setContactIds = new set<id>();

        for(Group_Client__c GC1: GE.Group_Clients__r)
        {
        	setContactIds.add(GC1.client__c);
            if(GC1.Service_Agreement_Item__c != null)
                serviceAgreementItemIds.add(GC1.Service_Agreement_Item__c);
        }

        map<id, Contact> mapId_Contact = new map<id,contact>([select id,(select id, Blacklist_Client__c from Client_Blacklist__r), (select id, Client__c, Tag__c, Tag__r.Name, Required__c, Weighting__c from Client_Tags__r)  from contact where id in :setContactIds]);

        Map<Id,enrtcr__Support_Contract_Item__c> serviceAgreementItemsMap = new Map<Id, enrtcr__Support_Contract_Item__c>(enrtcr.ServiceAgreementItems.getServiceAgreementItems(serviceAgreementItemIds));

		for(Group_Client__c GC1: GE.Group_Clients__r){
			skedGroupClient GC = new skedGroupClient();

            GC.id = GC1.client__c;
            Contact con = mapId_Contact.get(GC1.client__c);

			GC.groupId = GE.id;
            GC.name = GC1.client__r.Name;
            system.debug(GC.name);
            GC.label = GC1.client__r.Name;
            GC.contactNumber = GC1.client__r.MobilePhone;
			GC.site = (GC1.Site__c != null)?new enrtcr.ListOption(GC1.Site__c, GC1.Site__r.Name):null;
			GC.program = (GC1.Program__c != null)?new enrtcr.ListOption(GC1.Program__c, GC1.Program__r.Name):null;
            GC.rate = (GC1.Rate__c != null)?new enrtcr.ListOption(GC1.Rate__c, GC1.Rate__r.enrtcr__RateType__r.Name + ' - ' + GC1.Rate__r.Name):null;
			GC.service = (GC1.Service__c != null)?new enrtcr.ListOption(GC1.Service__c, GC1.Service__r.Name):null;
            GC.serviceAgreementItem = GC1.Service_Agreement_Item__c;
			GC.quantity = GC1.Quantity__c;
			GC.deliveryMethod = GC1.Delivery_Method__c;

			GC.blacklist = new List<string>();
			Set<String> stBlacklist = new Set<String>();
            for(Client_Blacklist__c CB: con.Client_Blacklist__r){
            	stBlacklist.add(CB.Blacklist_Client__c);
                //GC.blacklist.add(CB.Blacklist_Client__c);
            }
            GC.blacklist.addAll(stBlacklist);

            GC.tags = new List<skedTag>();
            for ( Client_Tag__c clientTag : con.Client_Tags__r ) {
            	skedTag tag = new skedTag(clientTag);
            	GC.tags.add(tag);
            }

            if(GC1.Service_Agreement_Item__c != null)
            {
                enrtcr__Support_Contract_Item__c serviceAgreementItem = serviceAgreementItemsMap.get(GC1.Service_Agreement_Item__c);

				if(GC.service != null)
				{
					if (GC.site != null) {
						GC.serviceAgreementItemName = GC.site.label + ' - ';
					}

					GC.serviceAgreementItemName += GC.service.label;

					if (GC.program != null) {
						GC.serviceAgreementItemName += ' - ' + GC.program.label;
					}

					if (GC.rate != null) {
						GC.serviceAgreementItemName += ' - ' + GC.rate.label;
					}
				}
				else
				{
					GC.serviceAgreementItemName = enrtcr.ServiceAgreementItems.getServiceName(serviceAgreementItem, true, false);
				}

                GC.startDate = ((Datetime)serviceAgreementItem.enrtcr__Support_Contract__r.enrtcr__Start_Date__c).format('dd/MM/YYYY');
                GC.endDate = ((Datetime)serviceAgreementItem.enrtcr__Support_Contract__r.enrtcr__End_Date__c).format('dd/MM/YYYY');
                GC.status = GC1.service_agreement_item__r.enrtcr__Support_Contract__r.enrtcr__Status__c;
            }

            if(GC1.client__r.Birthdate != null) {
            	Datetime bd = Date.newInstance(GC1.client__r.Birthdate.year(), GC1.client__r.Birthdate.month(), GC1.client__r.Birthdate.day());
            	GC.birthDate = bd.format('dd/MM/yyyy');
            }
            
            listGroupClients.add(GC);

		}

		appointments = new list<skedAppointmentDetail> ();
		system.debug(GE.Jobs__r);
		for(sked__job__c job: GE.Jobs__r){
			appointments.add(new skedAppointmentDetail(job));

		}

		for ( Group_Tag__c gTag : GE.Group_Tags__r ) {
			skedTag tag = new skedTag(gTag);
			tags.add(tag);
		}

		system.debug(appointments);

	}

}