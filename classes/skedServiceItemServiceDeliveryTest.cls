@isTest
public class skedServiceItemServiceDeliveryTest {

    @testSetup static void createSettings()
    {
        enrtcr__Settings__c settings = enrtcr__Settings__c.getOrgDefaults();

        if (settings.Id == null)
        {
            settings.enrtcr__Package_Namespace__c = 'enrtcr__';
            settings.enrtcr__Notification_Enabled__c = true;
            settings.enrtcr__Claimable_Travel_Time_Km_Threshold__c = 10;
            insert settings;
        }
    }

    static testMethod void doTestServiceItemCreation() {
        //ACCOUNT
        account acc = newAccount();
        //insert acc;
        //REGION
        sked__region__c region = newRegion();
        insert region;
        //Create Contact
        contact con = newContact();
        //insert con;
        //Create Note
        enrtcr__Note__c n = newNote(con);
        //insert n;
        sked__resource__c resource = newResource(newUser());
        resource.sked__Primary_Region__c = region.Id;
        //insert resource;
        List<SObject> lstObjects = new List<SObject>{acc, con, n, resource};
            insert lstObjects;
        //JOB
        sked__job__c job = newJob(region,acc);
        job.sked__Start__c = dateTime.newInstance(2016, 01, 20, 09, 00, 00);
        job.sked__Finish__c = dateTime.newInstance(2016, 01, 20, 10, 00, 00);
        job.sked__duration__c = 60;     
        job.sked__contact__c = con.id;


        
        insert job;
        //UPDATE JOB STATUS
        job.sked__job_status__c = 'Complete';
        update job;
        //CREATE RESOURCE
        
        //create Job Allocation
        sked__job_allocation__c ja = newJobAllocation(resource,job);
        //insert ja;
        //CreateService Item
        //Job_Service_Item__c si = newServiceItem(job);
        //insert si;
        List<SObject> lstObjects2 = new List<SObject>{ja};
            insert lstObjects2;
        //update Service Item to update Service Delivered Related Record
       // si.Quantity__c = 2;
        //update si;
       // delete si;
    }

    static testMethod void doTestSkedUtils(){
        skedUtils.GetDifferentDays('2017-01-01','2017-02-01');
        skedUtils.GetStartOfDate('2017-01-01','Australia/Sydney');
        skedUtils.GetStartOfDate(datetime.now(),'Australia/Sydney');
        skedUtils.GetEndOfDate(datetime.now(),'Australia/Sydney');
        skedUtils.ConvertDateToIsoString(date.today());
        skedUtils.convertToTimezone(datetime.now(),'Australia/Sydney');
        skedUtils.addDays(datetime.now(),5,'Australia/Sydney');
        skedUtils.daySequenceOfWeek('Mon');
    }
    
    static testMethod void doTestForHistoricalCaseNotes() {
        //ACCOUNT
        account acc = newAccount();
        insert acc;
        //REGION
        sked__region__c region = newRegion();
        insert region;
        //Create Contact
        contact con = newContact();
        insert con;
        //Create Note
        enrtcr__Note__c n = newNote(con);
        insert n;


        //JOB
        sked__job__c job = newJob(region,acc);
        job.sked__Start__c = dateTime.newInstance(2016, 01, 20, 09, 00, 00);
        job.sked__Finish__c = dateTime.newInstance(2016, 01, 20, 10, 00, 00);
        job.sked__duration__c = 60; 
        job.sked__contact__c = con.Id;
        insert job;
        //UPDATE JOB
        Test.startTest();

        job.sked__contact__c = con.id; 
        update job;

        //Create another case note
        enrtcr__Note__c n2 = newNote(con);
        insert n2;

        Test.stopTest();
    }

//    static testMethod  void testJobServiceItem(){
//        enrtcr__Support_Delivered__c tmpSerivceDelivery = skedDataFactory.createServiceDelivered(skedDataFactory.createEnriteCareReferenceData());
//
//
//        account acc = newAccount();
//        insert acc;
//        //REGION
//        sked__region__c region = newRegion();
//        insert region;
//        //Create Contact
//        contact con = newContact();
//        insert con;
//        //Create Note
//        enrtcr__Note__c n = newNote(con);
//        insert n;
//
//
//
//        //JOB
//        sked__job__c job = newJob(region,acc);
//        job.sked__Start__c = dateTime.newInstance(tmpSerivceDelivery.enrtcr__Date__c, Datetime.now().time());
//        job.sked__Finish__c = dateTime.newInstance(tmpSerivceDelivery.enrtcr__Date__c, Datetime.now().time().addHours(1));
//        job.sked__duration__c = 60;
//        job.Service_Agreement__c = tmpSerivceDelivery.enrtcr__Support_Contract__c;
//        job.Service_Agreement_Item__c = tmpSerivceDelivery.enrtcr__Support_Contract_Item__c;
//        job.sked__Actual_End__c = date.today();
//        insert job;
//
//        test.startTest();
//
//        Job_Service_Item__c si = newServiceItem(job);
//        si.Service_Agreement_Item__c = tmpSerivceDelivery.enrtcr__Support_Contract_Item__c;
//
//        insert si;
//
//
//        job.sked__job_status__c = 'Complete';
//
//
//        update job;
//
//        Group_Attendee__c groupAttendee = new Group_Attendee__c();
//        groupAttendee.Contact__c = tmpSerivceDelivery.enrtcr__Client__c;
//        groupAttendee.Attended__c = true;
//        groupAttendee.Job__c = job.Id;
//
//        insert groupAttendee;
//
//        si.Group_Attendee__c = groupAttendee.Id;
//        update si;
//
//        update job;
//
//        delete si;
//
//        test.stopTest();
//
//    }
    
    public static user newUser(){
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        User u = new User(Alias = 'admin', Email='VA_Admin@vision.com', 
            EmailEncodingKey='UTF-8', LastName='Admin', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='VA_Admin@vision.com');
        
        return u;
    }
    
    public static account newAccount(){
        account acc = new account();
        acc.name = 'Skedulo Account';
        
        
        return acc;
    }
    
    public static sked__region__c newRegion(){
        sked__region__c region = new sked__region__c();
        region.name = 'test region';
        region.sked__timezone__c = 'Australia/Brisbane';
        
        return region;
    }
    
    public static sked__resource__c newResource(user u){
        
        sked__resource__c resource = new sked__resource__c();
        resource.name = 'Skedulo Resource';
        resource.sked__User__c = u.id;
        return resource;
        
    }
    
    public static sked__job_allocation__c newJobAllocation(sked__resource__c resource, sked__job__c job){
        
        sked__job_allocation__c jobAllocation = new sked__job_allocation__c();
        jobAllocation.sked__resource__c = resource.id;
        jobAllocation.sked__job__c = job.id;
        return jobAllocation;
    }
    
    public static sked__job__c newJob(sked__region__c region,account acc){
        sked__job__c job = new sked__job__c();
        job.sked__region__c = region.id;
        job.sked__account__c = acc.id;
        job.sked__Address__c = '79 mclachlan';
        
        return job;
    }
    
    public static Job_Service_Item__c newServiceItem(sked__job__c job){

        Job_Service_Item__c si = new Job_Service_Item__c();
        si.Comment__c = 'Skedulo Test';
        si.job__c = job.id;
        si.Quantity__c = 1;
        si.Travel_Kms__c = 10;
        si.UOM__c = 'hours';
        
        return si;
    }
    
    public static contact newContact(){
        contact con = new contact();
        con.FirstName = 'Skedulo';
        con.LastName = 'Test';
        con.enrtcr__Status__c = 'Current';
        con.Birthdate = date.newInstance(1987, 01, 20);
        con.enrtcr__Date_Client_Registered__c = date.newInstance(2016, 01, 20);
        con.OtherStreet = '79 Mclachlan Street';
        con.OtherCity = 'Fortitude Valley';
        con.OtherState = 'QLD';
        con.OtherPostalCode = '4006';
        con.enrtcr__Primary_Disability__c = 'Anxiety';
        con.enrtcr__Preferred_Communication_Method__c = 'SMS';
        con.Phone = '0420524911';
        con.enrtcr__Client_Region__c = 'Eastern';
        con.MobilePhone = '0420524911';
        con.enrtcr__Primary_Referral_Source__c  = 'Internet - EFA Website';
        
        return con;
    }
    
    public static enrtcr__Note__c newNote(contact con){
        enrtcr__Note__c note = new enrtcr__Note__c();
        note.enrtcr__Client__c = con.id;
        note.name = 'Skedulo Note';
        note.enrtcr__Description__c = 'Test Note';
        note.enrtcr__Type__c = 'Email';
                
        return note;
    }

}