global class skedHistoricalCaseNoteUpdateBatch implements Database.Batchable<sObject> {
	
	Set<Id> jobIds;
	
	global skedHistoricalCaseNoteUpdateBatch(Set<Id> jobIds) {
		this.jobIds = jobIds;
	}
	
	global list<sked__Job__c> start(Database.BatchableContext BC) {
		return [Select Id, Name from sked__Job__c where Id IN :jobIds];
	}

   	global void execute(Database.BatchableContext BC, List<sked__Job__c> jobs) {
		new skedTriggerHandler().updateHistoricalCaseNotesOnInsert(jobs);
	}
	
	global void finish(Database.BatchableContext BC) {
		
	}
	
}