public class skedJsonAvailableServiceUtil {
	public static string getServiceItems(string clientID,string startDate, string sid ){

        ServiceDeliveryAPI__c SDAPI = ServiceDeliveryAPI__c.getOrgDefaults();
        if(SDAPI.secret__c == null && Test.isRunningTest()){
            SDAPI = new ServiceDeliveryAPI__c(secret__c = '1234567', ApiName__c='abc');
        }
        
        //string addr =SDAPI.site__c;
        string addr = 'callout:'+SDAPI.ApiName__c+'/'+'services/apexrest/enrtcr/ServiceDelivery/ClientServiceOptions?clientId='+clientID+'&deliveryDate='+startDate+'&validStatuses[]=Current';
        //
        //
        system.debug('addr '+ addr);

        HttpRequest req = new HttpRequest();
        req.setEndpoint( addr );
        req.setMethod('GET');
        //req.setHeader('Authorization', 'OAuth '+sid);
        req.setHeader('Content-Type','application/json');
        System.assertNotEquals(null, SDAPI);
        System.assertNotEquals(null, SDAPI.secret__c);
        req.setHeader('secret',SDAPI.secret__c);


        Http http = new Http();
        HttpResponse res = http.send(req);
        integer responseCode = 0;
        string jsonResponse = ''; 
        responseCode = res.getStatusCode(); 
        jsonResponse = res.getBody();
        //system.debug(jsonResponse);
        //string retString = (String)JSON.deserialize(jsonResponse,String.Class);
        //system.debug(retString);
        return jsonResponse;

    }

    //Run Create Attendee Attachments when Job is dispatch
    public static void createAttachmentOnAttendeeWhenJobDispatch(list<sked__Job__c> jobs, map<id,sked__job__c> oldMap){
        list<Id> jobIds = new list<id>();
        list<id> attendeeIds = new list<id>();
        //if(oldMap == null){
        //    for(sked__Job__c job: jobs){
        //        if(job.sked__Job_Status__c == SkeduloConstants.JOB_STATUS_DISPATCHED){
        //            jobIds.add(job.id);
        //        }
        //    }
        //}else{
        for(sked__Job__c job: jobs){
            if(job.sked__Job_Status__c == skedConfigs.JSON_GEN_STATUS && job.sked__Job_Status__c != oldMap.get(job.id).sked__Job_Status__c){
                jobIds.add(job.id);
            }
        }
        //}

        for(Group_Attendee__c gatt: [select id from Group_Attendee__c where Job__c in: jobIds]){
            attendeeIds.add(gatt.id);
        }

        if(attendeeids.size() > 0){
            skedAttendeeAvailableServiceBatch b = new skedAttendeeAvailableServiceBatch(attendeeIds,UserInfo.getSessionId());
            Database.executeBatch(b,5);
        }
    }

    //For Single 
    public static void createAttachmentOnJobWhenDispatched(list<sked__Job__c> jobs, map<id,sked__job__c> oldMap){
        list<Id> jobIds = new list<id>();
        for(sked__Job__c job: jobs){
            if(job.sked__Job_Status__c == skedConfigs.JSON_GEN_STATUS && job.sked__Job_Status__c != oldMap.get(job.id).sked__Job_Status__c && job.Group_Event__c == null){
                jobIds.add(job.id);
            }
        }
        if(jobIds.size() > 0){
            skedSingleJobAvailableserviceBatch b = new skedSingleJobAvailableserviceBatch(jobIds,UserInfo.getSessionId());
            Database.executeBatch(b,5);
        }
    }
}