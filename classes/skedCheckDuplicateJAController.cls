public class skedCheckDuplicateJAController {
	public List<Allocation> lstDuplicates {get; set;}
	public List<SelectOption> lstSize {get; set;}
	public List<SelectOption> lstfilterOptions {get; set;}
	public String filter {get; set;}
	public String startDate {get; set;}
	public String endDate {get; set;}
	public String size {get; set;}
	public boolean displayDate {get; set;}
	private integer noEachPage {
		get {
			if (String.isNotBlank(this.size)) {
				return Integer.valueOf(this.size);
			}
			return 5;
		}	 
		set;
	}
	private integer totalDuplicated = 0;
	private integer prvPositon;
	private integer nxtPosition;
	private integer offset;
			
	public skedCheckDuplicateJAController() {
		this.displayDate = false;
		this.size = '12';
		this.offset = 0;
		this.prvPositon = 0;
		this.nxtPosition = this.noEachPage; 
		this.lstDuplicates = new List<Allocation>();
		this.startDate = '';
		this.endDate = '';

		this.lstSize = new List<SelectOption>();
		for (Integer i = 0; i <= 7; i++) {
			String j = String.valueOf(i + 5);
			SelectOption s = new SelectOption(j, j);
			this.lstSize.add(s);
		}

		this.lstfilterOptions = new List<SelectOption>();
		this.lstfilterOptions.add(new SelectOption('All Jobs','All Jobs'));
		this.lstfilterOptions.add(new SelectOption('Future Jobs','Future Jobs'));
		this.lstfilterOptions.add(new SelectOption('Past Jobs','Past Jobs'));
		this.lstfilterOptions.add(new SelectOption('Time Period','Time Period'));

		getDuplicated();
	}

	public void changeFilter() {
		this.startDate = '';
		this.endDate = '';
		if (this.filter.equals('Time Period')) {
			this.displayDate = true;
		}
		else {
			this.displayDate = false;
		}
	}

	public void getDuplicated() {
		String totalQuery = createTotalQuery();
		this.totalDuplicated = Database.countQuery(totalQuery);

		this.lstDuplicates.clear();
		if (this.offset < 0) this.offset = 0;
		Map<String, List<Allocation>> mapResourceAllocation = new Map<String, List<Allocation>>();
		List<sked__Job_Allocation__c> lstJAs = new List<sked__Job_Allocation__c>();
		String query = createQuery();
		lstJAs = Database.query(query);
	
		for (sked__Job_Allocation__c ja : lstJAs) {
			Allocation a = new Allocation(ja);
			//this.lstDuplicates.add(a);
			//String jobDate = ja.sked__Job__r.sked__start__c.format('yyyy-MM-dd');
			String resName = ja.sked__Resource__r.Name;
			List<Allocation> lstAll = mapResourceAllocation.get(resName);
			if (lstAll == null) {
				lstAll = new List<Allocation>();
			}
			lstAll.add(a);
			//lstAll.sort();
			mapResourceAllocation.put(resName, lstAll);
		}

		if (!mapResourceAllocation.isEmpty()) {
			for (List<Allocation> lstA : mapResourceAllocation.values()) {
				//lstA.sort();
				this.lstDuplicates.addAll(lstA);
			}
		}
	}

	public void changeSize() {
		getDuplicated();
	}

	public void previous()
	{
		this.offset = (this.offset - this.noEachPage) > 0 ? (this.offset - this.noEachPage) : 0;
		getDuplicated();
	}

	public void next()
	{
		this.offset = (this.offset + this.noEachPage) <= this.totalDuplicated ? (this.offset + this.noEachPage) : (this.totalDuplicated - this.noEachPage);
		getDuplicated();
	}

	public boolean getprev()
	{
		if(this.offset - this.noEachPage >= 0){
			return false;
		}
		else {
			return true;
		}
	}

	public boolean getnxt()
	{
		if (this.offset + this.noEachPage < this.totalDuplicated) {
			return false;
		}
		else {
			return true;
		}
	}

	private string createTotalQuery() {
		String totalQuery = 'SELECT count() ';
		totalQuery = addFilterToTquery(totalQuery);

		return totalQuery;
	}

	private string createQuery() {
		String query = 'SELECT id, name, sked__Job__c, sked__Job__r.name, sked__resource__c,';
		query += 'sked__resource__r.name, sked__job__r.sked__start__c,';
		query += 'sked__job__r.sked__finish__c, sked__Status__c';
		query = addFilterToTquery(query);
		query += 'ORDER BY sked__resource__r.name,sked__Job__r.sked__Start__c LIMIT ' + noEachPage + ' OFFSET ' + offset;

		return query;
	}

	private string addFilterToTquery(String query) {
		query += ' FROM sked__Job_Allocation__c WHERE sked_isDuplicated__c = true';
		query += ' AND sked__Status__c != \'' + skedConstants.JOB_ALLOCATION_STATUS_COMPLETE + '\'';
		query += ' AND sked__Status__c != \'' + skedConstants.JOB_ALLOCATION_STATUS_DECLINED + '\'';
		query += ' AND sked__Status__c != \'' + skedConstants.JOB_ALLOCATION_STATUS_DELETED + '\'';
		
		if (String.isNotBlank(this.filter) && this.filter != 'All Jobs') {
			DateTime today = DateTime.newInstance(Date.valueOf(System.today()), Time.newInstance(0, 0, 0, 0));
			String strToday = today.format('yyyy-MM-dd');

			if (this.filter.equals('Future Jobs')) {
				String filter = getTimeFormatForQuery(strToday);
				query += ' AND sked__Job__r.sked__Start__c >= ' + filter;
			}
			else if (this.filter.equals('Past Jobs')) {
				String filter = getTimeFormatForQuery(strToday);
				query += ' AND sked__Job__r.sked__Start__c < ' + filter;
			}
			else if (this.filter.equals('Time Period')) {
				if (String.isNotBlank(this.startDate)) {
					String filter = getTimeFormatForQuery(this.startDate);
					query += ' AND sked__Job__r.sked__Start__c >= ' + filter;
				}

				if (String.isNotBlank(this.endDate)) {
					if (String.isNotBlank(this.startDate) && this.startDate == this.endDate) {
						Date tempDate = Date.valueOf(this.startDate).addDays(1);
						this.endDate = DateTime.newInstance(tempDate, Time.newInstance(0, 0, 0, 0)).format('yyyy-MM-dd');
					}
					else {
						Date tempDate = Date.valueOf(this.endDate).addDays(1);
						this.endDate = DateTime.newInstance(tempDate, Time.newInstance(0, 0, 0, 0)).format('yyyy-MM-dd');	
					}
					String filter = getTimeFormatForQuery(this.endDate);
					query += ' AND sked__Job__r.sked__Start__c <= ' + filter;
				}
			}	
		}
		
		System.debug('query = ' + query);
		return query;
	}

	private String getTimeFormatForQuery(String strDate) {
		String result = '';

		result = strDate + 'T' + '00:00:00Z';

		return result;
	}

	public class Allocation implements Comparable{
		public String id {get; set;}
		public String name {get; set;}
		public String status {get; set;}
		public String jobId {get; set;}
		public String jobName {get; set;}
		public String jobStart {get; set;}
		public DateTime start;
		public String jobFinish {get; set;}
		public String resId {get; set;}
		public String resName {get; set;}

		public Allocation(sked__Job_Allocation__c ja) {
			String timeFormat = 'MM/dd/yyyy hh:mm a';
			this.id = ja.id;
			this.name = ja.name;
			this.status = ja.sked__Status__c;
			this.jobId = ja.sked__Job__c;
			this.jobName = ja.sked__Job__r.name;
			this.resId = ja.sked__Resource__c;
			this.resName = ja.sked__Resource__r.name;
			this.jobStart = ja.sked__job__r.sked__start__c.format(timeFormat);
			this.jobFinish = ja.sked__job__r.sked__finish__c.format(timeFormat);
			this.start = ja.sked__Job__r.sked__Start__c;
		}

		public Integer compareTo(Object compareTo) {
		    Allocation ja = (Allocation)compareTo;
		    if (this.start == ja.start) return 0;
		    if (this.start > ja.start) return 1;
		    return -1;    
		}
	}
}