@isTest
private class skedRACController_Test {
    @isTest static void getConfigData() {
        skedRACDataSetup dataSetup = new skedRACDataSetup();
        Map<string, sObject> mapTestData = dataSetup.setupRACTestData();
        skedRACController.getConfigData();
    }
    
    @isTest static void getAvailabilityList() {
        skedRACDataSetup dataSetup = new skedRACDataSetup();
        Map<string, sObject> mapTestData = dataSetup.setupRACTestData();
        sked__Region__c regionQld = (sked__Region__c)mapTestData.get('regionQld');
        sked__Region__c regionNsw = (sked__Region__c)mapTestData.get('regionNsw');
        string[] regionIds = new string[]{regionQld.Id, regionNsw.Id};
            
        DateTime dt = system.now().addDays(7);
        Date nextWeekStartDate = skedDateTimeUtils.getDate(dt, regionQld.sked__Timezone__c).toStartOfWeek();        
        skedRACController.getAvailabilityList(regionIds, regionQld.sked__Timezone__c, JSON.serialize(nextWeekStartDate));
    }
    
    @isTest static void getResourceAvailabilities() {
        skedRACDataSetup dataSetup = new skedRACDataSetup();
        Map<string, sObject> mapTestData = dataSetup.setupRACTestData();
        sked__Region__c regionQld = (sked__Region__c)mapTestData.get('regionQld');
        sked__Resource__c resourceTeamLeader = (sked__Resource__c)mapTestData.get('resourceTeamLeader');
        
        DateTime dt = system.now().addDays(7);
        Date nextWeekStartDate = skedDateTimeUtils.getDate(dt, regionQld.sked__Timezone__c).toStartOfWeek();
        skedRACController.getResourceAvailabilities(resourceTeamLeader.Id, regionQld.sked__Timezone__c, JSON.serialize(nextWeekStartDate));
    }
    
    @isTest static void testShift() {
        skedRACDataSetup dataSetup = new skedRACDataSetup();
        Map<string, sObject> mapTestData = dataSetup.setupRACTestData();
        sked__Region__c regionNsw = (sked__Region__c)mapTestData.get('regionNsw');
        sked__Resource__c resourceTeamLeader = (sked__Resource__c)mapTestData.get('resourceTeamLeader');
        
        DateTime dt = system.now().addDays(7);
        
        skedRACController.shiftSaveModel model = new skedRACController.shiftSaveModel();
        model.availabilityType = 'Available';
        model.endTimeInt = 1700;
        model.isActivity = false; 
        model.pattern = 1;
        model.patternWeeks = new List<string>{'mon', 'wed', 'fri'};
        model.recurWeeks = 5;
        model.recurring = true;
        model.regionId = regionNsw.Id;
        model.resourceId = resourceTeamLeader.Id;
        model.selectedDateIso = dt.format('yyyy-MM-dd');
        model.skipHolidays = false;
        model.skipWeekends = true;
        model.startTimeInt = 800;
        model.timezoneSidId = regionNsw.sked__Timezone__c;
        
        string jsonModel = JSON.serialize(model);
        skedRemoteResultModel result = new skedRemoteResultModel();
        
        result = skedRACController.saveShift(jsonModel);
        sked__Availability__c skedAvailability = (sked__Availability__c)result.data;
        skedRACController.shiftModel activityShiftModel = new skedRACController.shiftModel(skedAvailability, regionNsw.sked__Timezone__c);
        
        skedRACController.shiftSaveModel activityModel = new skedRACController.shiftSaveModel();
        activityModel.activityType = 'Meal Break';
        activityModel.address = '27 Torrens St, Braddon ACT 2612, Australia';
        activityModel.endTimeInt = 1700;
        activityModel.isActivity = true; 
        activityModel.lat = -35.2728161;
        activityModel.lng = 149.1339697;
        activityModel.pattern = 1;
        activityModel.patternWeeks = new List<string>{'mon', 'sat'};
        activityModel.recurWeeks = 5;
        activityModel.recurring = true;
        activityModel.regionId = regionNsw.Id;
        activityModel.resourceId = resourceTeamLeader.Id;
        activityModel.selectedDateIso = dt.format('yyyy-MM-dd');
        activityModel.skipHolidays = false;
        activityModel.skipWeekends = false;
        activityModel.startTimeInt = 800;
        activityModel.timezoneSidId = regionNsw.sked__Timezone__c;
        
        jsonModel = JSON.serialize(activityModel);
        
        result = skedRACController.saveShift(jsonModel);
        sked__Activity__c skedActivity = (sked__Activity__c)result.data;

        //dt = skedDateTimeUtils.addDays(dt, 7, regionNsw.sked__Timezone__c);
        Date d = skedDateTimeUtils.getDate(dt, regionNsw.sked__Timezone__c);
        d = d.toStartOfWeek();
        DateTime repDate = DateTime.newInstance(d, Time.newInstance(0,0,0,0));

        skedRACController.replicateModel repModel = new skedRACController.replicateModel();
        repModel.resourceId = resourceTeamLeader.Id;
        repModel.sourceFrom = repDate.format('yyyy-MM-dd');
        repModel.sourceTo = skedDateTimeUtils.addDays(repDate, 14, regionNsw.sked__Timezone__c).format('yyyy-MM-dd');
        repModel.destinationFrom = skedDateTimeUtils.addDays(repDate, 30, regionNsw.sked__Timezone__c).format('yyyy-MM-dd');
        repModel.destinationTo = skedDateTimeUtils.addDays(repDate, 90, regionNsw.sked__Timezone__c).format('yyyy-MM-dd');
        repModel.canOverride = true;

        jsonModel = JSON.serialize(repModel);
        skedRACController.saveReplication(jsonModel);
        
        skedRACController.shiftModel skedActivityShiftModel = new skedRACController.shiftModel(skedActivity, regionNsw.sked__Timezone__c);
        
        skedRACController.approveShift(skedActivity.Id);
        skedRACController.deleteShift(skedActivity.Id);
    }
    
    @isTest static void testDateTimeUtils() {
        skedRACDataSetup dataSetup = new skedRACDataSetup();
        Map<string, sObject> mapTestData = dataSetup.setupRACTestData();
        sked__Region__c regionNsw = (sked__Region__c)mapTestData.get('regionNsw');
        
        DateTime dt = skedDateTimeUtils.addMonths(system.now(), 1, regionNsw.sked__Timezone__c);
        dt = skedDateTimeUtils.addYears(dt, 1, regionNsw.sked__Timezone__c);
        DateTime testDate = skedDateTimeUtils.getStartOfDate(dt, regionNsw.sked__Timezone__c);
        testDate = skedDateTimeUtils.getEndOfDate(dt, regionNsw.sked__Timezone__c);
    }
    
    public class skedRACDataSetup {
        public void setupSkeduloSfhitTypeSettings() {
            List<sked_Shift_Type_Setting__c> shiftTypeSettings = new List<sked_Shift_Type_Setting__c>();
            
            sked_Shift_Type_Setting__c setting1 = new sked_Shift_Type_Setting__c(
                Name = 'Annual Leave Setting',
                Short_Name__c = 'ALS',
                Shift_Type__c = 'Annual Leave',
                Can_Be_Pre_Dated__c = FALSE,
                Color__c = '#D8D8D8',
                Background_Color__c = '#C5C5C4'
            );
            shiftTypeSettings.add(setting1);
            
            sked_Shift_Type_Setting__c setting2 = new sked_Shift_Type_Setting__c(
                Name = 'Available Setting',
                Short_Name__c = 'AS',
                Shift_Type__c = 'Available',
                Can_Be_Pre_Dated__c = FALSE,
                Color__c = '#EBFFC6',            
                Background_Color__c = '#F0C1D3'
            );
            shiftTypeSettings.add(setting2);
            
            sked_Shift_Type_Setting__c setting3 = new sked_Shift_Type_Setting__c(
                Name = 'Occupied Setting',
                Short_Name__c = 'OS',
                Shift_Type__c = 'Occupied Leave',
                Can_Be_Pre_Dated__c = FALSE,
                Color__c = '#D8D8D8',            
                Background_Color__c = '#CCE4D0'
            );
            shiftTypeSettings.add(setting3);
            
            sked_Shift_Type_Setting__c setting4 = new sked_Shift_Type_Setting__c(
                Name = 'Sick Leave Setting',
                Short_Name__c = 'SLS',
                Shift_Type__c = 'Sick Leave',
                Can_Be_Pre_Dated__c = TRUE,
                Color__c = '#D8D8D8',            
                Background_Color__c = '#FFD6D6'
            );
            shiftTypeSettings.add(setting4);
            
            insert shiftTypeSettings;
        }
        
        public void setupskedRACSetting() {
            sked_RAC_Setting__c setting = new sked_RAC_Setting__c();
            setting.Respect_Resource_Timezone__c = true;
            setting.Enable_Availability_Template__c = true;
            insert setting;
        }
        
        public Map<string, sObject> setupCommonTestData() {
            Map<string, sObject> mapTestData = new Map<string, sObject>();
            string timezoneSidId = UserInfo.getTimeZone().getId();
            DateTime current = system.now();
            Date startOfWeek = skedDateTimeUtils.getDate(current, timezoneSidId).toStartOfWeek();
            Date nextWeekStartDate = startOfWeek.addDays(7);
            DateTime nextWeekStartTime = DateTime.newInstance(nextWeekStartDate, time.newInstance(0, 0, 0, 0));
            
            /*********************************************************1st Level**************************************************/
            List<sObject> firstList = new List<sObject>();
            
            /*********************************************************Region**************************************************/
            sked__Region__c regionQld = new sked__Region__c(
                Name = 'QLD',
                sked__Timezone__c = 'Australia/Queensland'
            );
            firstList.add(regionQld);
            mapTestData.put('regionQld', regionQld);
            
            sked__Region__c regionNsw = new sked__Region__c(
                Name = 'NSW',
                sked__Timezone__c = 'Australia/NSW'
            );
            firstList.add(regionNsw);
            mapTestData.put('regionNsw', regionNsw);
            
            sked__Region__c regionVic = new sked__Region__c(
                Name = 'VIC',
                sked__Timezone__c = 'Australia/Victoria'
            );
            firstList.add(regionVic);
            
            /*********************************************************Tag**************************************************/
            sked__Tag__c tagQualityControl = new sked__Tag__c(
                Name = 'Quality Control',
                sked__Type__c = 'Skill',
                sked__Classification__c = 'Global'
            );
            firstList.add(tagQualityControl);
            mapTestData.put('tagQualityControl', tagQualityControl);
            
            sked__Tag__c tagServerEngineer = new sked__Tag__c(
                Name = 'Server Engineer',
                sked__Type__c = 'Skill',
                sked__Classification__c = 'Global'
            );
            firstList.add(tagServerEngineer);
            mapTestData.put('tagServerEngineer', tagServerEngineer);
            
            /*********************************************************Holidays**************************************************/
            sked__Holiday__c globalHoliday = new sked__Holiday__c(
                Name = 'Global Holidays',
                sked__Start_Date__c = system.today().addDays(1),
                sked__End_Date__c = system.today().addDays(1),
                sked__Global__c = true
            );
            firstList.add(globalHoliday);
            
            sked__Holiday__c qldHoliday = new sked__Holiday__c(
                Name = 'Qld Holidays',
                sked__Start_Date__c = system.today().addDays(1),
                sked__End_Date__c = system.today().addDays(1),
                sked__Global__c = false
            );
            firstList.add(qldHoliday);
            /*********************************************************Account**************************************************/
            Account accountTest = new Account(
                Name = 'Test Account',
                BillingCountry = 'Australia',
                BillingStreet = '12 Maynard Rd',
                BillingCity = 'Plympton Park',
                BillingState = 'South Australia',
                BillingPostalCode = '5038'
            );
            firstList.add(accountTest);
            mapTestData.put('accountTest', accountTest);
            
            /*****************************************************Available templates**********************************************/
            sked__Availability_Template__c resourceTeamLeaderTemplate = new sked__Availability_Template__c();
            firstList.add(resourceTeamLeaderTemplate);
            
            sked__Availability_Template__c resourceCustomerService1Template = new sked__Availability_Template__c();
            firstList.add(resourceCustomerService1Template);
            
            
            insert firstList;
            /*********************************************************End of 1st level**************************************************/
            
            /*********************************************************2nd Level**************************************************/
            List<sObject> secondList = new List<sObject>();
            
            /*********************************************************Region holidays**************************************************/
            sked__Holiday_Region__c qldRegionHoliday = new sked__Holiday_Region__c(
                sked__Holiday__c = qldHoliday.Id,
                sked__Region__c = regionQld.Id
            );
            secondList.add(qldRegionHoliday);
            /*********************************************************Contact**************************************************/
            Contact contactTest = new Contact(
                FirstName = 'Test',
                LastName = 'Contact',
                AccountId = accountTest.Id,
                OtherCity = 'NY',
                OtherPostalCode = '4006',
                Phone = '0401467938',
                RecordTypeId = skedTestDataFactory.getRecordTypeIDByDeveloperName('Contact','Client')
            );
            secondList.add(contactTest);
            mapTestData.put('contactTest', contactTest);
            
            /*********************************************************Opportunity**************************************************/
            Opportunity opportunityTest = new Opportunity(
                Name = 'Test Opportunity',
                AccountId = accountTest.Id,
                StageName = 'Negotiation',
                CloseDate = system.today().addMonths(1)
            );
            secondList.add(opportunityTest);
            mapTestData.put('opportunityTest', opportunityTest);
            
            /*********************************************************Resource**************************************************/
            sked__Resource__c resourceTeamLeader = new sked__Resource__c(
                Name = 'Team Leader',
                sked__Primary_Region__c = regionNsw.Id,
                sked__Category__c = 'Team Leader',
                sked__Resource_Type__c = 'Person',
                sked__Home_Address__c = '79 McLachlan St, Fortitude Valley QLD 4006, Australia',
                sked__GeoLocation__Latitude__s = -27.457730,
                sked__GeoLocation__Longitude__s = 153.037080,
                sked__User__c = UserInfo.getUserId(),
                sked__Is_Active__c = TRUE
            );
            secondList.add(resourceTeamLeader);
            mapTestData.put('resourceTeamLeader', resourceTeamLeader);
            
            sked__Resource__c resourceCustomerService1 = new sked__Resource__c(
                Name = 'Customer Service 1',
                sked__Primary_Region__c = regionQld.Id,
                sked__Category__c = 'Customer Service',
                sked__Resource_Type__c = 'Person',
                sked__Home_Address__c = '79 McLachlan St, Fortitude Valley QLD 4006, Australia',
                sked__GeoLocation__Latitude__s = -27.457730,
                sked__GeoLocation__Longitude__s = 153.037080,
                sked__Is_Active__c = TRUE
            );
            secondList.add(resourceCustomerService1);
            mapTestData.put('resourceCustomerService1', resourceCustomerService1);
            
            sked__Resource__c resourceCustomerService2 = new sked__Resource__c(
                Name = 'Customer Service 2',
                sked__Primary_Region__c = regionQld.Id,
                sked__Category__c = 'Customer Service',
                sked__Resource_Type__c = 'Person',
                sked__Home_Address__c = '79 McLachlan St, Fortitude Valley QLD 4006, Australia',
                sked__GeoLocation__Latitude__s = -27.457730,
                sked__GeoLocation__Longitude__s = 153.037080,
                sked__Is_Active__c = TRUE
            );
            secondList.add(resourceCustomerService2);
            mapTestData.put('resourceCustomerService2', resourceCustomerService2);
            
            sked__Resource__c resourceCustomerService3 = new sked__Resource__c(
                Name = 'Customer Service 3',
                sked__Primary_Region__c = regionQld.Id,
                sked__Category__c = 'Customer Service',
                sked__Resource_Type__c = 'Person',
                sked__Home_Address__c = '79 McLachlan St, Fortitude Valley QLD 4006, Australia',
                sked__GeoLocation__Latitude__s = -27.457730,
                sked__GeoLocation__Longitude__s = 153.037080,
                sked__Is_Active__c = TRUE
            );
            secondList.add(resourceCustomerService3);
            mapTestData.put('resourceCustomerService3', resourceCustomerService3);
            
            
            insert secondList;
            /*********************************************************End of 2nd level**************************************************/
            
            /*********************************************************3rd Level**************************************************/
            List<sObject> thirdList = new List<sObject>();
            /*********************************************************Resource Tags**************************************************/
            sked__Resource_Tag__c customerService1_qualityControl = new sked__Resource_Tag__c(
                sked__Resource__c = resourceCustomerService1.Id,
                sked__Tag__c = tagQualityControl.Id
            );
            thirdList.add(customerService1_qualityControl);
            
            sked__Resource_Tag__c customerService1_serverEngineer = new sked__Resource_Tag__c(
                sked__Resource__c = resourceCustomerService1.Id,
                sked__Tag__c = tagServerEngineer.Id
            );
            thirdList.add(customerService1_serverEngineer);
            
            sked__Resource_Tag__c customerService2_serverEngineer = new sked__Resource_Tag__c(
                sked__Resource__c = resourceCustomerService2.Id,
                sked__Tag__c = tagServerEngineer.Id
            );
            thirdList.add(customerService2_serverEngineer);
            
            sked__Availability__c resourceTeamLeaderAvail1 = new sked__Availability__c(
                sked__Start__c = nextWeekStartTime.addHours(7),
                sked__Finish__c = nextWeekStartTime.addHours(12),
                sked__Is_Available__c = FALSE,
                sked__Resource__c = resourceTeamLeader.Id,
                sked__Status__c = 'Approved'
            );
            thirdList.add(resourceTeamLeaderAvail1);
            
            sked__Availability__c resourceTeamLeaderAvail2 = new sked__Availability__c(
                sked__Start__c = nextWeekStartTime.addHours(7),
                sked__Finish__c = nextWeekStartTime.addHours(12),
                sked__Is_Available__c = TRUE,
                sked__Resource__c = resourceTeamLeader.Id,
                sked__Status__c = 'Approved'
            );
            thirdList.add(resourceTeamLeaderAvail2);
            
            sked__Activity__c resourceTeamLeaderActivity = new sked__Activity__c(
                sked__Start__c = current.addDays(7),
                sked__End__c = current.addDays(7).addHours(1),
                sked__Address__c = resourceTeamLeader.sked__Home_Address__c,
                sked__GeoLocation__Latitude__s = resourceTeamLeader.sked__GeoLocation__Latitude__s,
                sked__GeoLocation__Longitude__s = resourceTeamLeader.sked__GeoLocation__Longitude__s,
                sked__Resource__c = resourceTeamLeader.Id
            );
            thirdList.add(resourceTeamLeaderActivity);
            
            /*********************************************************Available template entries************************************************/
            sked__Availability_Template_Entry__c monEntryForTeamLeader = new sked__Availability_Template_Entry__c(sked__Availability_Template__c = resourceTeamLeaderTemplate.Id, sked__Weekday__c = 'MON', sked__Start_Time__c = 900, sked__Finish_Time__c = 1700, sked__Is_Available__c = true);
            thirdList.add(monEntryForTeamLeader);
            sked__Availability_Template_Entry__c tueEntryForTeamLeader = new sked__Availability_Template_Entry__c(sked__Availability_Template__c = resourceTeamLeaderTemplate.Id, sked__Weekday__c = 'TUE', sked__Start_Time__c = 600, sked__Finish_Time__c = 1700, sked__Is_Available__c = true);
            thirdList.add(tueEntryForTeamLeader);
            sked__Availability_Template_Entry__c wedEntryForTeamLeader = new sked__Availability_Template_Entry__c(sked__Availability_Template__c = resourceTeamLeaderTemplate.Id, sked__Weekday__c = 'WED', sked__Start_Time__c = 900, sked__Finish_Time__c = 1700, sked__Is_Available__c = true);
            thirdList.add(wedEntryForTeamLeader);
            sked__Availability_Template_Entry__c thuEntryForTeamLeader = new sked__Availability_Template_Entry__c(sked__Availability_Template__c = resourceTeamLeaderTemplate.Id, sked__Weekday__c = 'THU', sked__Start_Time__c = 900, sked__Finish_Time__c = 2000, sked__Is_Available__c = true);
            thirdList.add(thuEntryForTeamLeader);
            sked__Availability_Template_Entry__c friEntryForTeamLeader = new sked__Availability_Template_Entry__c(sked__Availability_Template__c = resourceTeamLeaderTemplate.Id, sked__Weekday__c = 'FRI', sked__Start_Time__c = 900, sked__Finish_Time__c = 1700, sked__Is_Available__c = true);
            thirdList.add(friEntryForTeamLeader);
            
            sked__Availability_Template_Resource__c avaiTempTeamLeader = new sked__Availability_Template_Resource__c(sked__Availability_Template__c = resourceTeamLeaderTemplate.Id, sked__Resource__c = resourceTeamLeader.Id);
            thirdList.add(avaiTempTeamLeader);
            
            sked__Availability_Template_Entry__c monEntryForCustomerService1 = new sked__Availability_Template_Entry__c(sked__Availability_Template__c = resourceCustomerService1Template.Id, sked__Weekday__c = 'MON', sked__Start_Time__c = 900, sked__Finish_Time__c = 1700, sked__Is_Available__c = true);
            thirdList.add(monEntryForCustomerService1);
            sked__Availability_Template_Entry__c tueEntryForCustomerService1 = new sked__Availability_Template_Entry__c(sked__Availability_Template__c = resourceCustomerService1Template.Id, sked__Weekday__c = 'TUE', sked__Start_Time__c = 600, sked__Finish_Time__c = 1700, sked__Is_Available__c = true);
            thirdList.add(tueEntryForCustomerService1);
            sked__Availability_Template_Entry__c wedEntryForCustomerService1 = new sked__Availability_Template_Entry__c(sked__Availability_Template__c = resourceCustomerService1Template.Id, sked__Weekday__c = 'WED', sked__Start_Time__c = 900, sked__Finish_Time__c = 1700, sked__Is_Available__c = true);
            thirdList.add(wedEntryForCustomerService1);
            sked__Availability_Template_Entry__c thuEntryForCustomerService1 = new sked__Availability_Template_Entry__c(sked__Availability_Template__c = resourceCustomerService1Template.Id, sked__Weekday__c = 'THU', sked__Start_Time__c = 900, sked__Finish_Time__c = 2000, sked__Is_Available__c = true);
            thirdList.add(thuEntryForCustomerService1);
            sked__Availability_Template_Entry__c friEntryForCustomerService1 = new sked__Availability_Template_Entry__c(sked__Availability_Template__c = resourceCustomerService1Template.Id, sked__Weekday__c = 'FRI', sked__Start_Time__c = 900, sked__Finish_Time__c = 1700, sked__Is_Available__c = true);
            thirdList.add(friEntryForCustomerService1);
            
            sked__Availability_Template_Resource__c avaiTempCustomerService1 = new sked__Availability_Template_Resource__c(sked__Availability_Template__c = resourceCustomerService1Template.Id, sked__Resource__c = resourceCustomerService1.Id);
            thirdList.add(avaiTempCustomerService1);
            
            insert thirdList;
            /*********************************************************End of 3rd level**************************************************/
            
            /*********************************************************4th Level**************************************************/
            List<sObject> fourthList = new List<sObject>();
            /*********************************************************Jobs**************************************************/
            sked__Job__c job1 = new sked__Job__c(
                sked__Account__c = accountTest.Id,
                sked__Region__c = regionNsw.Id,
                sked__Start__c = nextWeekStartTime.addHours(8),
                sked__Finish__c = nextWeekStartTime.addHours(9),
                sked__Duration__c = 60,
                sked__Type__c = 'Group Event',
                sked__Address__c = '1/227-239 Wells Rd, Chelsea Heights VIC 3196, Australia',
                sked__GeoLocation__Latitude__s = -38.041935,
                sked__GeoLocation__Longitude__s = 145.1392648,
                sked__Job_Status__c = skedConstants.JOB_STATUS_PENDING_ALLOCATION,
                sked__Contact__c = contactTest.Id
            );
            fourthList.add(job1);
            mapTestData.put('job1', job1);
            
            sked__Job__c job2 = new sked__Job__c(
                sked__Account__c = accountTest.Id,
                sked__Region__c = regionNsw.Id,
                sked__Start__c = nextWeekStartTime.addHours(13),
                sked__Finish__c = nextWeekStartTime.addHours(14),
                sked__Duration__c = 60,
                sked__Type__c = 'Group Event',
                sked__Address__c = '1/227-239 Wells Rd, Chelsea Heights VIC 3196, Australia',
                sked__GeoLocation__Latitude__s = -38.041935,
                sked__GeoLocation__Longitude__s = 145.1392648,
                sked__Job_Status__c = skedConstants.JOB_STATUS_PENDING_ALLOCATION,
                sked__Contact__c = contactTest.Id
            );
            fourthList.add(job2);
            mapTestData.put('job2', job2);            
            
            insert fourthList;
            /*********************************************************End of 4th level**************************************************/
            
            /*********************************************************5th Level**************************************************/
            List<sObject> fifthList = new List<sObject>();
            /*********************************************************Job Allocations**************************************************/
            sked__Job_Allocation__c jobAllocation1 = new sked__Job_Allocation__c(
                sked__Job__c = job1.Id,
                sked__Resource__c = resourceTeamLeader.Id,
                sked__Status__c = skedConstants.JOB_ALLOCATION_STATUS_PENDING_DISPATCH
            );
            fifthList.add(jobAllocation1);
            
            sked__Job_Allocation__c jobAllocation2 = new sked__Job_Allocation__c(
                sked__Job__c = job2.Id,
                sked__Resource__c = resourceTeamLeader.Id,
                sked__Status__c = skedConstants.JOB_ALLOCATION_STATUS_PENDING_DISPATCH
            );
            fifthList.add(jobAllocation2);
            
            insert fifthList;
            /*********************************************************End of 7th level**************************************************/
            return mapTestData;
        }
        
        public Map<string, sObject> setupRACTestData() {
            setupSkeduloSfhitTypeSettings();
            setupskedRACSetting();
            
            Map<string, sObject> mapTestData = setupCommonTestData();
            
            return mapTestData;
        }
        
    }
}