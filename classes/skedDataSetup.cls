public class skedDataSetup {
    
    public static boolean skeduloAdminSettingsSetup = FALSE;

    public static void setupServiceTypeColorSettings() {
        List<Service_Type_Color__c> serviceTypeColorSettings = [SELECT Id from Service_Type_Color__c where Name = 'Default'];

        if(serviceTypeColorSettings.size()==0)
        {
            Service_Type_Color__c setting1 = new Service_Type_Color__c(
                    Name = 'Default',
                    Dark_Color__c = '#D8D8D8',
                    Light_Color__c = '#EBFFC6'
            );
            
            serviceTypeColorSettings.add(setting1);
        }

        upsert serviceTypeColorSettings;
    }
    
    public static void setupSkeduloSfhitTypeSettings() {
        List<sked_Shift_Type_Setting__c> shiftTypeSettings = new List<sked_Shift_Type_Setting__c>();
        
        sked_Shift_Type_Setting__c setting1 = new sked_Shift_Type_Setting__c(
            Name = 'Annual Leave Setting',
            Shift_Type__c = 'Annual Leave',
            Can_Be_Pre_Dated__c = FALSE,
            Available_Start__c = 0,
            Available_End__c = 2400,
            Default_Start__c = NULL,
            Default_End__c = NULL,
            Step__c = 60,
            Not_Show__c = FALSE,
            Is_AVailable__c = FALSE,
            Color__c = '#D8D8D8'
        );
        shiftTypeSettings.add(setting1);
        
        sked_Shift_Type_Setting__c setting2 = new sked_Shift_Type_Setting__c(
            Name = 'Available Setting',
            Shift_Type__c = 'Available',
            Can_Be_Pre_Dated__c = FALSE,
            Available_Start__c = 0,
            Available_End__c = 2400,
            Default_Start__c = NULL,
            Default_End__c = NULL,
            Step__c = 30,
            Not_Show__c = FALSE,
            Is_AVailable__c = TRUE,
            Color__c = '#EBFFC6'
        );
        shiftTypeSettings.add(setting2);
        
        sked_Shift_Type_Setting__c setting3 = new sked_Shift_Type_Setting__c(
            Name = 'Occupied Setting',
            Shift_Type__c = 'Occupied Leave',
            Can_Be_Pre_Dated__c = FALSE,
            Available_Start__c = 0,
            Available_End__c = 2400,
            Default_Start__c = NULL,
            Default_End__c = NULL,
            Step__c = 15,
            Not_Show__c = FALSE,
            Is_AVailable__c = FALSE,
            Color__c = '#D8D8D8'
        );
        shiftTypeSettings.add(setting3);
        
        sked_Shift_Type_Setting__c setting4 = new sked_Shift_Type_Setting__c(
            Name = 'Sick Leave Setting',
            Shift_Type__c = 'Sick Leave',
            Can_Be_Pre_Dated__c = TRUE,
            Available_Start__c = 0,
            Available_End__c = 2400,
            Default_Start__c = NULL,
            Default_End__c = NULL,
            Step__c = 30,
            Not_Show__c = FALSE,
            Is_AVailable__c = FALSE,
            Color__c = '#D8D8D8'
        );
        shiftTypeSettings.add(setting4);
        
        insert shiftTypeSettings;
    }
    
    public static void setupSkeduloAdminSettings() {
        if (skeduloAdminSettingsSetup == TRUE) {
            return;
        } else {
            skeduloAdminSettingsSetup = TRUE;
        }
        sked_Admin_Settings__c adminSettings = sked_Admin_Settings__c.getOrgDefaults();
        adminSettings.End_Working_Time__c = 1800;
        adminSettings.Number_of_Jobs_to_alert__c = 32;
        adminSettings.Replicate_Max_Weeks__c = 20;
        adminSettings.Start_Working_Time__c = 900;
        
        upsert adminSettings;
    }
    
    public static Map<string, sObject> setupCommonTestData() {
        Map<string, sObject> mapTestData = new Map<string, sObject>();
        
        /*********************************************************1st Level**************************************************/
        List<sObject> firstList = new List<sObject>();
        
        /*********************************************************Region**************************************************/
        sked__Region__c regionQld = new sked__Region__c(
            Name = 'QLD',
            sked__Timezone__c = 'Australia/Queensland'
        );
        firstList.add(regionQld);
        mapTestData.put('regionQld', regionQld);
        
        sked__Region__c regionNsw = new sked__Region__c(
            Name = 'NSW',
            sked__Timezone__c = 'Australia/NSW'
        );
        firstList.add(regionNsw);
        
        sked__Region__c regionVic = new sked__Region__c(
            Name = 'VIC',
            sked__Timezone__c = 'Australia/Victoria'
        );
        firstList.add(regionVic);
        
        /*********************************************************Tag**************************************************/
        sked__Tag__c tagQualityControl = new sked__Tag__c(
            Name = 'Quality Control',
            sked__Type__c = 'Skill',
            sked__Classification__c = 'Global'
        );
        firstList.add(tagQualityControl);
        mapTestData.put('tagQualityControl', tagQualityControl);
        
        sked__Tag__c tagServerEngineer = new sked__Tag__c(
            Name = 'Server Engineer',
            sked__Type__c = 'Skill',
            sked__Classification__c = 'Global'
        );
        firstList.add(tagServerEngineer);
        mapTestData.put('tagServerEngineer', tagServerEngineer);
        
        /*********************************************************Holidays**************************************************/
        sked__Holiday__c globalHoliday = new sked__Holiday__c(
            Name = 'Global Holidays',
            sked__Start_Date__c = system.today().addDays(1),
            sked__End_Date__c = system.today().addDays(1),
            sked__Global__c = true
        );
        firstList.add(globalHoliday);
        
        sked__Holiday__c qldHoliday = new sked__Holiday__c(
            Name = 'Qld Holidays',
            sked__Start_Date__c = system.today().addDays(1),
            sked__End_Date__c = system.today().addDays(1),
            sked__Global__c = false
        );
        firstList.add(qldHoliday);
        /*********************************************************Account**************************************************/
        Account accountTest = new Account(
            Name = 'Test Account',
            BillingCountry = 'Australia',
            BillingStreet = '12 Maynard Rd',
            BillingCity = 'Plympton Park',
            BillingState = 'South Australia',
            BillingPostalCode = '5038'
        );
        firstList.add(accountTest);
        mapTestData.put('accountTest', accountTest);
        
        
        insert firstList;
        /*********************************************************End of 1st level**************************************************/
        
        /*********************************************************2nd Level**************************************************/
        List<sObject> secondList = new List<sObject>();
        
        /*********************************************************Region holidays**************************************************/
        sked__Holiday_Region__c qldRegionHoliday = new sked__Holiday_Region__c(
            sked__Holiday__c = qldHoliday.Id,
            sked__Region__c = regionQld.Id
        );
        secondList.add(qldRegionHoliday);
        /*********************************************************Contact**************************************************/
        Contact contactTest = new Contact(
            FirstName = 'Test',
            LastName = 'Contact',
            AccountId = accountTest.Id,
            enrtcr__Primary_Referral_Source__c = 'Internet - EFA Website',
            enrtcr__Client_Region__c = 'Test',
            OtherCity = 'NY',
            OtherPostalCode = '4006',
            Phone = '0401467938'
        );
        secondList.add(contactTest);
        mapTestData.put('contactTest', contactTest);


        Contact contactTest2 = new Contact(
                FirstName = 'Test',
                LastName = 'Contact2',
                AccountId = accountTest.Id,
                enrtcr__Primary_Referral_Source__c = 'Internet - EFA Website',
                enrtcr__Client_Region__c = 'Test',
                OtherCity = 'NY',
                OtherPostalCode = '4006',
                Phone = '0401467938'
        );
        secondList.add(contactTest2);
        mapTestData.put('contactTest2', contactTest2);

        Contact employeeTest = new Contact(
                FirstName = 'Test',
                LastName = 'Employee',
                AccountId = accountTest.Id,
                enrtcr__Primary_Referral_Source__c = 'Internet - EFA Website',
                enrtcr__Client_Region__c = 'Test',
                RecordTypeId= skedTestDataFactory.getRecordTypeIDByDeveloperName('Contact','Employee'),
                OtherCity = 'NY',
                OtherPostalCode = '4006',
                Phone = '0401467938'
        );
        secondList.add(employeeTest);
        mapTestData.put('employeeTest', employeeTest);
        
        /*********************************************************Opportunity**************************************************/
        Opportunity opportunityTest = new Opportunity(
            Name = 'Test Opportunity',
            AccountId = accountTest.Id,
            StageName = 'Negotiation',
            CloseDate = system.today().addMonths(1)
        );
        secondList.add(opportunityTest);
        mapTestData.put('opportunityTest', opportunityTest);
        
        /*********************************************************Resource**************************************************/
        sked__Resource__c resourceTeamLeader = new sked__Resource__c(
            Name = 'Team Leader',
            sked__Primary_Region__c = regionQld.Id,
            sked__Category__c = 'Team Leader',
            sked__Resource_Type__c = 'Person',
            sked__Home_Address__c = '79 McLachlan St, Fortitude Valley QLD 4006, Australia',
            sked__GeoLocation__Latitude__s = -27.457730,
            sked__GeoLocation__Longitude__s = 153.037080,
            Travel_Method__c = 'Own Car',
            Maximum_Travel_Distance__c = 999,
            sked__User__c = UserInfo.getUserId(),
            sked__Is_Active__c = TRUE
        );
        secondList.add(resourceTeamLeader);
        mapTestData.put('resourceTeamLeader', resourceTeamLeader);
        
        sked__Resource__c resourceCustomerService1 = new sked__Resource__c(
            Name = 'Customer Service 1',
            sked__Primary_Region__c = regionQld.Id,
            sked__Category__c = 'Customer Service',
            sked__Resource_Type__c = 'Person',
            sked__Home_Address__c = '79 McLachlan St, Fortitude Valley QLD 4006, Australia',
            sked__GeoLocation__Latitude__s = -27.457730,
            sked__GeoLocation__Longitude__s = 153.037080,
            Travel_Method__c = 'Own Car',
            Maximum_Travel_Distance__c = 999,
            sked__Is_Active__c = TRUE
        );
        secondList.add(resourceCustomerService1);
        mapTestData.put('resourceCustomerService1', resourceCustomerService1);
        
        sked__Resource__c resourceCustomerService2 = new sked__Resource__c(
            Name = 'Customer Service 2',
            sked__Primary_Region__c = regionQld.Id,
            sked__Category__c = 'Customer Service',
            sked__Resource_Type__c = 'Person',
            sked__Home_Address__c = '79 McLachlan St, Fortitude Valley QLD 4006, Australia',
            sked__GeoLocation__Latitude__s = -27.457730,
            sked__GeoLocation__Longitude__s = 153.037080,
            Travel_Method__c = 'Own Car',
            Maximum_Travel_Distance__c = 999,
            sked__Is_Active__c = TRUE
        );
        secondList.add(resourceCustomerService2);
        mapTestData.put('resourceCustomerService2', resourceCustomerService2);
        
        sked__Resource__c resourceCustomerService3 = new sked__Resource__c(
            Name = 'Customer Service 3',
            sked__Primary_Region__c = regionQld.Id,
            sked__Category__c = 'Customer Service',
            sked__Resource_Type__c = 'Person',
            sked__Home_Address__c = '79 McLachlan St, Fortitude Valley QLD 4006, Australia',
            sked__GeoLocation__Latitude__s = -27.457730,
            sked__GeoLocation__Longitude__s = 153.037080,
            Travel_Method__c = 'Own Car',
            Maximum_Travel_Distance__c = 999,
            sked__Is_Active__c = TRUE
        );
        secondList.add(resourceCustomerService3);
        mapTestData.put('resourceCustomerService3', resourceCustomerService3);
        
        
        insert secondList;
        /*********************************************************End of 2nd level**************************************************/
        
        /*********************************************************3rd Level**************************************************/
        List<sObject> thirdList = new List<sObject>();
        
        /*********************************************************Service Delivery**************************************************/

        
        /*********************************************************Resource Tags**************************************************/
        sked__Resource_Tag__c customerService1_qualityControl = new sked__Resource_Tag__c(
            sked__Resource__c = resourceCustomerService1.Id,
            sked__Tag__c = tagQualityControl.Id
        );
        thirdList.add(customerService1_qualityControl);
        
        sked__Resource_Tag__c customerService1_serverEngineer = new sked__Resource_Tag__c(
            sked__Resource__c = resourceCustomerService1.Id,
            sked__Tag__c = tagServerEngineer.Id
        );
        thirdList.add(customerService1_serverEngineer);
        
        sked__Resource_Tag__c customerService2_serverEngineer = new sked__Resource_Tag__c(
            sked__Resource__c = resourceCustomerService2.Id,
            sked__Tag__c = tagServerEngineer.Id
        );
        thirdList.add(customerService2_serverEngineer);
        
        insert thirdList;
        /*********************************************************End of 3rd level**************************************************/
        
        return mapTestData;
    }
    
    public static Map<string, sObject> setupSMCTestData() {
        setupSkeduloAdminSettings();
        setupSkeduloSfhitTypeSettings();
        Map<string, sObject> mapTestData = setupCommonTestData();
        
        return mapTestData;
    }
    
    public static Map<string, sObject> setupSDJMTestData() {
        setupSkeduloAdminSettings();
        setupSkeduloSfhitTypeSettings();
        Map<string, sObject> mapTestData = setupCommonTestData();
        
        List<sked__Resource__c> resources = new List<sked__Resource__c>();
        resources.add((sked__Resource__c)mapTestData.get('resourceTeamLeader'));
        resources.add((sked__Resource__c)mapTestData.get('resourceCustomerService1'));
        resources.add((sked__Resource__c)mapTestData.get('resourceCustomerService2'));
        resources.add((sked__Resource__c)mapTestData.get('resourceCustomerService3'));
        
        DateTime currentTime = system.now();
        DateTime tempDt = currentTime;
        List<sked__Availability__c> resourceAvailabilities = new List<sked__Availability__c>();
        for (sked__Resource__c resource : resources) {
            while (tempDt < currentTime.addMonths(1)) {
                DateTime startOfDay = DateTime.newInstance(tempDt.date(), time.newInstance(0, 0, 0, 0));
                DateTime endOfDay = startOfDay.addDays(1);
                sked__Availability__c avail = new sked__Availability__c(
                    sked__Resource__c = resource.Id,
                    sked__Is_Available__c = TRUE,
                    sked__Type__c = 'Available',
                    sked__Status__c = 'Approved',
                    sked__Start__c = startOfDay,
                    sked__Finish__c = endOfDay
                );
                resourceAvailabilities.add(avail);
                
                tempDt = tempDt.addDays(1);
            }
        }
        insert resourceAvailabilities;
        
        return mapTestData;
    }
    
    public static Map<string, sObject> setupJobTestData() {
        setupSkeduloAdminSettings();
        setupSkeduloSfhitTypeSettings();
        Map<string, sObject> mapTestData = setupCommonTestData();
        sked__Region__c regionQld = (sked__Region__c)mapTestData.get('regionQld');
        DateTime dt = system.now();
        List<sked__Job__c> jobList = new List<sked__Job__c>();
            
        for(integer i = 1; i <= 50; i++) {
            string durationText = Math.mod(i, 5) == 1 ? '1h' : Math.mod(i, 5) + 'd';
            sked__Job__c job = new sked__Job__c(
       
                sked__Type__c = 'Desktop',
                sked__Region__c = regionQld.Id,
                sked__Start__c = dt,
                sked__Finish__c = dt.addHours(1),
                sked__Duration__c = 60,
                sked__Address__c = '79 mclachlan Fortitude valley',
                sked__Job_Status__c = 'Pending Dispatch',
                sked__Contact__c = mapTestData.get('contactTest').Id
            );
            jobList.add(job);
            mapTestData.put('job' + i, job);
            if(Math.mod(i, 5) == 0) {
                dt = dt.addDays(1);
            }
        }
        insert jobList;
        sked__Job__c parentJob = jobList.get(5);
        sked__Job__c followJob1 = jobList.get(6);
        followJob1.sked__Parent__c = parentJob.Id;
        sked__Job__c followJob2 = jobList.get(6);
        followJob2.sked__Parent__c = parentJob.Id;

        
        return mapTestData;
    }
}