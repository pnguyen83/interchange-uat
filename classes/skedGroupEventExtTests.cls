@isTest
private class skedGroupEventExtTests {

    @isTest static void TestGeneralMethods() {
        skedDataFactory.createRegions();
        Account acc = skedDataFactory.createAccount();
        Contact con = skedDataFactory.createEmployee(acc);
        Contact client = skedDataFactory.createClient(acc, 'TesClient');
        enrtcr__Service__c serviceItem  = skedDataFactory.createServiceItemGroup('Test Service');
        sked__Location__c location = skedDataFactory.createLocation();
        List<enrtcr__Site__c> testSites = new List<enrtcr__Site__c>();
        testSites.add(new enrtcr__Site__c(Name = 'Test'));
        insert testSites;

        skedConfigs__c locSetting =  new skedConfigs__c(Name = 'RM_LocationOrSiteFilter', Value__c='Location');
        insert locSetting;

        Test.startTest();
            skedGroupEventExt.getAllRegions();
            skedGroupEventExt.getSkedGroupEvent('',location.id);
            locSetting.Value__c = 'Site';
            update locSetting;
            skedGroupEventExt.getSkedGroupEvent('',testSites[0].id);
            skedGroupEventExt.searchFacilitator('Tes');
            skedGroupEventExt.searchAccount('Tes');
            skedGroupEventExt.searchServiceItem('Tes');
            skedGroupEventExt.searchClients('Tes');
        Test.stopTest();
    }

    @isTest static void TestInsertGroupAttendee() {
         Map<string, sObject> mapTestData = skedDataSetup.setupJobTestData();

        Contact con = (Contact)mapTestData.get('employeeTest');
        Contact client = (Contact)mapTestData.get('contactTest');
        Contact client2 = (Contact)mapTestData.get('contactTest2');
        Account acc = (Account)mapTestData.get('accountTest');
        sked__Job__c job = (sked__Job__c)mapTestData.get('job1');

        Group_Attendee__c groupAtendee = new Group_Attendee__c(Job__c=job.id, Contact__c=client.id);
        insert groupAtendee;
    }
    
    @isTest static void testCreateGroupEvent()
    {

        Map<string, sObject> mapTestData = skedDataSetup.setupJobTestData();

        Contact con = (Contact)mapTestData.get('employeeTest');
        Contact client = (Contact)mapTestData.get('contactTest');
        Contact client2 = (Contact)mapTestData.get('contactTest2');
        Account acc = (Account)mapTestData.get('accountTest');

        enrtcr__Support_Contract_Item__c serviceItem = skedDataFactory.createSupportContract(client);
        Group_Event__c GE = skedDataFactory.createGroupEvent(con, null, (sked__Region__c)mapTestData.get('regionQld'),
                acc , client);


        sked__Job__c job = (sked__Job__c)mapTestData.get('job1');


        skedGroupEvent skedGE = skedGroupEventExt.getSkedGroupEvent(GE.id,'');
        skedGE.startDate = Datetime.now().format('dd/MM/YYYY');
        skedGE.startTime = Datetime.now().format('HH:mm');
        skedGE.duration = 60;

        list<skedGroupClient> newAtteedees = new list<skedGroupClient>();
        skedGroupClient skedGC = new skedGroupClient();
        skedGC.accId = acc.id;
        skedGC.id = client2.Id;
        newAtteedees.add(skedGC);


        Test.startTest();
        string gID = skedGroupEventExt.saveGroupEvent(skedGE);
        skedGE.listGroupClients = newAtteedees;
        Group_Attendee__c att = new Group_Attendee__c(Job__c = job.Id, Contact__c = client.id);
        insert att;
        skedGE.altAddressLookup = 'Location';
        skedGroupEventExt.saveGroupEvent(skedGE);
        skedGE.altAddressLookup = 'Site';
        skedGroupEventExt.saveGroupEvent(skedGE);

        Test.stopTest();
    }


    @isTest static void TestupdateAttendees()
    {
        Map<string, sObject> mapTestData = skedDataSetup.setupJobTestData();

        Contact con = (Contact)mapTestData.get('employeeTest');
        Contact client = (Contact)mapTestData.get('contactTest');
        Contact client2 = (Contact)mapTestData.get('contactTest2');
        Account acc = (Account)mapTestData.get('accountTest');

        enrtcr__Support_Contract_Item__c serviceItem = skedDataFactory.createSupportContract(client);
        Group_Event__c GE = skedDataFactory.createGroupEvent(con, null, (sked__Region__c)mapTestData.get('regionQld'),
               acc , client);


        sked__Job__c job = (sked__Job__c)mapTestData.get('job1');


        skedGroupEvent skedGE = skedGroupEventExt.getSkedGroupEvent(GE.id,'');
        skedGE.startDate = Datetime.now().format('dd/MM/YYYY');
        skedGE.startTime = Datetime.now().format('HH:mm');
        skedGE.duration = 60;

        list<skedGroupClient> newAtteedees = new list<skedGroupClient>();
        skedGroupClient skedGC = new skedGroupClient();
        skedGC.accId = acc.id;
        skedGC.id = client2.Id;
        newAtteedees.add(skedGC);


        Test.startTest();

        Group_Attendee__c att = new Group_Attendee__c(Job__c = job.Id, Contact__c = client.id);
        insert att;

        att.Note__c = 'Test Note';
        update att;

        skedGroupEventExt.updateAttendees(newAtteedees, job.Id);

        Test.stopTest();
    }


    @isTest static void testSearchAtteedees()
    {

        Map<string, sObject> mapTestData = skedDataSetup.setupJobTestData();

        Contact con = (Contact)mapTestData.get('employeeTest');
        Contact client = (Contact)mapTestData.get('contactTest');
        Contact client2 = (Contact)mapTestData.get('contactTest2');
        Account acc = (Account)mapTestData.get('accountTest');

        enrtcr__Support_Contract_Item__c serviceItem = skedDataFactory.createSupportContract(client);
        Group_Event__c GE = skedDataFactory.createGroupEvent(con, null, (sked__Region__c)mapTestData.get('regionQld'),
                acc , client);


        sked__Job__c job = (sked__Job__c)mapTestData.get('job1');


        skedGroupEvent skedGE = skedGroupEventExt.getSkedGroupEvent(GE.id,'');
        skedGE.startDate = Datetime.now().format('dd/MM/YYYY');
        skedGE.startTime = Datetime.now().format('HH:mm');
        skedGE.duration = 60;

        list<skedGroupClient> newAtteedees = new list<skedGroupClient>();
        skedGroupClient skedGC = new skedGroupClient();
        skedGC.accId = acc.id;
        skedGC.id = client2.Id;
        newAtteedees.add(skedGC);

        Test.startTest();
        Group_Attendee__c att = new Group_Attendee__c(Job__c = job.Id, Contact__c = client.id);
        insert att;

        att.Note__c = 'Test Note';
        update att;

        skedGroupEventExt.searchAtteedees(job.Id);

        Test.stopTest();
    }


    @isTest static void testCreateJobFromGroup(){
        Account acc = skedDataFactory.createAccount();
        Contact emp = skedDataFactory.createEmployee(acc);
        Contact client = skedDataFactory.createClient(acc, 'Skedulo Test');
        sked__Region__c reg = skedDataFactory.createRegions()[0];
        Group_Event__c ge = skedDataFactory.createGroupEvent(emp, null,  reg,  acc,  client);
        
        sked__Tag__c tag = new sked__Tag__c(
            Name = 'Quality Control',
            sked__Type__c = 'Skill',
            sked__Classification__c = 'Global'
        );
        insert tag;

        Group_Tag__c gt = new Group_Tag__c(Group_Event__c = Ge.id, Tag__c = tag.Id, Required__c = true);
        insert gt;

        skedTag st = new skedTag(tag);
        st.required = true;
        skedGroupEvent skedGE = skedGroupEventExt.getSkedGroupEvent(ge.id,'');
        skedGE.startDate = Datetime.now().format('dd/MM/YYYY');
        skedGE.startTime = Datetime.now().format('HH:mm');
        skedGE.duration = 60;

        Test.startTest();

            skedGroupEventExt.createJob(skedGE, new list<skedTag>{st});
            skedGroupEventExt.searchGroupTags('comm');
            skedGroupEventExt.addTagsToJob(Ge.Id, new list<Group_Tag__c>{gt});
            skedGE = skedGroupEventExt.getSkedGroupEvent(ge.id,'');
        Test.stopTest();
    }
    

    @isTest static void testCreateGroupEventFromCtrl()
    {

        Map<string, sObject> mapTestData = skedDataSetup.setupJobTestData();

        Contact con = (Contact)mapTestData.get('employeeTest');
        Contact client = (Contact)mapTestData.get('contactTest');
        Contact client2 = (Contact)mapTestData.get('contactTest2');
        Account acc = (Account)mapTestData.get('accountTest');

        sked__Tag__c tag = new sked__Tag__c(Name = 'Communication');
        insert tag;

        enrtcr__Support_Contract_Item__c serviceItem = skedDataFactory.createSupportContract(client);
        Group_Event__c GE = skedDataFactory.createGroupEvent(con, null, (sked__Region__c)mapTestData.get('regionQld'),
                acc , client);
        Group_Tag__c gt = new Group_Tag__c(Group_Event__c = Ge.id, Tag__c = tag.Id, Required__c = true);
        insert gt;

        sked__Job__c job = (sked__Job__c)mapTestData.get('job1');


        skedGroupEvent skedGE = skedGroupEventExt.getSkedGroupEvent(GE.id,'');
        skedGE.startDate = Datetime.now().format('dd/MM/YYYY');
        skedGE.startTime = Datetime.now().format('HH:mm');
        skedGE.duration = 60;

        list<skedGroupClient> newAtteedees = new list<skedGroupClient>();
        skedGroupClient skedGC = new skedGroupClient();
        skedGC.accId = acc.id;
        skedGC.id = client2.Id;
        newAtteedees.add(skedGC);
        Test.startTest();
            skedGroupEventExt.createGroupEvent(skedGE);
        Test.stopTest();
    }

    @isTest static void testCreateAttendeeAttachment(){

        Map<string, sObject> mapTestData = skedDataSetup.setupJobTestData();
        ServiceDeliveryAPI__c SD = new ServiceDeliveryAPI__c();
        SD.secret__c = '1234567';
        insert SD;
        Contact client = (Contact)mapTestData.get('contactTest');
        sked__Job__c job = (sked__Job__c)mapTestData.get('job1');
        Group_Attendee__c att = new Group_Attendee__c(Job__c = job.Id, Contact__c = client.id);
        insert att;

        list<id> attids = new list<id>{att.id};
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new skedHttpCalloutMock(skedHttpCalloutMock.ATTEENDEE_CLIENT));

        skedAttendeeAvailableServiceBatch b = new skedAttendeeAvailableServiceBatch(attids,UserInfo.getSessionId());
        Database.executeBatch(b,1);
        Test.stopTest();

    }


    @isTest static void testSearchClientsWithActiveServiceAgreements()
    {

        List<sked__Region__c> lstRegion = skedDataFactory.createRegions();
        Account acc = skedDataFactory.createAccount();
        Contact con = skedDataFactory.createEmployee(acc);
        enrtcr__Support_Delivered__c serviceDelivered =  skedDataFactory.createServiceDelivered(skedDataFactory.createEnriteCareReferenceData());

        Test.startTest();

        Contact testClient = [SELECT id, name from contact where id= :serviceDelivered.enrtcr__Client__c];

        Group_Event__c GE = skedDataFactory.createGroupEvent(con, null, lstRegion[0], acc, testClient);

        list<skedGroupClient> newAtteedees = new list<skedGroupClient>();
        skedGroupClient skedGC = new skedGroupClient();
        skedGC.accId = acc.id;
        skedGC.id = testClient.id;
        newAtteedees.add(skedGC);

 
        skedGroupEvent skedGE = skedGroupEventExt.getSkedGroupEvent(GE.id,'');
        skedGE.startDate = Datetime.now().format('dd/MM/YYYY');
        skedGE.startTime = Datetime.now().format('HH:mm');
        skedGE.duration = 60;
        skedGroupEventExt.searchClientsWithActiveServiceAgreements('Test', '');

        Test.stopTest();
    }

    @isTest static void testController()
    {

        Map<string, sObject> mapTestData = skedDataSetup.setupJobTestData();

        Contact con = (Contact)mapTestData.get('employeeTest');
        Contact client = (Contact)mapTestData.get('contactTest');
        Contact client2 = (Contact)mapTestData.get('contactTest2');
        Account acc = (Account)mapTestData.get('accountTest');

        enrtcr__Support_Contract_Item__c serviceItem = skedDataFactory.createSupportContract(client);
        Group_Event__c groupEvent = skedDataFactory.createGroupEvent(con, null, (sked__Region__c)mapTestData.get('regionQld'),
                acc , client);


        sked__Job__c job = (sked__Job__c)mapTestData.get('job1');

        Test.startTest();
        skedGroupEventExt ctr = new skedGroupEventExt(new ApexPages.StandardController(groupEvent));
        ctr.getRecordName();
        string url = ctr.jobURL;
        skedGroupEventExt.getConfigData();

        skedGroupEventExt.skedManageAttendee attendee = new skedGroupEventExt.skedManageAttendee('a','b','c','d');
        skedGroupEventExt.cancelJob(groupEvent.Id,job.Id,'Test');
        Test.stopTest();
    }

    @isTest static void testInsertServiceDelivery()
    {

        enrtcr__Support_Delivered__c support_delivered = skedDataFactory.createServiceDelivered(skedDataFactory.createEnriteCareReferenceData());
    }

    @isTest static void testUtils(){
        Test.startTest();

        Test.stopTest();
    }
    
    @isTest static void testException(){
        try{
            integer i = 1/0;
        }catch(exception ex){
            skedGroupEventExt.getExceptionMessage(ex);
        }
    }

    @isTest static void testDailyRecurringAfter(){
        Account acc = skedDataFactory.createAccount();
        Contact emp = skedDataFactory.createEmployee(acc);
        Contact client = skedDataFactory.createClient(acc, 'Skedulo Test');
        sked__Region__c reg = skedDataFactory.createRegions()[0];
        Group_Event__c ge = skedDataFactory.createGroupEvent(emp, null,  reg,  acc,  client);
        
        sked__Tag__c tag = new sked__Tag__c(
            Name = 'Quality Control',
            sked__Type__c = 'Skill',
            sked__Classification__c = 'Global'
        );
        insert tag;

        Group_Tag__c gt = new Group_Tag__c(Group_Event__c = Ge.id, Tag__c = tag.Id, Required__c = true);
        insert gt;

        skedTag st = new skedTag(tag);
        st.required = true;
        skedGroupEvent skedGE = skedGroupEventExt.getSkedGroupEvent(ge.id,'');
        skedGE.startDate = Datetime.now().format('dd/MM/YYYY');
        skedGE.startTime = Datetime.now().format('HH:mm');
        skedGE.duration = 60;
        skedGE.skipWeekends = true;
        skedGE.is_recurring = true;
        skedGE.skipHolidays = false;
        skedGE.selectedRepeat = 'Daily';
        skedGE.every = 1 ;
        skedGE.selectedEnding = 'After';
        skedGE.endingOn =  Datetime.now().addDays(7).format('dd/MM/YYYY');        skedGE.numberOfSessions = 3;
        skedGE.selectedNthDay = 14;
        skedGE.selectedWeekdays = 'Friday,Monday';

        Test.startTest();
        skedGroupEventExt.createJob(skedGE, new list<skedTag>{st});
        Test.stopTest();
    }

    @isTest static void testDailyRecurringAfterHoliday(){
        Account acc = skedDataFactory.createAccount();
        Contact emp = skedDataFactory.createEmployee(acc);
        Contact client = skedDataFactory.createClient(acc, 'Skedulo Test');
        sked__Region__c reg = skedDataFactory.createRegions()[0];
        Group_Event__c ge = skedDataFactory.createGroupEvent(emp, null,  reg,  acc,  client);
        
        sked__Tag__c tag = new sked__Tag__c(
            Name = 'Quality Control',
            sked__Type__c = 'Skill',
            sked__Classification__c = 'Global'
        );
        insert tag;

        Group_Tag__c gt = new Group_Tag__c(Group_Event__c = Ge.id, Tag__c = tag.Id, Required__c = true);
        insert gt;

        skedTag st = new skedTag(tag);
        st.required = true;
        skedGroupEvent skedGE = skedGroupEventExt.getSkedGroupEvent(ge.id,'');
        skedGE.startDate = Datetime.now().format('dd/MM/YYYY');
        skedGE.startTime = Datetime.now().format('HH:mm');
        skedGE.duration = 60;
        skedGE.skipWeekends = true;
        skedGE.is_recurring = true;
        skedGE.skipHolidays = true;
        skedGE.selectedRepeat = 'Daily';
        skedGE.every = 1 ;
        skedGE.selectedEnding = 'After';
        skedGE.endingOn =  Datetime.now().addDays(7).format('dd/MM/YYYY');
        skedGE.numberOfSessions = 3;
        skedGE.selectedNthDay = 14;
        skedGE.selectedWeekdays = 'Friday,Monday';

        Test.startTest();
        skedGroupEventExt.createJob(skedGE, new list<skedTag>{st});
        Test.stopTest();
    }

    @isTest static void testDailyRecurringOn(){
        Account acc = skedDataFactory.createAccount();
        Contact emp = skedDataFactory.createEmployee(acc);
        Contact client = skedDataFactory.createClient(acc, 'Skedulo Test');
        sked__Region__c reg = skedDataFactory.createRegions()[0];
        Group_Event__c ge = skedDataFactory.createGroupEvent(emp, null,  reg,  acc,  client);
        
        sked__Tag__c tag = new sked__Tag__c(
            Name = 'Quality Control',
            sked__Type__c = 'Skill',
            sked__Classification__c = 'Global'
        );
        insert tag;

        Group_Tag__c gt = new Group_Tag__c(Group_Event__c = Ge.id, Tag__c = tag.Id, Required__c = true);
        insert gt;

        skedTag st = new skedTag(tag);
        st.required = true;
        skedGroupEvent skedGE = skedGroupEventExt.getSkedGroupEvent(ge.id,'');
        skedGE.startDate = Datetime.now().format('dd/MM/YYYY');
        skedGE.startTime = Datetime.now().format('HH:mm');
        skedGE.duration = 60;
        skedGE.skipWeekends = true;
        skedGE.is_recurring = true;
        skedGE.skipHolidays = false;
        skedGE.selectedRepeat = 'Daily';
        skedGE.every = 1 ;
        skedGE.selectedEnding = 'On';
        skedGE.endingOn =  Datetime.now().addDays(7).format('dd/MM/YYYY');
        skedGE.numberOfSessions = 3;
        skedGE.selectedNthDay = 14;
        skedGE.selectedWeekdays = 'Friday,Monday';

        Test.startTest();
        skedGroupEventExt.createJob(skedGE, new list<skedTag>{st});
        Test.stopTest();
    }


    @isTest static void testWeeklyRecurringAfter(){
        Account acc = skedDataFactory.createAccount();
        Contact emp = skedDataFactory.createEmployee(acc);
        Contact client = skedDataFactory.createClient(acc, 'Skedulo Test');
        sked__Region__c reg = skedDataFactory.createRegions()[0];
        Group_Event__c ge = skedDataFactory.createGroupEvent(emp, null,  reg,  acc,  client);
        
        sked__Tag__c tag = new sked__Tag__c(
            Name = 'Quality Control',
            sked__Type__c = 'Skill',
            sked__Classification__c = 'Global'
        );
        insert tag;

        Group_Tag__c gt = new Group_Tag__c(Group_Event__c = Ge.id, Tag__c = tag.Id, Required__c = true);
        insert gt;

        skedTag st = new skedTag(tag);
        st.required = true;
        skedGroupEvent skedGE = skedGroupEventExt.getSkedGroupEvent(ge.id,'');
        skedGE.startDate = Datetime.now().format('dd/MM/YYYY');
        skedGE.startTime = Datetime.now().format('HH:mm');
        skedGE.duration = 60;
        skedGE.skipWeekends = true;
        skedGE.is_recurring = true;
        skedGE.skipHolidays = false;
        skedGE.selectedRepeat = 'Weekly';
        skedGE.every = 1 ;
        skedGE.selectedEnding = 'After';
        skedGE.endingOn =  Datetime.now().addDays(14).format('dd/MM/YYYY');
        skedGE.numberOfSessions = 3;
        skedGE.selectedNthDay = 14;
        skedGE.selectedWeekdays = 'Friday,Monday';

        Test.startTest();
        skedGroupEventExt.createJob(skedGE, new list<skedTag>{st});
        Test.stopTest();
    }

    @isTest static void testWeeklyRecurringOn(){
        Account acc = skedDataFactory.createAccount();
        Contact emp = skedDataFactory.createEmployee(acc);
        Contact client = skedDataFactory.createClient(acc, 'Skedulo Test');
        sked__Region__c reg = skedDataFactory.createRegions()[0];
        Group_Event__c ge = skedDataFactory.createGroupEvent(emp, null,  reg,  acc,  client);
        
        sked__Tag__c tag = new sked__Tag__c(
            Name = 'Quality Control',
            sked__Type__c = 'Skill',
            sked__Classification__c = 'Global'
        );
        insert tag;

        Group_Tag__c gt = new Group_Tag__c(Group_Event__c = Ge.id, Tag__c = tag.Id, Required__c = true);
        insert gt;

        skedTag st = new skedTag(tag);
        st.required = true;
        skedGroupEvent skedGE = skedGroupEventExt.getSkedGroupEvent(ge.id,'');
        skedGE.startDate = Datetime.now().format('dd/MM/YYYY');
        skedGE.startTime = Datetime.now().format('HH:mm');
        skedGE.duration = 60;
        skedGE.skipWeekends = true;
        skedGE.is_recurring = true;
        skedGE.skipHolidays = false;
        skedGE.selectedRepeat = 'Weekly';
        skedGE.every = 1 ;
        skedGE.selectedEnding = 'On';
        skedGE.endingOn =  Datetime.now().addDays(14).format('dd/MM/YYYY');
        skedGE.numberOfSessions = 3;
        skedGE.selectedNthDay = 14;
        skedGE.selectedWeekdays = 'Friday,Monday';

        Test.startTest();
        skedGroupEventExt.createJob(skedGE, new list<skedTag>{st});
        Test.stopTest();
    }

    @isTest static void testMonthlyRecurringAfter(){
        Account acc = skedDataFactory.createAccount();
        Contact emp = skedDataFactory.createEmployee(acc);
        Contact client = skedDataFactory.createClient(acc, 'Skedulo Test');
        sked__Region__c reg = skedDataFactory.createRegions()[0];
        Group_Event__c ge = skedDataFactory.createGroupEvent(emp, null,  reg,  acc,  client);
        
        sked__Tag__c tag = new sked__Tag__c(
            Name = 'Quality Control',
            sked__Type__c = 'Skill',
            sked__Classification__c = 'Global'
        );
        insert tag;

        Group_Tag__c gt = new Group_Tag__c(Group_Event__c = Ge.id, Tag__c = tag.Id, Required__c = true);
        insert gt;

        skedTag st = new skedTag(tag);
        st.required = true;
        skedGroupEvent skedGE = skedGroupEventExt.getSkedGroupEvent(ge.id,'');
        skedGE.startDate = Datetime.now().format('dd/MM/YYYY');
        skedGE.startTime = Datetime.now().format('HH:mm');
        skedGE.duration = 60;
        skedGE.skipWeekends = true;
        skedGE.is_recurring = true;
        skedGE.skipHolidays = false;
        skedGE.selectedRepeat = 'Monthly';
        skedGE.every = 1 ;
        skedGE.selectedEnding = 'After';
        skedGE.endingOn =  Datetime.now().addMonths(7).format('dd/MM/YYYY');
        skedGE.numberOfSessions = 3;
        skedGE.selectedNthDay = 14;
        skedGE.selectedWeekdays = 'Friday,Monday';

        Test.startTest();
        skedGroupEventExt.createJob(skedGE, new list<skedTag>{st});
        Test.stopTest();
    }

    @isTest static void testMonthlyRecurringOn(){
        Account acc = skedDataFactory.createAccount();
        Contact emp = skedDataFactory.createEmployee(acc);
        Contact client = skedDataFactory.createClient(acc, 'Skedulo Test');
        sked__Region__c reg = skedDataFactory.createRegions()[0];
        Group_Event__c ge = skedDataFactory.createGroupEvent(emp, null,  reg,  acc,  client);
        
        sked__Tag__c tag = new sked__Tag__c(
            Name = 'Quality Control',
            sked__Type__c = 'Skill',
            sked__Classification__c = 'Global'
        );
        insert tag;

        Group_Tag__c gt = new Group_Tag__c(Group_Event__c = Ge.id, Tag__c = tag.Id, Required__c = true);
        insert gt;

        skedTag st = new skedTag(tag);
        st.required = true;
        skedGroupEvent skedGE = skedGroupEventExt.getSkedGroupEvent(ge.id,'');
        skedGE.startDate = Datetime.now().format('dd/MM/YYYY');
        skedGE.startTime = Datetime.now().format('HH:mm');
        skedGE.duration = 60;
        skedGE.skipWeekends = true;
        skedGE.is_recurring = true;
        skedGE.skipHolidays = false;
        skedGE.selectedRepeat = 'Monthly';
        skedGE.every = 1 ;
        skedGE.selectedEnding = 'On';
        skedGE.endingOn =  Datetime.now().addMonths(3).format('dd/MM/YYYY');
        skedGE.numberOfSessions = 3;
        skedGE.selectedNthDay = 14;
        skedGE.selectedWeekdays = 'Friday,Monday';

        Test.startTest();
        skedGroupEventExt.createJob(skedGE, new list<skedTag>{st});
        Test.stopTest();
    }

    @isTest static void testYearlyRecurringAfter(){
        Account acc = skedDataFactory.createAccount();
        Contact emp = skedDataFactory.createEmployee(acc);
        Contact client = skedDataFactory.createClient(acc, 'Skedulo Test');
        sked__Region__c reg = skedDataFactory.createRegions()[0];
        Group_Event__c ge = skedDataFactory.createGroupEvent(emp, null,  reg,  acc,  client);
        
        sked__Tag__c tag = new sked__Tag__c(
            Name = 'Quality Control',
            sked__Type__c = 'Skill',
            sked__Classification__c = 'Global'
        );
        insert tag;

        Group_Tag__c gt = new Group_Tag__c(Group_Event__c = Ge.id, Tag__c = tag.Id, Required__c = true);
        insert gt;

        skedTag st = new skedTag(tag);
        st.required = true;
        skedGroupEvent skedGE = skedGroupEventExt.getSkedGroupEvent(ge.id,'');
        skedGE.startDate = Datetime.now().format('dd/MM/YYYY');
        skedGE.startTime = Datetime.now().format('HH:mm');
        skedGE.duration = 60;
        skedGE.skipWeekends = true;
        skedGE.is_recurring = true;
        skedGE.skipHolidays = false;
        skedGE.selectedRepeat = 'Yearly';
        skedGE.every = 1 ;
        skedGE.selectedEnding = 'After';
        skedGE.endingOn =  Datetime.now().addYears(3).format('dd/MM/YYYY');
        skedGE.numberOfSessions = 3;
        skedGE.selectedNthDay = 14;
        skedGE.selectedWeekdays = 'Friday,Monday';

        Test.startTest();
        skedGroupEventExt.createJob(skedGE, new list<skedTag>{st});
        Test.stopTest();
    }

    @isTest static void testYearlyRecurringOn(){
        Account acc = skedDataFactory.createAccount();
        Contact emp = skedDataFactory.createEmployee(acc);
        Contact client = skedDataFactory.createClient(acc, 'Skedulo Test');
        sked__Region__c reg = skedDataFactory.createRegions()[0];
        Group_Event__c ge = skedDataFactory.createGroupEvent(emp, null,  reg,  acc,  client);
        
        sked__Tag__c tag = new sked__Tag__c(
            Name = 'Quality Control',
            sked__Type__c = 'Skill',
            sked__Classification__c = 'Global'
        );
        insert tag;

        Group_Tag__c gt = new Group_Tag__c(Group_Event__c = Ge.id, Tag__c = tag.Id, Required__c = true);
        insert gt;

        skedTag st = new skedTag(tag);
        st.required = true;
        skedGroupEvent skedGE = skedGroupEventExt.getSkedGroupEvent(ge.id,'');
        skedGE.startDate = Datetime.now().format('dd/MM/YYYY');
        skedGE.startTime = Datetime.now().format('HH:mm');
        skedGE.duration = 60;
        skedGE.skipWeekends = true;
        skedGE.is_recurring = true;
        skedGE.skipHolidays = false;
        skedGE.selectedRepeat = 'Yearly';
        skedGE.every = 1 ;
        skedGE.selectedEnding = 'On';
        skedGE.endingOn =  Datetime.now().addYears(3).format('dd/MM/YYYY');
        skedGE.numberOfSessions = 3;
        skedGE.selectedNthDay = 14;
        skedGE.selectedWeekdays = 'Friday,Monday';

        Test.startTest();
        skedGroupEventExt.createJob(skedGE, new list<skedTag>{st});
        Test.stopTest();
    }

    @isTest static void testUtil(){
        sked__Region__c reg = new sked__Region__c();
        reg.Name = 'Western Australia';
        reg.sked__Timezone__c = 'Australia/Sydney';
        reg.sked__Country_Code__c = 'AU';
        insert reg;

        sked__Holiday__c holiday = new sked__Holiday__c(sked__Global__c = true,sked__Start_Date__c = system.today(),sked__End_Date__c=system.today());
        insert holiday;

        sked__Holiday__c holiday2 = new sked__Holiday__c(sked__Global__c = false,sked__Start_Date__c = system.today(),sked__End_Date__c=system.today());
        insert holiday2;

        sked__Holiday_Region__c regHol = new sked__Holiday_Region__c(sked__Region__c = reg.id,sked__Holiday__c = holiday2.id);
        insert regHol;


        Test.startTest();

        Set<Date> holidays = skedUtils.getHolidaysByRegion(reg.id);
        skedUtils.getRegionHolidays(system.today(),'Western Australia');

        Test.stopTest();
    }

}