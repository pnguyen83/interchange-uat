global class skedSingleJobAvailableserviceBatch implements Database.Batchable<sObject>,Database.AllowsCallouts, Database.StateFul {
	
	String query;
	list<id> jobIds = new list<id>();
	string sessId;
	
  //get list Attendee ID and current user sessionId
	global skedSingleJobAvailableserviceBatch(list<id> jIds,string sessionId) {
		this.jobIds = jIds;
		sessId = sessionId;
	}
	
  //Query group Attendee
	global Database.QueryLocator start(Database.BatchableContext BC) {
		query = 'select id,sked__Contact__c,sked__Region__r.sked__Timezone__c,sked__Start__c from sked__Job__c where id in:jobIds';
		return Database.getQueryLocator(query);
	}

   	global void execute(Database.BatchableContext BC, List<sked__Job__c> scope) {

      //get Map Attendee and available service json
   		list<Attachment> lstAttachments = new list<Attachment>();
   		map<id,Attachment> mapIDAttachment = new Map<id,Attachment>();

   		for(Attachment att: [Select Id, Name,ParentId,Body From Attachment where ParentId in: scope]){
   			mapIDAttachment.put(att.ParentId,att);
   		}

   		for(sked__Job__c job: scope){
   			string Servicebody = skedJsonAvailableServiceUtil.getServiceItems(job.sked__Contact__c,job.sked__Start__c.format('yyyy-MM-dd',job.sked__Region__r.sked__Timezone__c),sessId);
   			Attachment att = new Attachment();
   			if(mapIDAttachment.containskey(job.id)){
   				att = mapIDAttachment.get(job.id);
   			}else{
   				att.ParentId = job.id;
   			}
   			att.Body= Blob.valueOf(Servicebody);
   			att.name = 'available_service.json';
 			lstAttachments.add(att);
   		}
   		upsert lstAttachments;
	}
	
	global void finish(Database.BatchableContext BC) {
		
	}
}