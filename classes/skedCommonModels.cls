public virtual class skedCommonModels {

    public static String PRIOD_WEEK         = 'week';
    public static String PRIOD_2WEEKS       = '2 weeks';
    public static String PRIOD_MONTH        = 'month';
    public static String PRIOD_FORTNIGHT    = 'Current Fortnightly Roster';

    public static String JOB_HOVER_FIELDS   = 'RM_JOB_HOVER_FIELDS';
    public static String JOB_DETAILS        = 'RM_JOB_DETAILS';

    public static String ROSTER_MANAGEMENT  = 'skedRosterManagement';
    public static String GROUP_EVENT        = 'skedGroupEvent';

    public class CustomException extends Exception {}

    /*
    * 
    */
    public static ConfigData getConfigData(String component){
        ConfigData config = new ConfigData();
        if(component == ROSTER_MANAGEMENT){
            Integer startSchedule = Math.mod(skedConfigs.SCHEDULE_START,100)==0?skedConfigs.SCHEDULE_START:( Integer.valueOf( Math.floor( skedConfigs.SCHEDULE_START/100 ) * 100 ) );
            Integer endSchedule = Math.mod(skedConfigs.SCHEDULE_END,100)==0?skedConfigs.SCHEDULE_END:( Integer.valueOf( Math.ceil( skedConfigs.SCHEDULE_END/100 ) * 100 ) );
            DateTime startTime = DateTime.newInstance(System.today(), Time.newInstance(0, skedTimezoneUtil.toMinute(startSchedule),0,0));
            DateTime endTime = DateTime.newInstance(System.today(), Time.newInstance(0, skedTimezoneUtil.toMinute(endSchedule),0,0));
            if(endTime <= startTime) endTime = endTime.addDays(1);
            for(DateTime tmp = startTime; tmp < endTime; tmp = tmp.addMinutes(60) ){
                Integer minute = tmp.hour() * 60 + tmp.minute();
                RMTimeModel tm =  new RMTimeModel( skedTimezoneUtil.toTime( minute ), tmp.format('hh:mm a'));
                for(Integer i = skedConfigs.SCHEDULE_INTERVAL; i < 60; i+= skedConfigs.SCHEDULE_INTERVAL){
                    DateTime tmp2 = tmp.addMinutes(i);
                    Integer minute2 = tmp2.hour() * 60 + tmp2.minute();
                    tm.minors.add( new TimeModel(skedTimezoneUtil.toTime( minute2 ), tmp2.format('hh:mm a')) );
                }
                //Check interval
                config.timeList.add( tm );
            }
        }
        config.clientLabel = [Select Name from RecordType where sObjectType='Contact' and DeveloperName='Client'].Name;
        
        return config;
    }

    public static DateTime toDateTime(String dtString){
        Date d = toDate(dtString.subStringBefore(' '));
        list<String> tString = dtString.substringAfter(' ').split(':');
        DateTime dt = DateTime.newInstance(d, Time.newInstance(Integer.valueOf(tString[0]), Integer.valueOf(tString[1]), 0, 0));
        return dt;

    }

    public static Date toDate(String ds){
        list<String> dString = ds.split('-');
        return Date.newInstance(Integer.valueOf(dString[0]), Integer.valueOf(dString[1]), Integer.valueOf(dString[2]));
    }
    
    public static DateTime getStartTime(String dateString, String period){
        if( period.equalsIgnoreCase( PRIOD_FORTNIGHT ) ){
            RosterModel roster =  getCurrentRoster();
            if( roster !=null ){
                return roster.startDate;
            }
        }
        Date d = skedUtils.ConvertToDateValue(dateString);
        DateTime startTime = DateTime.newInstance(d, Time.newInstance(0,0,0,0));

        return startTime;
    }

    public static DateTime getEndTime(DateTime startTime, String period){
        DateTime endTime = startTime.addDays(1);//Day period
        if(period.equalsIgnoreCase( PRIOD_WEEK )){
            endTime = endTime.addDays(6);
        }else if(period.equalsIgnoreCase( PRIOD_2WEEKS ) || period.equalsIgnoreCase( PRIOD_FORTNIGHT )){
            endTime = endTime.addDays(13);
        }else if(period.equalsIgnoreCase( PRIOD_MONTH )){
            endTime = endTime.addMonths(1);
                    }
        return endTime;
    }

    @RemoteAction
    public static list<Item> getRegions(String searchString){

        String filter = String.isBlank(searchString)? '%%': ('%' + searchString.trim() + '%');

        list<Item> result = new list<Item>();
        //result.add( new Item( '','All regions' ));

        for(sked__Region__c r : [Select Id, Name   from sked__Region__c where Name like :filter order by Name]){
            result.add( new Item(r.Id, r.Name));
        }
        return result;
    }

    @RemoteAction
    public static list<skedOption> getLocations(String searchString, String regionId){
        if(skedConfigs.LOCATION_OR_SITE_FILTER == 'Location') return searchLocations(searchString, regionId);
        return searchSites(searchString);
    }

    public static list<skedOption> searchLocations(String searchString, String regionId){
        String filter = String.isBlank(searchString)? '%%': ('%' + searchString.trim() + '%');

        list<skedOption> result = new list<skedOption>();

        //result.add(new Item( '0', 'All locations' ));
        Id rId = String.isBlank(regionId)?null: (Id)(regionId);
        for(sked__Location__c r : [Select Id, Name, sked__Region__c, sked__Address__c from sked__Location__c where Name like :filter order by Name]){
            if(rId != null && r.sked__Region__c != rId) continue;
            skedOption location = new skedOption(r.Id, r.Name);
            location.key = r.sked__Address__c;
            result.add( location );
        }

        return result;
    }

   
    public static list<skedOption> searchSites(String searchString){
        
        String filter = String.isBlank(searchString)? '%%': ('%' + searchString.trim() + '%');

        list<skedOption> result = new list<skedOption>();
        
        for(enrtcr__Site__c r : [Select Id, Name, Address__c from enrtcr__Site__c where Name like :filter order by Name]){
            skedOption site = new skedOption(r.Id, r.Name);
            site.key = r.Address__c;
            result.add( site );
        }
        
        return result;
    }

    @RemoteAction
    public static list<ClientModel> getClients(String searchString){
        String filter = String.isBlank(searchString)? '%%': ('%' + searchString.trim() + '%');

        list<ClientModel> result = new list<ClientModel>();
        for(Contact  contact : [Select Id, Name, Phone, MobilePhone, OtherStreet, OtherCity, OtherState, OtherPostalCode, OtherCountry, Birthdate, sked__Region__r.Id, sked__Region__r.Name  from Contact where Name like :filter order by Name limit 20]){
            result.add( new ClientModel(contact));
        }
        return result;
    }

    @RemoteAction
    public static list<Item> getResources(String searchString){
        String filter = String.isBlank(searchString)? '%%': ('%' + searchString.trim() + '%');

        list<Item> result = new list<Item>();
        for(sked__Resource__c  resource : [Select Id, Name  from sked__Resource__c where Name like :filter and sked__Is_Active__c=true order by Name]){
            result.add( new Item(resource.Id, resource.Name));
        }
        return result;
    }

    public static list<sked__Job__c> getRecurringJobs(String jobId, boolean allJobs){
        list<sked__Job__c> jobs = new list<sked__Job__c>();
        sked__Job__c job = [Select Id, sked__Job_Status__c, sked__Recurring_Schedule__c from sked__Job__c where Id=:jobId];
        if(job.sked__Recurring_Schedule__c!=null && allJobs){
            jobs = [Select Id, sked__Job_Status__c from sked__Job__c
                      where sked__Recurring_Schedule__c=:job.sked__Recurring_Schedule__c
                      and Id!=:job.Id and (NOT (sked__Job_Status__c IN ('Complete','Cancelled')))
                      and sked__Start__c > :System.now()];
        }
        jobs.add(job);
        return jobs;
    }

    /*
    *
    */
    public static void sendEmail(String subject, String body, list<String> toAddresses){
        Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
        
        message.toAddresses = toAddresses;
        //message.toAddresses = toAddresses;
        message.subject = subject;
        message.htmlBody = body;
        Messaging.SingleEmailMessage[] messages = new List<Messaging.SingleEmailMessage> {message};
        Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);
        if (results[0].success) {
            System.debug('The email was sent successfully.');
        } else {
            System.debug('The email failed to send: ' + results[0].errors[0].message);
        }
    }

    @RemoteAction
    public static list<String> getAbortReasons(){
        list<String> values = new list<String>();
        Schema.DescribeFieldResult fieldResult = sked__Job__c.sked__Abort_Reason__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();

       for( Schema.PicklistEntry f : ple){
          values.add(f.getValue());
       }
       return values;
    }

    /*
    * Build dynamic SOQL query
    */
    public static list<String> getFields(String key){
        list<String> fields = new list<String>();
        Map<String,String> fieldMap = getFieldMap();
        if(skedConfigs__c.getAll().containsKey( key )){
            String f = skedConfigs__c.getAll().get( key ).Value__c;
            if(String.isBlank(f)) return fields;
            for(String s : f.split(',')){
                String field = s.trim().toLowerCase();
                if(fieldMap.containsKey( field )) fields.add( field );
            }
        }
        return fields;
    }

    /*
    * Get a map of field api name to field label
    */
    public static Map<String, String> getFieldMap(){
        Map<String, String> fieldMap = new Map<String, String>();
        Map <String, Schema.SObjectField> fm = Schema.SObjectType.sked__Job__c.fields.getMap();
        for(Schema.SObjectField sfield : fm.Values()){
            schema.describefieldresult dfield = sfield.getDescribe();
            fieldMap.put(dfield.getName().toLowerCase(), dfield.getLabel());
        }
        return fieldMap;
    }

    //DATA MODELS

    public class ActionResult{
        public boolean hasError;
        public String msg{
            get;
            set{
                msg = value;
                if(msg == null) msg = '';
            }
        }
        public String recId;
        public String json;
        public Object result;

        public ActionResult(boolean err, String msg){
            this.hasError   = err;
            this.msg        = msg;
            this.json       = '{}';
        }

        public ActionResult(boolean err, String msg, String recId){
            this.hasError   = err;
            this.msg        = msg;
            this.recId      = recId;
        }
    }

    public class DateSlotModel {
        public String dateString{get;set;}
        public String label{get;set;}
        public List<SlotModel> slots{get;set;}
        public DateSlotModel(DateTime dt){
            this.dateString = dt.format('yyyy-MM-dd');
            this.label = dt.format('E, MMM dd');//E: Day in Week (Thu), MMM: Month (Aug), dd: Day in month (27)
            slots = new list<SlotModel>();
        }
    }

    public class SlotModel {
        //common attributes
        public String id;
        public String status;//allocation status
        public String dateString;
        public TimeModel start;
        public TimeModel finish;
        public Integer duration;
        public String slotColor;
        public Map<String,String> jobInfo;
        public list<String> jobDetails;
        public String slotType;
        //Availability attributes
        public boolean isAvailabilityRecord;
        public String availabilityRecordLabel;
        //Job attributes
        public TimeModel actualStart;
        public TimeModel actualEnd;
        public String resourceId;
        public String resourceName;
        public String allocationId;
        public String clientId;
        public String jobNumber;
        public String slotStatus;
        public String clientName;
        public String clientAddress;
        public String clientPhone;

        public String clientMobile;
        public Integer travelTime;
        public String jobNotes;
        public boolean visibleInMobile;
        public Boolean isAdhoc;
        public Boolean isRecurring;
        public Boolean isGroup;
        public String overnightDS;
        public String location;
        public String allocatedResources;
        public String jobServiceType;
        //Group Event Job
        public String groupName;
        public String groupId;
        public String address;


        //consructor for job slot
        public SlotModel(sked__Job__c job, DateTime startTime, DateTime finishTime){
            this.id = job.Id;
            this.overnightDS    = startTime==null?job.sked__Start__c.format('yyyy-MM-dd'):startTime.format('yyyy-MM-dd');
            this.status         = job.sked__Job_Allocations__r.isEmpty()?'unallocated':'allocated';
            this.slotStatus      = job.sked__Job_Status__c;
            this.duration       = Integer.valueOf(job.sked__Duration__c);
            this.dateString     = startTime==null?job.sked__Start__c.format('yyyy-MM-dd'):startTime.format('yyyy-MM-dd');
            this.start          = new TimeModel(job.sked__Start__c, startTime==null?job.sked__Start__c:startTime);
            this.finish         = new TimeModel(job.sked__Finish__c, finishTime==null?job.sked__Finish__c:finishTime);
            this.actualStart    = new TimeModel(job.sked__Actual_Start__c, job.sked__Actual_Start__c);
            this.actualEnd      = new TimeModel(job.sked__Actual_End__c, job.sked__Actual_End__c);
            this.address        = job.sked__Address__c;
            this.jobNumber      = job.Name;
            this.slotType       = 'Job';
            this.clientId       = job.sked__Contact__r.Id;
            this.clientName     = job.sked__Contact__r.Name;
            this.jobNotes       = job.sked__Notes_Comments__c;
            this.groupId        = job.Group_Event__c;
            this.groupName      = job.Group_Event__r.Name;
            if(job.sked__Contact__r.OtherStreet!=null) this.clientAddress = skedUtils.CombineAddress(job.sked__Contact__r.OtherStreet, job.sked__Contact__r.OtherCity, job.sked__Contact__r.OtherState, job.sked__Contact__r.OtherPostalCode, job.sked__Contact__r.OtherCountry);
            this.clientPhone    = job.sked__Contact__r.Phone ;
            this.clientMobile   = job.sked__Contact__r.MobilePhone ;

            //this.jobInfo = getJobInfo(job.Id); //new Map<String,String>{'Name' => 'JOB-0001','Contact' => 'John Doe'};

            this.allocatedResources = '';
            for(sked__Job_Allocation__c ja : job.sked__Job_Allocations__r){
                this.allocatedResources = String.isBlank(this.allocatedResources)? ja.sked__Resource__r.Name : (this.allocatedResources + ', ' + ja.sked__Resource__r.Name);
            }
            if(job.sked__Job_Allocations__r.isEmpty()) this.allocatedResources = 'No resource allocated';
            
            //Set Color
            Map<String,skedConfigs__c> configMap = skedConfigs__c.getAll();
            String key = 'RM_JobStatusColors_' + this.slotStatus;
            if(configMap.containsKey(key)){
                skedConfigs__c config = configMap.get(key);
                this.slotColor = config.Value__c;
            }
            //this.location = 'New Farm';
            this.jobDetails = new list<String>{start.label + ' - ' + finish.label, job.sked__Description__c, allocatedResources};
            //this.jobDetails = getJobDetails(job.Id);
        }
        //constructor for availability slot
        public SlotModel(sked__Availability__c avail){
            this.id             = avail.Id;
            this.slotStatus     = avail.sked__Status__c;
            this.slotType       = avail.sked__Is_Available__c==true?'Availability':'Unavailability';
            this.dateString     = avail.sked__Start__c.format('yyyy-MM-dd');
            this.start          = new TimeModel(avail.sked__Start__c, avail.sked__Start__c);
            this.finish         = new TimeModel(avail.sked__Finish__c, avail.sked__Finish__c);
            this.duration       = Integer.valueOf((avail.sked__Finish__c.getTime() - avail.sked__Start__c.getTime())/1000/60);
            this.isAvailabilityRecord       = avail.sked__Is_Available__c;
            this.jobNumber    = avail.sked__Type__c!=null?avail.sked__Type__c : this.slotType;
            this.jobDetails = new list<String>{start.label + ' - ' + finish.label};
            //Set Color
            Map<String,skedConfigs__c> configMap = skedConfigs__c.getAll();
            String key = 'RM_AvailabilityColor';
            if(configMap.containsKey(key)){
                skedConfigs__c config = configMap.get(key);
                this.slotColor = config.Value__c;
            }

        }

        public void populateJobInfo(sked__Job__c job){
            Map<String,String> result = new Map<String,String>();

            Map<String,String> fieldMap = getFieldMap();
            Map <String, Schema.SObjectField> fm = Schema.SObjectType.sked__Job__c.fields.getMap();

            for(String field : getFields(JOB_HOVER_FIELDS)){
                if(fm.get(field).getDescribe().getType() == Schema.DisplayType.DATETIME){
                    DateTime dt = (DateTime)job.get(field);
                    result.put( fieldMap.get(field), dt.format());
                }else result.put( fieldMap.get(field), String.valueOf(job.get( field )));
            }

            this.jobInfo = result;
        }
        /*
        public list<String> getJobDetails(String jobId){
            list<String> result = new list<String>();
            String query = 'Select Id ';
            for(String field : skedCommonModels.getFields(JOB_DETAILS)){
                if(!query.containsIgnoreCase(field)) query += ',' + field;
            }
            query += ' from sked__Job__c where Id=:jobId';

            Map<String,String> fieldMap = getFieldMap();
            Map <String, Schema.SObjectField> fm = Schema.SObjectType.sked__Job__c.fields.getMap();

            for(sked__Job__c job : Database.query(query)){
                for(String field : getFields(JOB_DETAILS)){
                    if(fm.get(field).getDescribe().getType() == Schema.DisplayType.DATETIME){
                        DateTime dt = (DateTime)job.get(field);
                        result.add( dt.format());
                    }else result.add( String.valueOf(job.get( field )));
                }
            }

            return result;
        }
        */
    }

    public class TimeModel {
        public Integer id;
        public String label;
        public TimeModel(DateTime dt, DateTime dtLabel){
            String d = dt==null?'':dt.format('HH:mm');//Hour in day (0-23)
            if(d==''){
                this.id = 0;
            } else {
                this.id = Integer.valueOf(d.subStringBefore(':'))*100 + Integer.valueOf(d.subStringAfter(':'));
            }

            this.label = dtLabel==null?'':dtLabel.format('hh:mm a');//Hour in am/pm (1-12)
        }
        public TimeModel(Integer i, String l){
            this.id     = i;
            this.label  = l;
        }
    }

    public class Item {
        public String id;
        public String label;
        public integer noOfRooms;
        public integer saiCount;
        public Item(String id, String label){
            this.id = id;
            this.label = label;
        }

        public Item(String id, String label, integer nor){
            this.id = id;
            this.label = label;
            this.noOfRooms = nor;
        }
    }


    //== skedShiftCreationController

    //DataModel
    public class ClientModel {
        public String id;
        public String name;
        public String label;
        public String homePhone;
        public String mobile;
        public String dob;
        public String clientAddress;
        public String shiftAddress;
        public String locationType;
        public skedOption site;
        public skedOption region;
        public ClientModel(contact c){
            this.id = c.Id;
            this.name = c.name;
            this.label = c.name;
            this.homePhone = c.Phone;
            this.mobile     = c.MobilePhone;
            this.dob    = c.Birthdate==null?'':c.Birthdate.format();
            
            if(c.OtherStreet!=null) this.clientAddress = skedUtils.CombineAddress(c.OtherStreet, c.OtherCity, c.OtherState, c.OtherPostalCode, c.OtherCountry);
            this.shiftAddress = this.clientAddress;
            this.region     = new skedOption(c.sked__Region__r.Id, c.sked__Region__r.Name);
            this.locationType = 'Home';
        }
    }

    public class ConfigData{
        public list<RMTimeModel> timeList{get;set;}
        public String jobURL{get;set;}
        public String locationOrSiteFilter{get;set;}
        public String defaultDeliveryMethod{get;set;}
        public boolean enableRecurringJobs{get;set;}
        public String clientLabel{get;set;}

        public ConfigData(){
            this.timeList           = new list<RMTimeModel>();
            this.jobURL             = skedConfigs.JOB_URL;
            this.locationOrSiteFilter     = skedConfigs.LOCATION_OR_SITE_FILTER;
            this.defaultDeliveryMethod      = skedConfigs.DEFAULT_DELIVERY_METHOD;
            this.enableRecurringJobs    = skedConfigs.ENABLE_RECURRING_JOB;
        }
    }

    public class RMTimeModel{
        public Integer id;
        public String label;
        public list<TimeModel> minors;

        public RMTimeModel(Integer i, String l){
            this.id     = i;
            this.label  = l;
            minors      = new list<TimeModel>();
        }
    }

    //=== Roster Utils ===
    public static RosterModel getCurrentRoster() {
        RosterModel result = new RosterModel();
        sked_Roster_Setting__c rosterSetting = sked_Roster_Setting__c.getOrgDefaults();
        if(rosterSetting != null) {
            DateTime rosterEndDate = rosterSetting.Start_Date__c.addDays((Integer)rosterSetting.Period__c);
            while(rosterEndDate < System.now()){
                rosterEndDate = rosterEndDate.addDays((Integer)rosterSetting.Period__c);
            }
            result.startDate = rosterEndDate.addDays(-(Integer)rosterSetting.Period__c);
            result.finishDate = rosterEndDate.addMinutes(-1);
            return result;
        }
        return null;
    }
    
    public static RosterModel getNextRoster() {
        RosterModel currentRoster = getCurrentRoster();
        RosterModel result = new RosterModel();
        sked_Roster_Setting__c rosterSetting = sked_Roster_Setting__c.getOrgDefaults();
        if(currentRoster != null) {
            result.startDate = currentRoster.finishDate.addMinutes(1);
            result.finishDate = result.startDate.addDays((Integer)rosterSetting.Period__c).addMinutes(-1);
            return result;
        }
        return null; 
    }

    public class RosterModel {
        public Datetime startDate {get; set;}
        public Datetime finishDate {get; set;}
        
        public RosterModel() {
            
        }
    }
}