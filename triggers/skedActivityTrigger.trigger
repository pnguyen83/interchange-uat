trigger skedActivityTrigger on sked__Activity__c (after insert, after update) {
	skedTriggerUtil.isFromSkeduloActivity = true;

	if(Trigger.isAfter){
		if( Trigger.isInsert || Trigger.isUpdate){
			if(skedTriggerUtil.isFromEvent)  return; 
			skedTriggerUtil.syncActivitiesToEvents(Trigger.new);
		}
	}
	
}