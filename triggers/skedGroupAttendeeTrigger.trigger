trigger skedGroupAttendeeTrigger on Group_Attendee__c (before insert, before update, before delete, after insert, after update, after delete) {
    if(Trigger.isBefore){
        if(Trigger.isDelete)
        {
            skedJobTriggerHandler.removeGroupAttendeeServiceItem(Trigger.oldMap);
        }
    }
    if(Trigger.isAfter){
        if(Trigger.isInsert)
        {
            skedJobTriggerHandler.generateGroupAttendeeServiceItem(Trigger.newMap);

            skedJobTriggerHandler.createAttendeeAttachment(trigger.new);
        }

        if(Trigger.isUpdate){
            //generate Note to Contact
            skedJobTriggerHandler.generateContactNote( Trigger.new, Trigger.oldMap);

            skedJobTriggerHandler.generateGroupAttendeeServiceItem(Trigger.newMap);
        }
    }
}