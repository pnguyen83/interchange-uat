trigger skedJobServiceItemTrigger on Job_Service_Item__c (after insert,before insert,after update, before update,before delete) {
    skedTriggerHandler handler = new skedTriggerHandler();
    if(Trigger.isBefore){
        if(Trigger.isInsert){
            skedTriggerHandler.populateJSIData(Trigger.new);
        }else if(Trigger.isUpdate){
            //SEP-385: Update Service information on the SD when a JSI is changed in Sked x
            skedTriggerHandler.updateJSIOnSAIChange(Trigger.new, Trigger.oldMap);
        }else if(Trigger.isDelete){
            //Delete Related Service Delivery when Job Service Item is Deleted
            handler.deleteServiceDelivered(trigger.old);
        }
    }else{//After 
        if(Trigger.isInsert){
            //Create Service Delivered when Job Service Item is created : RW-60
            handler.createServiceDelivery(trigger.new); 
            //SEP-389: Send email notification if Jobs are created outside SA date range
            skedTriggerHandler.validateServiceAgreementDate(Trigger.new);
        }else if(Trigger.isUpdate){
            //Update Related Service Delivered Record : RW-60
            handler.updateServiceDelivery(trigger.new,trigger.oldMap);
            //update Actual_Distance_Traveled_KM__c on Job Allocation from Job Service Item : SEP-25
            handler.updateActualDistanceTraveled(trigger.new, trigger.old);
        }
    }
}