trigger skedJobAllocationTriggers on sked__Job_Allocation__c (after insert, after update, after delete, before insert, before update) {
    if(skedTriggerHandler.bypassJobAllocationTrigger==true) return;
    skedTriggerUtil.isFromJobAllocation = true;
    skedTriggerHandler handler = new skedTriggerHandler();

    if(Trigger.isInsert){
        if (trigger.isBefore) {
            //update estimated distance for job allocation
            // TD: comment out because of null object exception
            skedTriggerUtil.updateDistance(trigger.new); //update for SEP-25
            
            //
            for(sked__Job_Allocation__c ja : Trigger.new){
                ja.Show_Estimated_Distance_Travelled__c = skedConfigs.SHOW_ESTIMATED_DISTANCE_TRAVELLED;
            }
        }
        //Create new Events for Dispatched Job Allocations
        list<Id> jaIDs = skedTriggerUtil.getNewDispatchedJobAllocationIDs( Trigger.new );
        skedTriggerUtil.syncJobAllocationsToEvents( jaIDs );
        //Update Worker on the Job Service Item -- PM - TODO: Why is this being updated when this Can be muliple resources?
        //handler.updateWorkerOnJobServiceItem(trigger.new);
        //check duplicated JA
        skedDuplicatedJABatch.checkDuplicatedJA(trigger.new);
    }

    if(Trigger.isUpdate){
        if (trigger.isBefore) { //update for SEP-25
            List<sked__Job_Allocation__c> lstNew = skedTriggerUtil.getInProgressAllocation(trigger.newMap, trigger.oldMap);
            if (lstNew != null) {
                skedTriggerUtil.updateDistance(lstNew);
            }
            
        }
        
        //Create new Events for Dispatched Job Allocations
        Set<String> statusUpdateSyncToEvent = new Set<String>{'Dispatched','Confirmed','En Route','Checked In','In Progress','Complete','Modified'};
        list<Id> jaIDs = skedTriggerUtil.getJobAllocationIDs_StatusChanged( statusUpdateSyncToEvent, Trigger.new, Trigger.old, Trigger.oldMap );
        skedTriggerUtil.syncJobAllocationsToEvents( jaIDs );

        skedTriggerUtil.syncServiceDelivery(trigger.new, trigger.OldMap);

        //Delete Events when Job are Declined
        list<Id> deletedJAIDs = skedTriggerUtil.getJobAllocationIDs_StatusChanged( new Set<String>{'Declined','Deleted'}, Trigger.new, Trigger.old, Trigger.oldMap );
        list<Event> lstEvent = [select id from Event where Job_Allocation_ID__c in:deletedJAIDs];
        if(lstEvent.size() > 0){
            delete lstEvent;
        }

        //list<Id> deletedStatusJAIDs = skedTriggerUtil.getJobAllocationIDs_StatusChanged( new Set<String>{'Deleted'}, Trigger.new, Trigger.old, Trigger.oldMap );
        List<sked__Job_Allocation__c> jobAllocations = [Select Id, sked__Job__c, sked__Resource__c From sked__Job_Allocation__c Where Id IN :deletedJAIDs];
        if(jobAllocations.size() > 0) {
            handler.updateResourceOnJobServiceItem(jobAllocations);
        }
        
        skedDuplicatedJABatch.checkDuplicatedJA(trigger.new);    
        
    }
    //Delete Job Allocation Delete Events
    if(Trigger.isDelete){
        skedTriggerUtil.deleteRelatedEvents( Trigger.old );
    }

}