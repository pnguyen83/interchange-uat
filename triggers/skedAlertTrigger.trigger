trigger skedAlertTrigger on enrtcr__Alert__c (after insert) 
{
    if (Trigger.isAfter && Trigger.isInsert)
    {
        Set<Id> contactIds = new Set<Id>();
        for (enrtcr__Alert__c a : Trigger.New)
        {
            contactIds.add(a.enrtcr__Client__c);
        }
        skedTriggerHandler handler = new skedTriggerHandler();
        skedTriggerHandler.bypassJobTrigger = true;
        handler.updateJobsForClientAlerts(contactIds);
        skedTriggerHandler.bypassJobTrigger = false;
    }
}