trigger skedEventTrigger on Event (after insert, after update, after delete) {
	skedTriggerUtil.isFromEvent = true;

	if( Trigger.isAfter){
		if(Trigger.isUpdate){
			if(skedConfigs.LOCK_EVENT) skedTriggerUtil.lockEvents(Trigger.new);
		}

		if(Trigger.isDelete){
			if(skedConfigs.LOCK_EVENT) skedTriggerUtil.lockEvents(Trigger.old);
			//Delete related Activities
			skedTriggerUtil.deleteActivities( Trigger.old );
		}

		if(Trigger.isInsert || Trigger.isUpdate){
			if(skedTriggerUtil.isFromSkeduloActivity || skedTriggerUtil.isFromJobAllocation)  return;
			skedTriggerUtil.syncEventsToActivities( Trigger.new);
		}
	}
	
}